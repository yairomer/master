\chapter{Results and discussions}
% ===============================

\label{chapter:results}
\lhead{Chapter 3. \emph{Results}}

\section{The Hall conductance}
% ----------------------------

We present here the results for the calculated Chern numbers, which are the Hall conductance in units of $\frac{e^2}{h}$, as described in section \ref{section:berry_curvature}.

As a reference, we compare the t-J model's results to those of the non-interacting model which are presented in fig. \ref{fig:phase_diagram_non_interacting}. 
\begin{figure}[htb!]
	\begin{center}
		\subfigure{\label{fig:phase_diagram_non_interacting-a}			\includegraphics{phase_diagram_non_interacting.png}}
		\subfigure{\label{fig:phase_diagram_non_interacting-b}\includegraphics{chern_non_interacting.png}}
	\caption[Chern number - non-interacting]{Two representations of the Chern number as a function of the number of fermions in the non-interacting model}
  	\label{fig:phase_diagram_non_interacting}
	\end{center}
\end{figure}
For the non-interacting model the Hall conductivity grows linearly with the doping (the number of fermions) up to the point where it reaches half filing where the Hall conductance flips it's sign and then continues anti-symmetrically up until full filling.

For the t-J Model we have calculated the Chern number for all doping up to half filling for a range of values of $t/J$. The results are presented in fig. \ref{fig:phase_diagram_v0_00}.
\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=16cm]{phase_diagram_v0_00.png}
	\caption[Chern number - no NNN hopping]{The Chern number in the t-J model. The areas marked out are of missing data which could not be calculated due to degeneracy of the ground state}
  	\label{fig:phase_diagram_v0_00}
	\end{center}
\end{figure}
For a small number of fermions the Hall conductance is as expected the same as of the non-interacting model since the contribution of the interaction is small. But as we approach half filing the Hall conductance starts to deviate from that of the non-interacting model, especially for small values of $t/J$. We believe though that the small size of the system has significant effect on the results and the large variations in the Chern numbers are likely to be attributed to these finite size effect. We elaborate on the problems of the systems size in appendix \ref{appendix:limitations}.

The addition of the NNN hopping term partially removes the large variations in the Chern number. The results for the system with a NNN hopping term of $v=0.5J$ are presented in fig. \ref{fig:phase_diagram_v0_50}.
\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=16cm]{phase_diagram_v0_50.png}
	\caption[Chern number - with NNN hopping]{The Chern number in the t-J model with NNN hoping with $v=0.5J$. The areas marked out are of missing data which could not be calculated due to degeneracy of the ground state}
  	\label{fig:phase_diagram_v0_50}
	\end{center}
\end{figure}
In this case we can clearly see that for even number of holes close to half filing the systems tend to have a hole-like (negative) Hall conductance (appearing in shades of brown). We attribute this result to the pair binding mechanism.

\section{Connection to Pair binding}
% ----------------------------------

In section \ref{section:pair_binding} we have described the pair binding mechanism and the condition for it to happen depending on how negative is the second derivative for the total energy with respect to the filling of the system (eq. \eqref{eq:pair_binding}). We find a strong correlation between this criteria and the area where the hall conductance changes sign. In fig. \ref{fig:chern_vs_pair_no_NNN} and fig. \ref{fig:chern_vs_pair_with_NNN} we plot side by side the Chern number for even filling and the second derivative of the total energy. 
\begin{figure}[ht]
	\begin{center}
		\subfigure[Chern number at even filling without NNN]{\label{fig:chern_vs_pair_no_NNN-a}\includegraphics[width=14cm]{chern_diagram_even_v0_00.png}}
		\subfigure[The pair binding criteria without NNN]{\label{fig:chern_vs_pair_no_NNN-b}\includegraphics[width=14cm]{pair_binding_diagram_v0_00.png}}
	\end{center}
	\caption[Chern vs. Pair binding]{The Chern number and the pair binding criteria showing the good correlation between the two, suggests that the sign reversal anomaly in the t-J model might be related to the pairing of holes to create a new effective charge carrier}
  	\label{fig:chern_vs_pair_no_NNN}
\end{figure}
\begin{figure}[ht]
	\begin{center}
		\subfigure[Chern number at even filling with NNN $v=0.5J$]{\label{fig:chern_vs_pair_with_NNN-a}\includegraphics[width=14cm]{chern_diagram_even_v0_50.png}}
		\subfigure[The pair binding criteria with NNN $v=0.5J$]{\label{fig:chern_vs_pair_with_NNN-b}\includegraphics[width=14cm]{pair_binding_diagram_v0_50.png}}
	\end{center}
	\caption[Chern vs. Pair binding]{The same plot with NNN interaction}
  	\label{fig:chern_vs_pair_with_NNN}
\end{figure}
The high correlation between the two suggests that the sign reversal anomaly in the t-J model might be related to the pairing of holes to create a new effective charge carrier. In fig. \ref{fig:holes_chern_v0_50} we plot the Chern number as a function of the number of pairs of holes for $t/J=1$. The behaviour of the system resembles the expected behaviour of a non-interacting system with pair of holes as the charge carriers. 
\begin{figure}[ht]
	\begin{center}
		\includegraphics{holes_chern_v0_50.png}
		\caption[Chern number vs. pairs of holes]{The Chern number plotted as a function of the number of pairs of holes for $t/J=1$. The behaviour resembles the expected behaviour of a non interacting system with pair of holes as the charge carriers}
		\label{fig:holes_chern_v0_50}
	\end{center}
\end{figure}

Further work is suggested to better examine the correlation between the pair binding criteria and sign reversal of the Hall conductance. This could be done by going into larger systems by utilizing more compute power and by using more advance methods such as DMRG, as the same as what has been done in \cite{gong2014emergent}.

\clearpage

\section{Conclusion}
% ------------------
In this work we have studied the behavior of the Hall conductance in a 4 by 4 2D t-J model. We have found large deviations in the behavior of the system in comparison to the non-interaction model. In specific we have found that the system under goes a sign reversal of the Hall conductance for system filling below half filling, which does not happen in the non-interacting system. Although the small size of the system, which subjects it to some non-negligible finite size effects, we believe that these results provide good evidence to the strong effect the interaction have on the behavior of the system. We have shown that the behavior of the system's Hall conductance has as good correlation with the tendency of the system to support a pair binding mechanism, this suggest that the sign reversal effect in this model might be related to the pairing of holes to create a new effective charge carrier.