\chapter{A Short Description of the Lanczos Algorithm}
% ===================================================

\label{appendix:lanczos}
\lhead{Appendix B. \emph{A Short Description of the Lanczos Algorithm}}

We give here a short description of the Lanczos algorithm which has been used to calculated the system's ground energies and states. 

Lanczos is probably the most common method for numerically calculating the extreme (largest and smallest) eigenvalue and eigenstates of a matrix. The Lanczos algorithm finds the $m$ largest (by magnitude) eigenvalues and the corresponding eigenvectors of a given hermitian matrix (which from here on will be referred to as the leading eigenvalues and leading eigenvectors). It is especially efficient for large and\textbackslash{}or sparse matrices. The algorithm is in fact an extension of a much more simple method call power iteration which uses the fact that when multiplying some initial random vector again and again by a given matrix the resulting vector will converge to the leading eigenvector of the matrix. More formally, for a given matrix $A$ and a random initial vector $\boldsymbol{b}_{0}$ the series:
\begin{equation}
\boldsymbol{b}_{k+1}=\frac{A\boldsymbol{b}_{k}}{\left\Vert A\boldsymbol{b}_{k}\right\Vert }
\end{equation}
converges to the eigenvector $\boldsymbol{v}_{0}$ corresponding to the largest eigenvalue $\lambda_{0}$ of $A$. We assume here that $\boldsymbol{v}_{0}\cdot\boldsymbol{b}_{0}\ne0$ which is very probable when $\boldsymbol{b}_{0}$ is chosen randomly. In theory after finding $\boldsymbol{v}_{0}$, this method can be reused for finding the next eigenvalue and eigenvector by rerunning the algorithm with a new initial vector $\tilde{\boldsymbol{b}}_{0}$ which is perpendicular to $\boldsymbol{v}_{0}$ ($\boldsymbol{v}_{0}\cdot\tilde{\boldsymbol{b}}_{0}=0$). In practice this method is only stable for the largest eigenvector and converges slowly even for the largest eigenvalue.

The Lanczos algorithm, when used iteratively, is much more stable and faster. The basic idea behind the algorithm is to not only approximate the leading eigenvalue and vector but to calculate an approximation of a predefined small number $m$ of the leading eigenvalues and vectors of $A$ at each step. This is done by exploiting the fact that for any given hermitian matrix and a random vector there exist a straight forward way to find a tridiagonal matrix similar to the given
matrix which then can easily be solved. 

Remarks:
\begin{itemize}
\item A tridiagonal matrix is a matrix with non-zero elements only on the main diagonal and the two diagonals below and above the main diagonal.
\item $B$ is similar to $A$, if there exist some matrix $P$ for which $B=P^{-1}AP$. Two similar matrix share the same eigenvalues and a simple connection exist between their eigenvectors.
\end{itemize}

The ``tridiagonalization'' is done using using a space called Krylov space. For a given hermitian matrix $A$ and an arbitrary vector $\boldsymbol{b}$, both of dimension $n$, the Krylov matrix of $A$ and $\boldsymbol{b}$ is defined as:
\begin{equation}
K=\left(\begin{array}{ccccc}
| & | & | &  & |\\
\boldsymbol{b} & A\boldsymbol{b} & A^{2}\boldsymbol{b} & \cdots & A^{n-1}\boldsymbol{b}\\
| & | & | &  & |
\end{array}\right)
\end{equation}

It can be shown that under the transformation $C=K^{-1}AK$ , which is the projection of $A$ into the Krylov space, the resulting matrix $C$ has the following form:

\begin{equation}
C=\left(\begin{array}{cccccc}
0 & 0 & 0 & \cdots & 0 & -\alpha_{0}\\
1 & 0 & 0 & \cdots & 0 & -\alpha_{1}\\
0 & 1 & 0 & \cdots & 0 & -\alpha_{2}\\
\vdots & \vdots & \vdots & \ddots &  & \vdots\\
0 & 0 & 0 &  & 1 & -\alpha_{n-1}
\end{array}\right)
\end{equation}

Where $\alpha_{i}$ are the coefficients of $A$'s characteristic polynomial 
\begin{equation}
p\left(x\right)=\det\left(xI-A\right)=x^{n}+\alpha_{n-1}x^{n-1}+\cdots+\alpha_{1}x+\alpha_{0}
\end{equation}
The matrix $C$ is known as the Companion Matrix of $p\left(x\right)$. Although $C$ has a simple form, calculating $K^{-1}$ is very exhaustive. To solve this we can use the $QR$ decomposition of $K$ and calculate instead a matrix $H$ which is defined as follow:
\begin{eqnarray*}
C & = & K^{-1}AK=R^{-1}Q^{\dagger}AQR\\
\Rightarrow H & = & RCR^{-1}=Q^{\dagger}AQ
\end{eqnarray*}

$H$ must be an hermitian tridiagonal matrix, this is due to:$C$'s special form, the fact that $R$ is an upper triangular matrix and the fact $H$ must be hermitian as same as $A$, i.e.:

\begin{equation}
H=Q^{\dagger}AQ=\begin{pmatrix}\alpha_{1} & \beta_{1} &  &  & 0\\
\beta_{1}^{*} & \alpha_{2} & \beta_{2}\\
 & \beta_{2}^{*} & \alpha_{3} & \ddots\\
 &  & \ddots & \ddots & \beta_{n-1}\\
0 &  &  & \beta_{n-1}^{*} & \alpha_{n}
\end{pmatrix}
\end{equation}

One way to calculate the $Q$ matrix is to use the Gram-Schmidt $QR$ decomposition of $K$ (an other way would be to use Householder of Givens rotations) . When using the Gram-Schmidt $QR$ decomposition the columns of $Q$ are in fact the orthonormal basis vectors created by applying the Gram-Schmidt process to the Krylov vectors $\left\{ \boldsymbol{b},A\boldsymbol{b},A^{2}\boldsymbol{b},..,A^{n-1}\boldsymbol{b}\right\} $, we shall designate these vectors as $\boldsymbol{q}_{i}$. Therefore:
\begin{eqnarray*}
H=\begin{pmatrix}- & \boldsymbol{q}_{1} & -\\
- & \boldsymbol{q}_{2} & -\\
 & \vdots\\
- & \boldsymbol{q}_{n} & -
\end{pmatrix}A\left(\begin{array}{cccc}
| & | &  & |\\
\boldsymbol{q}_{1} & \boldsymbol{q}_{2} & \cdots & \boldsymbol{q}_{n}\\
| & | &  & |
\end{array}\right) & = & \begin{pmatrix}\alpha_{1} & \beta_{2} &  &  & 0\\
\beta_{2}^{*} & \alpha_{2} & \beta_{3}\\
 & \beta_{3}^{*} & \alpha_{3} & \ddots\\
 &  & \ddots & \ddots & \beta_{n}\\
0 &  &  & \beta_{n}^{*} & \alpha_{n}
\end{pmatrix}\\
\alpha_{i} & = & \boldsymbol{q}_{i}^{\dagger}A\boldsymbol{q}_{i}\\
\beta_{i} & = & \boldsymbol{q}_{i-1}^{\dagger}A\boldsymbol{q}_{i}
\end{eqnarray*}

The $\boldsymbol{q}_{i}$are calculated iteratively by calculating $A\boldsymbol{q}_{i-1}$ and then subtracting the projection of $A\boldsymbol{q}_{i-1}$ on to the subspace spanned by $\left\{ \boldsymbol{q}_{0},\boldsymbol{q}_{1},\cdots,\boldsymbol{q}_{i-1}\right\} $, i.e.:
\begin{eqnarray*}
\boldsymbol{v}_{i+1} & = & A\boldsymbol{q}_{i}-\sum_{j=0}^{i}\left(\boldsymbol{q}_{j}^{\dagger}A\boldsymbol{q}_{i}\right)\boldsymbol{q}_{j}\\
\boldsymbol{q}_{i+1} & = & \boldsymbol{v}_{i+1}/\left\Vert \boldsymbol{v}_{i+1}\right\Vert 
\end{eqnarray*}
The $\alpha$s and $\beta$s are already by-products of this calculation and $H$can by immediately constructed. Note that the only usage of $A$ in this process is for calculating the $A\boldsymbol{q}_{i}$'s, this comes is as a great advantage is cases where $A$ is a sparse matrix or in cases where $A$ is not stored as a matrix but as a function operating on a vector. From here, the eigenvalues and vectors of the tridiagonal matrix $H$ can easily be calculated using a variety of low cost algorithms solving the tridiagonal matrix eigenvectors problem. The resulting eigenvalues of $H$ are also the eigenvalues of $A$ and the eigenvectors of $A$ can be calculated by multiplying the eigenvectors of $H$ by $Q$, i.e.
\begin{equation}
\mbox{if }\lambda_{j}w_{j}=Hw_{j}\mbox{ then }\lambda_{j}Qw_{j}=AQw_{j}
\end{equation}

This is the original algorithm suggested by Cornelius Lanczos which includes running the above procedure once for calculating the complete list of eigenvalues of $A$. In practice, this algorithm is not numerically stable since as we reach higher orders of $A^{k}\boldsymbol{b}$ the Krylov vectors tend to converge into the leading eigenvector of $A$ and become very similar to one another. The solution is to predefine a small number $m$ and to only use the subspace defined by the $m$ first Krylov vectors. The results would be an in complete matrix $\tilde{H}$ of size $m\times m$ and an incomplete matrix $\tilde{Q}$ of size $n\times m$. The resulting eigenvalues and vectors would not be the exact eigenvalues and vectors of $A$ be they would be a very good approximation of the $m$ leading ones, these values are called Ritz eigenvalues Ritz eigenvectors . The whole process can then be repeated after replacing the initial random vector $\boldsymbol{b}$ with the leading Ritz vector which was calculated in the last iteration. With each iteration, the Ritz values will converge into the $m$ leading eigenvalues and vectors of $A$.
