\chapter{The Method}
% ==================

\label{chapter:the_method}
\lhead{Chapter 2. \emph{The Method}}


\section{t-J modle on the gauged torus}
% ------------------

In our work we have studied the case of a 2 dimensional, $4\times4$ square system with periodic boundary conditions governed by the t-J model Hamiltonian (Eq. \eqref{eq:t_j_model}):
\begin{equation}
\mathcal{H}=-t\sum_{\left\langle i,j\right\rangle ,s}\left(c_{is}^{\dagger}c_{js}+c_{js}^{\dagger}c_{is}\right)+J\sum_{\left\langle i,j\right\rangle }\left(\boldsymbol{S}_{i}\cdot\boldsymbol{S}_{j}-\frac{n_{i}n_{j}}{4}\right) \nonumber
\end{equation}
We have used the method described in section \ref{section:berry_curvature} to calculate the system's Hall conductance. The periodic boundary conditions help in minimizing the edge effect which exist in such a small system. For the mapping of the 2D system on to the surface of a torus embedded in a 3D space, we have arbitrary selected $x$ to point in the direction circulating around the center of the torus and $y$ to point in the direction completing the smaller circle around the tube creating the torus.

Following the method from section \ref{section:berry_curvature}, we introduced a magnetic field perpendicular to the lattice surface and uniformly distributed across it. We have set the perpendicular magnetic field $B_z$ to obey:
\begin{equation}
\int_0^{L_x}\int_0^{L_y} B_z dxdy = B_z \cdot L_xL_y =\frac{h}{q}
\end{equation}
where $q$ is the fermion's charge and $L_x$ and $L_y$ are the dimensions of the lattice. In our case $L_x=L_y=4$. This guaranties that a fermion completing a full circle around the entire field will gain an insignificant phase of $2\pi$.

Since magnetic monopoles do not exist, the fluxes described above, coming out of the torus, must also be accompanied by fluxes running inside torus and a point at which they enters the torus, as illustrated in fig. \ref{fig:torus_with_flux}.
\begin{figure}[ht]
	\begin{center}
		\includegraphics{torus_with_flux2.png}
	\caption[The magnetic flux through the torus]{The magnetic flux entering the torus through a single point and coming out uniformly through the torus surface }
  	\label{fig:torus_with_flux}
	\end{center}
\end{figure}
The point at which the flux enters the torus has no significant since, as mentioned above, the total amount of flux was selected such that a path circulating this point will gain an insignificant phase of $2\pi$. However, the fluxes running through the torus are significant and control the phase gained by a fermions circling around the torus in the $x$ or $y$ direction (i.e. completing a circle passing through the periodic edges of the lattice) as illustrated in fig. \ref{fig:lattice_ab}. Such close paths are known as Wilson loops and the phase gained over such paths is gauge invariant. The phase gained over these paths is not constant and can be shown to have a linear dependency on $x$ or $y$ (depending on the direction of the paths). The phase gained over such closed paths is only relevant up to an offset of $2\pi n$.
\begin{figure}[!ht]
	\begin{center}
		\subfigure{\label{fig:lattice_ab-a}\includegraphics{lattice_ab.eps}}
		\subfigure{\label{fig:lattice_ab-b}\includegraphics{torus_ab2.png}}
	\end{center}
	\caption[Paths effected by the flux inside the torus]{Representative closed Wilson loops which are effected by the flux inside the torus and through the center of the torus}
  	\label{fig:lattice_ab}
\end{figure}

We have arbitrarily decided to mark the point at which both a closed loop in the $x$ direction and a closed loop in the $y$ direction will have a phase gain of $\pi+2\pi n$ as the entrance point, designated by $\left(x_\text{in},y_\text{in}\right)$. This corresponds to a magnetic flux which after entering the torus is equally distributed in all directions.

We go on and introduce two additional Aharonov-Bhom (AB) fluxes which run inside the torus and through the center of it, as described in section \ref{section:berry_curvature} and illustrated in fig \ref{fig:torus_with_flux1}. The magnetic vector potential we have chose to used for describing the magnetic fields in the system is the following:
\begin{subequations}
\begin{IEEEeqnarray}{rCl}
A_x\left(x,y;\Theta_x,\Theta_y\right) & = & \frac{\hbar}{q}\left(-\frac{2\pi}{L_x L_y}\delta\left(x\right)\left(y\bmod L_y\right)\cdot L_x + \frac{\Theta_y}{L_x}\right)
\\
A_y\left(x,y;\Theta_x,\Theta_y\right) & = & \frac{\hbar}{q}\left(\frac{2\pi}{L_x L_y}\left(x\bmod L_x\right) - \frac{\Theta_x}{L_y}\right) \label{eq:magnetic_potential}
\end{IEEEeqnarray}
\end{subequations}

From the above equations we can see that changing $\Theta_x$ and $\Theta_y$ has the same effect as adding an offset to $x$ and $y$ which is in practice is the same as moving $\left(x_\text{in},y_\text{in}\right)$ over the lattice. For $\Theta_x=\Theta_y=0$ we get that $\left(x_\text{in},y_\text{in}\right)=\left(0,0\right)$, and the relation between the AB fluxes and $\left(x_\text{in},y_\text{in}\right)$ is given by:
\begin{subequations}
\begin{IEEEeqnarray}{rCl}
x_\text{in} & = & \Theta_x\frac{L_x}{2\pi} 
\\
y_\text{in} & = & \Theta_y\frac{L_y}{2\pi}
\end{IEEEeqnarray}
\end{subequations}

We can therefore equivalently write the magnetic potential as:
\begin{subequations}
\begin{IEEEeqnarray}{rCl}
A_x\left(x,y;\Theta_x,\Theta_y\right) & = & -\frac{h}{q}\frac{1}{L_x L_y}\left(\delta\left(x\right)\left(y\bmod L_y\right)\cdot L_x-x_\text{in}\right)
\\
A_y\left(x,y;\Theta_x,\Theta_y\right) & = & \frac{h}{q}\frac{1}{L_x L_y}\left(\left(x\bmod L_x\right) - y_\text{in}\right) \label{eq:magnetic_potential2}
\end{IEEEeqnarray}
\end{subequations}
Fig. \ref{fig:lattice_null0} and fig. \ref{fig:lattice_null1} shows the magnetic potential over the lattice by plotting the phase a fermion gains when hoping from site to site for two selected values of $\left(x_{\text{in}},y_{\text{in}}\right)$. 
\begin{figure}[ht!]
	\begin{center}
		\subfigure[$\left(x_{\text{in}},y_{\text{in}}\right)=\left(0,0\right)$]{\label{fig:lattice_null-a}\includegraphics{lattice_null0.eps}}
	\end{center}
	\caption[Phase gain on the lattice]{The phase a fermion gains when moving over the lattice with $\left(x_{\text{in}},y_{\text{in}}\right)=\left(0,0\right)$}
  	\label{fig:lattice_null0}
\end{figure}
\begin{figure}[ht!]
	\begin{center}
		\subfigure[$\left(x_{\text{in}},y_{\text{in}}\right)=\left(1.5,1.5\right)$]{\label{fig:lattice_null-b}\includegraphics{lattice_null1.eps}}
	\end{center}
	\caption[Phase gain on the lattice]{The phase a fermion gains when moving over the lattice with $\left(x_{\text{in}},y_{\text{in}}\right)=\left(1.5,1.5\right)$}
  	\label{fig:lattice_null1}
\end{figure}

We incorporate the magnetic field into the t-J model's Hamiltonian through Peierls substitution resulting in the following Hamiltonian:
\begin{equation}
\mathcal{H}\left(\Theta_x,\Theta_y\right)=-t\sum_{\left\langle i,j\right\rangle ,s}\left(e^{iqA_{ij}}c_{js}^{\dagger}c_{is}+e^{iqA_{ji}}c_{is}^{\dagger}c_{js}\right)+J\sum_{\left\langle i,j\right\rangle }\left(\boldsymbol{S}_{i}\cdot\boldsymbol{S}_{j}-\frac{n_{i}n_{j}}{4}\right) \label{eq:our_t_j_model}
\end{equation}
Where $A_{ij}$ is the line integral over $\vec{A}\left(x,y\right)$ along a straight line connecting site $i$ and site $j$, as shown in the two examples in fig. \ref{fig:lattice_null0} and fig. \ref{fig:lattice_null1}.


\section{Calculation of the Berry Curvature through the use of Wilson Loops}
% -------------------------------------------------------------------------------

We turn to eq. \eqref{eq:berry_curve1} for the calculation of the Hall conductance. We note that the expression $\bra{\frac{\partial\Psi_0}{\partial\Theta_x}}\ket{\frac{\partial\Psi_0}{\partial\Theta_y}}$ describing the Berry curvature is not suitable for direct numerical calculation since it contains the derivatives of the ground state which cannot be directly calculated using numerical diagonalization. This is mainly due to the fact that when performing numeric diagonalization the resulting ground states is calculated with some arbitrary random global phase for each time the calculation is preformed. Luckily this equation can be 'massaged` a bit to make it suitable for numerical calculation. 

We start from the line integral form of eq. \eqref{eq:berry_curve1} (which is related to \eqref{eq:berry_curve1} through Stokes theorem) \cite{bernevig2013topological}:
\begin{equation}
\sigma_{xy}=\frac{q^{2}i}{\pi h}\oint_\mathcal{T}\bra{\Psi_0}\frac{\partial}{\partial\boldsymbol{\Theta}}\ket{\Psi_0} \cdot d\boldsymbol{\Theta} \label{eq:berry_curve2}
\end{equation}
Here $\mathcal{T}$ is a path running over the boundary of the integral in \eqref{eq:berry_curve1}.

To calculate the integral numerically we divide the path $\mathcal{T}$ into a series of small steps, denoted by $d\boldsymbol{\Theta}_i$, going from $\boldsymbol{\Theta}_i$ to $\boldsymbol{\Theta}_{i+1}$ as illustrated in fig. \ref{fig:theta_path}. We then approximate the integral using following relation:
\begin{subequations}
\begin{IEEEeqnarray}{rCl}
e^{-i\oint_\mathcal{T}i\bra{\Psi_0}\frac{\partial}{\partial\boldsymbol{\Theta}}\ket{\Psi_0} \cdot d\boldsymbol{\Theta}} 
& \approx & e^{\sum_i\bra{\Psi_0\left(\boldsymbol{\Theta}_i\right)}\frac{\partial}{\partial\boldsymbol{\Theta}}\ket{\Psi_0\left(\boldsymbol{\Theta}_i\right)} \cdot d\boldsymbol{\Theta}_i} 
\label{eq:wilson_loop_taylor_a}\\
& = & \prod_i e^{\bra{\Psi_0\left(\boldsymbol{\Theta}_i\right)}\frac{\partial}{\partial\boldsymbol{\Theta}}\ket{\Psi_0\left(\boldsymbol{\Theta}_i\right)} \cdot d\boldsymbol{\Theta}_i} 
\label{eq:wilson_loop_taylor_b}\\
& \approx & \prod_i \left(1+\bra{\Psi_0\left(\boldsymbol{\Theta}_i\right)}\frac{\partial}{\partial\boldsymbol{\Theta}}\ket{\Psi_0\left(\boldsymbol{\Theta}_i\right)} \cdot d\boldsymbol{\Theta}_i\right) 
\label{eq:wilson_loop_taylor_c}\\
& = & \prod_i \bra{\Psi_0\left(\boldsymbol{\Theta}_i\right)}\left(\ket{\Psi_0\left(\boldsymbol{\Theta}_i\right)}+\frac{\partial}{\partial\boldsymbol{\Theta}}\ket{\Psi_0\left(\boldsymbol{\Theta}_i\right)} \cdot d\boldsymbol{\Theta}_i\right) 
\label{eq:wilson_loop_taylor_d}\\
& \approx & \prod_i \bra{\Psi_0\left(\boldsymbol{\Theta}_i\right)}\ket{\Psi_0\left(\boldsymbol{\Theta}_i+d\boldsymbol{\Theta}_i\right)} 
\label{eq:wilson_loop_taylor_e}\\
& = & \prod_i \bra{\Psi_0\left(\boldsymbol{\Theta}_i\right)}\ket{\Psi_0\left(\boldsymbol{\Theta}_{i+1}\right)}
\label{eq:wilson_loop_taylor_f}
\end{IEEEeqnarray}
\label{eq:wilson_loop}
\end{subequations}

where in \eqref{eq:wilson_loop_taylor_a} we approximate the integral as a sum of small steps $d\Theta_i$, in \eqref{eq:wilson_loop_taylor_c} we have used the first order Taylor expansion of $e^x$ and in \eqref{eq:wilson_loop_taylor_e} we noticing that the expression in the parenthesis of \eqref{eq:wilson_loop_taylor_d} is the first order Taylor expansion of $\ket{\Psi_0\left(\boldsymbol{\Theta}\right)}$ around $\boldsymbol{\Theta}=\boldsymbol{\Theta}_{i}$, evaluated at $\boldsymbol{\Theta}=\boldsymbol{\Theta}_{i}+d\boldsymbol{\Theta}_i$. For a more detailed derivation see \cite{ryb2004combinatorial}

\begin{figure}[ht]
	\begin{center}
		\includegraphics{theta_path.eps}
	\end{center}
	\caption[A path in $\boldsymbol{\Theta}$ space]{A path in $\boldsymbol{\Theta}$ space constructed out of small steps $d\boldsymbol{\Theta}_i$}
  	\label{fig:theta_path}
\end{figure}

The result is that, given a closed path $\mathcal{T}$ composed out of $N$ small steps $d\boldsymbol{\Theta}_i$, the integral over the berry curvature in the area encapsulated inside the path can be approximated by
\begin{IEEEeqnarray}{rCl}
\IEEEeqnarraymulticol{3}{l}{
\oint_\mathcal{T}i\bra{\Psi_0}\frac{\partial}{\partial\boldsymbol{\Theta}}\ket{\Psi_0} \cdot d\boldsymbol{\Theta} 
}\nonumber\\
\qquad\qquad\qquad & \approx -\text{Arg}( &
\bra{\Psi_0\left(\boldsymbol{\Theta}_1\right)}\ket{\Psi_0\left(\boldsymbol{\Theta}_2\right)}\bra{\Psi_0\left(\boldsymbol{\Theta}_2\right)}\ket{\Psi_0\left(\boldsymbol{\Theta}_3\right)}
\cdots
\nonumber\\
&& 
\cdots\bra{\Psi_0\left(\boldsymbol{\Theta}_{N-1}\right)}\ket{\Psi_0\left(\boldsymbol{\Theta}_N\right)}\bra{\Psi_0\left(\boldsymbol{\Theta}_N\right)}\ket{\Psi_0\left(\boldsymbol{\Theta}_1\right)}
)
\label{eq:wilson_loop2}
\end{IEEEeqnarray}
where $\text{Arg}\left(re^{i\theta}\right)=\theta$.

The advantage of this method is that it does not include any derivatives. In addition, since all the ground states appear in the format of $\ket{\Psi_0\left(\boldsymbol{\Theta}_i\right)}\bra{\Psi_0\left(\boldsymbol{\Theta}_i\right)}$ any random phase added to a state is cancelled out and does not affect the final result.

To calculate the integral over the entire Berry curvature, we have divided the $\boldsymbol{\Theta}$ space into a grid of small closed rectangular loops, as illustrated in fig. \ref{fig:theta_path2}, and have calculated the integral over the Berry curvature at each loop using eq. \eqref{eq:wilson_loop2}.
\begin{figure}[ht]
	\begin{center}
		\includegraphics{theta_path2.eps}
	\end{center}
	\caption[Small Wilson loops covering the entire lattice ]{Small Wilson loops covering the entire lattice. In practice the only area which needs to be calculated is the area marked by the red rectangle}
  	\label{fig:theta_path2}
\end{figure}

In practice, it is enough to calculate the integral only for one quarter of a unit cell due to the symmetries of translation, $\left(x,y\right)\rightarrow\left(x+n,y+m\right)$, and reflection $\left(x,y\right)\rightarrow\left(-x,y\right)$ and $\left(x,y\right)\rightarrow\left(x,-y\right)$.

In theory, the reflection symmetry of $\left(x,y\right)\rightarrow\left(y,x\right)$ could have also been exploited but was not put to use in this work.


\section{Calculation of ground states}
% ------------------------------------
For the calculation of the ground states in eq. \eqref{eq:wilson_loop2} we have used exact diagonalization of the Hamiltonian. We have done so by applying the Lanczos algorithm to the Hamiltonian described in eq. \eqref{eq:our_t_j_model}. The Lanczos algorithm is a method for iteratively calculating the $n$ eigenstates of a given linear operator which correspond to the $n$ largest (in magnitude) eigenvalues of the operator. Where $n$ is a tunable parameter of the algorithm. The iterations in the algorithm are mainly composed of applying the operator over and over again to a randomly selected initial state vector. We provide a more detailed description of the Lanczos algorithm in appendix \ref{appendix:lanczos} along with a description of how to use the Lanczos algorithm for finding the eigenstates corresponding to the lowest algebraic eigenvalues.

The fact that the main operation in the algorithm is the repeated application of the Hamiltonian, makes this algorithm especially useful for sparse Hamiltonians, such as in the case of the t-J model.


\section{Adding next nearest neighbors (NNN) hopping terms}
% ---------------------------------------------------------
We have also calculated the Hall conductance of a system with an additional next nearest neighbors (NNN) hopping term, which is introduced to remove an undesired symmetry which exist in a $4\times4$ lattice. We describe this undesired symmetry in appendix \ref{appendix:limitations}.

In this work we refer to NNN only as the diagonal neighbors upon the lattice. The Hamiltonian with the NNN hopping term is given by:
\begin{IEEEeqnarray}{rCl}
\mathcal{H}\left(\Theta_x,\Theta_y\right) & = & -t\sum_{\left\langle i,j\right\rangle ,s}\left(e^{iA_{ij}}c_{js}^{\dagger}c_{is}+e^{iA_{ji}}c_{is}^{\dagger}c_{js}\right)-v\sum_{\left\langle\left\langle i,j\right\rangle\right\rangle ,s}\left(e^{iA_{ij}}c_{js}^{\dagger}c_{is}+e^{iA_{ji}}c_{is}^{\dagger}c_{js}\right)
\nonumber\\
&&
\qquad\qquad+J\sum_{\left\langle i,j\right\rangle }\left(\boldsymbol{S}_{i}\cdot\boldsymbol{S}_{j}-\frac{n_{i}n_{j}}{4}\right)
\end{IEEEeqnarray}
Where $\left\langle\left\langle,\right\rangle\right\rangle$ donates a sum over the diagonal next nearest neighbors connections.

