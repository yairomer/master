\chapter{The Hubbard Model and the \emph{t-J} Model}
% ===================================================

\label{appendix:tj_model}
\lhead{Appendix A. \emph{The Hubbard Model and the $t-J$ Model}}


The t-J model is a descendant of the more general Hubbard model. The Hubbard approximates the physics of electrons living on a lattice with Coulomb interactions between them and between them and the underlying lattice. In the Hubbard model the wave functions at each lattice site are approximated to be non-overlapping. The electron occupy the localized states at the lattice sites (for a full derivation of the Hubbard model see p.21 in Auerbach's textbook \cite{auerbach1994interacting}). The Hamiltonian describing the Hubbard Model is:
\begin{equation}
\mathcal{H}=-\sum_{ijs}t_{ij}c_{is}^{\dagger}c_{js}+U\sum_{i}n_{i\uparrow}n_{i\downarrow}
\end{equation}

The first term is a ``hopping'' term which represents the tunnelling of electrons from one site to a neighboring one. The second term is the on site electron-electron Coulomb repulsion. 

The $t-J$ Model is derived from the Hubbard model be taking the limit of $U/t\gg1$, i.e. it is an approximation for cases where the energy of the on site Coulomb repulsion is significantly stronger then the hopping (kinetic) energy. In this limit the system will strongly prefer states that have at most one electron per site. Under this assumption we can make an approximation, limiting the Hilbert space to the subspace of only one or none electrons per site and project the Hamiltonian into this subspace. When doing so, the Coulomb repulsion term does not completely vanishes but due to the superexchange effect the approximation yield a new anti-ferromagnetic coupling term. By applying the 
projection, and only keeping terms up to first order in $t/U$ we obtain the t-J model which is described by:

\begin{equation}
\mathcal{H}=\boldsymbol{P}\left[-t\sum_{\left\langle i,j\right\rangle ,s}\left(c_{is}^{\dagger}c_{js}+c_{js}^{\dagger}c_{is}\right)+J\sum_{\left\langle i,j\right\rangle }\left(\boldsymbol{S}_{i}\cdot\boldsymbol{S}_{j}-\frac{n_{i}n_{j}}{4}\right)\right]\boldsymbol{P}
\end{equation}

Here $\boldsymbol{P}$ is a projection operator, define as $\boldsymbol{P} = \prod_{i}\left(1-n_{i,\uparrow}n_{i,\downarrow}\right)$. This operator projects a general state into the the Hilbert subspace of states with at max a single electron per site. $J_{ij}$ is the anti-ferromagnetic coupling constant and is equal to $J_{ij}=4t_{ij}^{2}/U$. Although the $J$ term is of higher order in $t/U$, it could still be of higher significant due to the fact that close to half filling, which is the physically interesting case, the system inhibits more occupied sites, which interact through the $J$ term, then the number of fermions which are permitted to hop.

In fact, to first order in $t/U$ there exist another term of the form:
\begin{equation}
\mathcal{J}'=-\frac{t^2}{2U}\sum_{\left\langle i,j\right\rangle,\left\langle j,k\right\rangle ,s}\left(c_{is}^{\dagger}c_{ks}n_j+c_{ks}^{\dagger}c_{is}n_j+c_{i}^{\dagger}\vec{\sigma}c_{k}\cdot c_{j}^{\dagger}\vec{\sigma}c_{j}+c_{k}^{\dagger}\vec{\sigma}c_{i}\cdot c_{j}^{\dagger}\vec{\sigma}c_{j}\right)
\end{equation}
This term `hops' a fermion, passing over a neighboring fermion. This term is assumed to have a significantly smaller effect in comparison to the $t$ hopping term, and is frequently omitted in the most t-J model related works. The base for this assumption is first due to the fact that this term is of higher order in $t/U$ than the $t$ term, and therefore has less impact. A second reason is the fact that close to half filling the system's ground state inhabit an anti-ferromagnetic order. The $t$ term, which hops a fermion to a neighboring site, distorts this order while $\mathcal{J}'$ maintains the anti-ferromagnetic order even after the hop. 