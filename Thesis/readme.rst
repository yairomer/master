dependencies:
    Packages:
        TBA
    Files:
        lstpatch.sty (I thinks that it should have been included in the listings package, I am not sure why it is not)
    
Building into a build subdirectory with TeXMaker:
    In the Configuration window:
    
    - Mark the "Use a ""build"" subdirectory for output files
    - Change: "bibtex %" -> "bibtex build/% -include-directory=build"