- Get data from repository:
    - run:
.. code::
        mkdir ~/MyData
        git clone -v --progress --recursive https://yairomer@bitbucket.org/yairomer/master.git ~/MyData/Master

- Set connection startup script:
    - run:
.. code::
        echo "sh ~/MyData/Master/Scripts/ssh_login.sh" >> ~/.bash_profile 