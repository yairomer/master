import os
import sys
import time

import numpy as np
import pandas as pd
import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.ticker as mtick

from chern_numbers.auxil import mirror_data_frame as mirror
from chern_numbers.auxil.colormaps import viridis

if __name__ == "__main__":

    data_folder = '../../../LocalData/Data/'
    data_file = os.path.join(data_folder, 'non_interacting.h5')

    data = pd.read_hdf(data_file, 'data')

    grouped_data = data.groupby('n_particles')

    n_particles_list = sorted(grouped_data.indices.keys())
    chern_numbers_df = pd.DataFrame(columns=('filling', 'chern_number'))

    for n_particles in n_particles_list:

        data1 = grouped_data.get_group(n_particles)

        # Barrey curvature
        loops = mirror(data1.pivot(index='x_c', columns='y_c', values='wilson_loop'))
        x_grid, y_grid = np.meshgrid(loops.index.values, loops.columns.values)
        loops = loops.values
        loops_area = mirror(data1.pivot(index='x_c', columns='y_c', values='loop_area')).values

        chern_number = loops.sum() * 4*4/loops_area.sum()

        chern_numbers_df = chern_numbers_df.append({'filling': n_particles, 'chern_number' : chern_number},
                                                   ignore_index=True)

    out_file = os.path.join(data_folder, 'non_interacting_chern.h5')
    chern_numbers_df.to_hdf(out_file, 'data')



