import os

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import yaml

import chern_numbers

if __name__ == '__main__':
    plt.close('all')

    data_folder = '../../../LocalData/Data/'

    system_params = {
        'x_size': 4,
        'y_size': 4,
        'n_particles': 1,
        'n_up_spins': 1,
        'use_hilbert_space_lut_flag': True,

        'perp_flux_quanta': 1,

        't_coeff': 1,
        'j_coeff': 0,
        'v_coeff': 0,
        'ni_nj': True,
        'use_max_offset': False,
        'use_hamiltonian_matrix_flag': True,
    }

    calculation_params = {
        'project_on_k': False,
        'loop_path': None
    }

    dx = 0.05
    dy = 0.05

    x_vec = np.arange(0, 0.5, dx)
    y_vec = np.arange(0, 0.5, dy)

    loop_path = [[dx, 0], [dx, dy], [0, dy]]

    x_grid, y_grid = np.meshgrid(x_vec, y_vec)
    data = pd.DataFrame({'x': x_grid.flat, 'y': y_grid.flat})

    calculation_params['loop_path'] = loop_path
    for indx in data.index:
        system_params['null_point'] = (data.iloc[indx]['x'], data.iloc[indx]['y'])

        ground_state_values = chern_numbers.calc_ground_state_values_non_interacting(system_params, calculation_params,
                                                                                     ground_state_values_list=None)
        for key in ground_state_values.keys():
            if key not in data.columns:
                data[key] = np.nan

        data.update(pd.DataFrame([ground_state_values], index=[indx]))

    data['wilson_loops'] = data['wilson_loops'].map(lambda x: x.repeat(2))
    data['ground_energies'] = data['ground_energies'].map(lambda x: x.repeat(2))

    data_tmp1 = data
    data = data.iloc[:0].drop('wilson_loops', axis=1)
    for i, n_particles in enumerate(np.arange(1, len(data_tmp1['ground_energies'][0])-1)):
        data_tmp2 = data_tmp1.copy()
        data_tmp2['n_particles'] = int(n_particles)
        data_tmp2['wilson_loop'] = data_tmp1['wilson_loops'].map(lambda x: x[:(i+1)].sum())
        data_tmp2['ground_energies'] = data_tmp1['ground_energies'].map(lambda x: x[np.r_[i,i+2]])
        data_tmp2 = data_tmp2.drop('wilson_loops', axis=1)
        data = data.append(data_tmp2, ignore_index=True)

    out_file = os.path.join(data_folder, 'non_interacting.h5')
    data.to_hdf(out_file, 'data')
