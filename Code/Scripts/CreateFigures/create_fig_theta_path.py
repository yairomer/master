import os

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import patches

import chern_numbers

inch_to_cm = 2.54

if __name__ == '__main__':

    # Parameters
    # ==========
    width0 = 10/inch_to_cm
    save_flag = True
    dpi = 100

    system_params = {
        'x_size': 4,
        'y_size': 4,
        'n_particles': 1,
        'n_up_spins': 1,
        'use_hilbert_space_lut_flag': True,

        'perp_flux_quanta': 1,
        'null_point': [0, 0],

        't_coeff': 1,
        'j_coeff': 0.5,
        'v_coeff': 0,
        'ni_nj': True,
        'use_max_offset': True,
        'use_hamiltonian_matrix_flag': True,
    }

    # Files and folders
    # =================
    def _j(*args):
        if args[0][0] == '.':
            args = (os.path.dirname(__file__),) + args
        return os.path.realpath(os.path.join(*args))
    figures_folder = _j('../../../LocalData/Figures/')

    # Prepare data
    # ============
    lattice = chern_numbers.SquareLattice(**system_params)

    # Plot
    # ====
    plt.close('all')
    plt.ion()
    plt.rc('font', size=10)

    # Figure 1
    # --------
    width = width0
    heigth = width
    fig = plt.figure(figsize=(width,heigth),dpi=dpi)
    lattice_figure = {'fig': fig}
    lattice.plot_lattice(figure_objects=lattice_figure, grid_plot_args={'linestyle': 'solid'})

    axes = lattice_figure['axes']
    axes.set_xlabel('$\\Theta_x$')
    axes.set_ylabel('$\\Theta_y$')

    axes.set_xlim((-0.1, 0.4))
    axes.set_ylim((-0.1, 0.4))

    theta_path_color = (0.5, 0.5, 0.5)
    arraow_parms = dict(width=0.002, head_width=0.02, head_length=0.02,
                        fc=theta_path_color, ec=theta_path_color, alpha=1)
    marker_parms = dict(color=(0,0,0), markersize=15, markeredgewidth=1)
    text_parms = dict(verticalalignment='center', horizontalalignment='center')

    axes.plot(0.1, 0.1, 'x', **marker_parms)
    axes.text(0.07, 0.07, '$\Theta_1$', **text_parms)
    axes.arrow(0.13, 0.1, 0.13, 0, **arraow_parms)
    axes.text(0.2, 0.08, '$d\Theta_1$', **text_parms)
    axes.plot(0.3, 0.1, 'x', **marker_parms)
    axes.text(0.33, 0.07, '$\Theta_2$', **text_parms)
    axes.arrow(0.3, 0.13, 0, 0.13, **arraow_parms)
    axes.text(0.33, 0.2, '$d\Theta_2$', **text_parms)
    axes.plot(0.3, 0.3, 'x', **marker_parms)
    axes.text(0.33, 0.33, '$\Theta_3$', **text_parms)
    axes.arrow(0.27, 0.3, -0.13, 0, **arraow_parms)
    axes.text(0.2, 0.32, '$d\Theta_3$', **text_parms)
    axes.plot(0.1, 0.3, 'x', **marker_parms)
    axes.text(0.07, 0.33, '$\Theta_4$', **text_parms)
    axes.arrow(0.1, 0.27, 0, -0.13, **arraow_parms)
    axes.text(0.07, 0.2, '$d\Theta_4$', **text_parms)

    fig.tight_layout()
    if save_flag:
        out_file = os.path.join(figures_folder,'theta_path.eps')
        if not os.path.isdir(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        fig.savefig(out_file, dpi=dpi)

        out_file = os.path.join(figures_folder,'theta_path.png')
        fig.savefig(out_file, dpi=dpi)

    # Figure 2
    # --------
    width = width0
    heigth = width
    fig = plt.figure(figsize=(width,heigth),dpi=dpi)
    lattice_figure2 = {'fig': fig}
    lattice.plot_lattice(figure_objects=lattice_figure2, grid_plot_args={'linestyle': 'solid'})

    axes = lattice_figure2['axes']
    axes.set_xlabel('$\\Theta_x$')
    axes.set_ylabel('$\\Theta_y$')

    theta_path_color = (0.5, 0.5, 0.5)
    arraow_parms = dict(width=0.002, head_width=0.02, head_length=0.02,
                        fc=theta_path_color, ec=theta_path_color, alpha=1)
    marker_parms = dict(color=(0,0,0), markersize=15, markeredgewidth=1)
    text_parms = dict(verticalalignment='center', horizontalalignment='center')

    for i_x in np.arange(0.05, 4, 0.3):
        for i_y in np.arange(0.05, 4, 0.3):

            axes.arrow(i_x+0.03, i_y+0.0, 0.13, 0, **arraow_parms)
            axes.arrow(i_x+0.2, i_y+0.03, 0, 0.13, **arraow_parms)
            axes.arrow(i_x+0.17, i_y+0.2, -0.13, 0, **arraow_parms)
            axes.arrow(i_x+0.0, i_y+0.17, 0, -0.13, **arraow_parms)

    axes.add_patch(patches.Rectangle((0, 0), 0.5, 0.5, fill=False, alpha=0.6, linewidth=4, edgecolor='red'))

    fig.tight_layout()
    if save_flag:
        out_file = _j(figures_folder,'theta_path2.eps')
        if not os.path.isdir(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        fig.savefig(out_file, dpi=dpi)

        out_file = _j(figures_folder,'theta_path2.png')
        fig.savefig(out_file, dpi=dpi)

    # Figure 3
    # --------
    width = width0
    heigth = width
    fig = plt.figure(figsize=(width,heigth),dpi=dpi)
    lattice_figure3 = {'fig': fig}
    lattice.plot_lattice(figure_objects=lattice_figure3, grid_plot_args={'linestyle': 'solid'})

    axes = lattice_figure3['axes']
    axes.set_xlabel('$\\Theta_x$')
    axes.set_ylabel('$\\Theta_y$')

    theta_path_color = (0.5, 0.5, 0.5)
    arraow_parms = dict(width=0.002, head_width=0.02, head_length=0.02,
                        fc=theta_path_color, ec=theta_path_color, alpha=1)
    marker_parms = dict(color=(0,0,0), markersize=15, markeredgewidth=1)
    text_parms = dict(verticalalignment='center', horizontalalignment='center')

    for i_x in np.arange(0.05, 1, 0.3):
        for i_y in np.arange(0.05, 1, 0.3):

            axes.arrow(i_x+0.03, i_y+0.0, 0.13, 0, **arraow_parms)
            axes.arrow(i_x+0.2, i_y+0.03, 0, 0.13, **arraow_parms)
            axes.arrow(i_x+0.17, i_y+0.2, -0.13, 0, **arraow_parms)
            axes.arrow(i_x+0.0, i_y+0.17, 0, -0.13, **arraow_parms)

    fig.tight_layout()
    if save_flag:
        out_file = os.path.join(figures_folder,'theta_path3.eps')
        if not os.path.isdir(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        fig.savefig(out_file, dpi=dpi)

        out_file = os.path.join(figures_folder,'theta_path3.png')
        fig.savefig(out_file, dpi=dpi)


    # Figure 4
    # --------
    width = width0
    heigth = width
    fig = plt.figure(figsize=(width,heigth),dpi=dpi)
    lattice_figure4 = {'fig': fig}
    lattice.plot_lattice(figure_objects=lattice_figure4, grid_plot_args={'linestyle': 'solid'})

    axes = lattice_figure4['axes']
    axes.set_xlabel('$\\Theta_x$')
    axes.set_ylabel('$\\Theta_y$')

    theta_path_color = (0.5, 0.5, 0.5)
    arraow_parms = dict(width=0.002, head_width=0.02, head_length=0.02,
                        fc=theta_path_color, ec=theta_path_color, alpha=1)
    marker_parms = dict(color=(0,0,0), markersize=15, markeredgewidth=1)
    text_parms = dict(verticalalignment='center', horizontalalignment='center')

    for i_x in np.arange(0.05, 0.5, 0.3):
        for i_y in np.arange(0.05, 0.5, 0.3):

            axes.arrow(i_x+0.03, i_y+0.0, 0.13, 0, **arraow_parms)
            axes.arrow(i_x+0.2, i_y+0.03, 0, 0.13, **arraow_parms)
            axes.arrow(i_x+0.17, i_y+0.2, -0.13, 0, **arraow_parms)
            axes.arrow(i_x+0.0, i_y+0.17, 0, -0.13, **arraow_parms)

    fig.tight_layout()
    plt.show
    if save_flag:
        out_file = os.path.join(figures_folder,'theta_path4.eps')
        if not os.path.isdir(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        fig.savefig(out_file, dpi=dpi)

        out_file = os.path.join(figures_folder,'theta_path4.png')
        fig.savefig(out_file, dpi=dpi)
