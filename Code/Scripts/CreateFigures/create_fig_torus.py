import os

import numpy as np
from scipy import interpolate
from mayavi import mlab
import PIL


def plot_arrow(x, y, z, n_samples=None, **kargs):
    if n_samples is not None:
        t = np.r_[0, np.cumsum(np.sqrt(np.diff(x)**2+np.diff(y)**2+np.diff(z)**2))]
        f = interpolate.interp1d(t, np.c_[x, y, z], axis=0, kind='quadratic')
        t_interp = np.linspace(0, t[-1], n_samples)

        line_interp = f(t_interp)
        x = line_interp[:,0]
        y = line_interp[:,1]
        z = line_interp[:,2]

    if 'scale_factor' in kargs:
        scale_factor = 1
    else:
        scale_factor = 1

    mlab.plot3d(x, y, z) #, **kargs)

    (u, v, w) = (x[-1]-x[-2], y[-1]-y[-2], z[-1]-z[-2])
    (u, v, w) = np.array((u, v, w))/np.linalg.norm((u, v, w))

    mlab.quiver3d(x[-1], y[-1], z[-1], u, v, w, scale_factor=scale_factor, mode='arrow')



def plot_torus(fig, R=1, r=0.3, torus_color=(0.8, 0.8, 0.8), torus_opacity=0.3,
               n_sample_points=101):
    theta_vec = np.linspace(0, 1, n_sample_points)
    phi_vec = np.linspace(0, 1, n_sample_points)

    (theta_grid,phi_grid) = np.meshgrid(theta_vec, phi_vec)

    x = (R+r*np.cos(phi_grid*2*np.pi))*np.cos(theta_grid*2*np.pi)
    y = (R+r*np.cos(phi_grid*2*np.pi))*np.sin(theta_grid*2*np.pi)
    z = r*np.sin(phi_grid*2*np.pi)

    mlab.mesh(x, y, z, color=torus_color, opacity=torus_opacity)


def plot_lattice(fig, R=1, r=0.3, nx=4, ny=4, n_sample_points=101,
                 grid_color=(0, 0, 0), sites_color=(1, 0, 0)):
    theta_lattice_vec = np.linspace(0, 1, nx+1)[:-1]
    phi_lattice_vec = np.linspace(0, 1, ny+1)[:-1]

    (theta_grid,phi_grid) = np.meshgrid(theta_lattice_vec, phi_lattice_vec)

    phi_vec = np.linspace(0,1,n_sample_points)
    for theta in theta_lattice_vec:
        x = (R+r*np.cos(phi_vec*2*np.pi))*np.cos(theta*2*np.pi)
        y = (R+r*np.cos(phi_vec*2*np.pi))*np.sin(theta*2*np.pi)
        z = r*np.sin(phi_vec*2*np.pi)

        mlab.plot3d(x, y, z, color=grid_color, opacity=1, tube_radius=None)

    theta_vec = np.linspace(0,1,101)
    for phi in phi_lattice_vec:
        x = (R+r*np.cos(phi*2*np.pi))*np.cos(theta_vec*2*np.pi)
        y = (R+r*np.cos(phi*2*np.pi))*np.sin(theta_vec*2*np.pi)
        z = r*np.sin(phi*2*np.pi)*np.ones_like(theta_vec)

        mlab.plot3d(x, y, z, color=grid_color, opacity=1, tube_radius=None)

    x = (R+r*np.cos(phi_grid*2*np.pi))*np.cos(theta_grid*2*np.pi)
    y = (R+r*np.cos(phi_grid*2*np.pi))*np.sin(theta_grid*2*np.pi)
    z = r*np.sin(phi_grid*2*np.pi)

    mlab.points3d(x, y, z, color=sites_color, scale_factor=0.05 ,opacity=1)

def plot_flux1(fig, R=1, r=0.3, null_point = (0.35, 0.35),
              flux_color = (0.4, 0.7, 0.8)):
    ## Plot Flux
    #  ---------
    for theta_a in np.linspace(-0.5+1/2./8., 0.5-1/2./8., 8):
        for phi0 in np.linspace(0, 1-1/4., 4):

            theta2 = theta_a+null_point[0]
            phi2 = phi0+null_point[1]

            x2 = (R+0.9*r*np.cos(phi2*2*np.pi ))*np.cos(theta2*2*np.pi)
            y2 = (R+0.9*r*np.cos(phi2*2*np.pi))*np.sin(theta2*2*np.pi)
            z2 = 0.9*r*np.sin(phi2*2*np.pi)*np.ones_like(theta2)

            x3 = (R+r*np.cos(phi2*2*np.pi ))*np.cos(theta2*2*np.pi)
            y3 = (R+r*np.cos(phi2*2*np.pi))*np.sin(theta2*2*np.pi)
            z3 = r*np.sin(phi2*2*np.pi)*np.ones_like(theta2)

            (u, v, w) = (x3-x2, y3-y2, z3-z2)
            (u, v, w) = np.array((u, v, w))/np.linalg.norm((u, v, w))

            mlab.quiver3d(x2, y2, z2, u, v, w, color=flux_color, scale_factor=0.2, mode='arrow')

def plot_flux2(fig, R=1, r=0.3, null_point = (0.35, 0.35),
              flux_color = (0.4, 0.7, 0.8)):
    ## Plot Flux
    #  ---------
    null_point = np.array(null_point)
    x0 = (R+r*np.cos(null_point[1]*np.pi ))*np.cos(null_point[0]*2*np.pi)
    y0 = (R+r*np.cos(null_point[1]*np.pi))*np.sin(null_point[0]*2*np.pi)
    z0 = r*np.sin(null_point[1]*np.pi)*np.ones_like(null_point[0])

    x1 = (R+0.9*r*np.cos(null_point[1]*np.pi ))*np.cos(null_point[0]*2*np.pi)
    y1 = (R+0.9*r*np.cos(null_point[1]*np.pi))*np.sin(null_point[0]*2*np.pi)
    z1 = 0.9*r*np.sin(null_point[1]*np.pi)*np.ones_like(null_point[0])


    (u, v, w) = (x1-x0, y1-y0, z1-z0)
    mlab.quiver3d(x0-10*u, y0-10*v, z0-10*w, u, v, w, color=flux_color, scale_factor=10, line_width=10, mode='arrow')

    for theta_a in np.linspace(-0.5+1/2./8., 0.5-1/2./8., 8):
        for phi0 in np.linspace(0, 1-1/4., 4):

            r0 = 0.4*r*(1-2*np.abs(theta_a))

            theta2 = theta_a+null_point[0]
            phi2 = phi0+null_point[1]

            #theta_vec = np.linspace(null_point[0]+0.05*(1-2*np.abs(theta_a))*np.sign(theta_a), theta2-0.01*np.sign(theta_a), 21)
            theta_vec = np.linspace(null_point[0]+0.03*np.sign(theta_a), theta2-0.03*np.sign(theta_a), 51)

            x2 = (R+0.9*r*np.cos(phi2*2*np.pi ))*np.cos(theta2*2*np.pi)
            y2 = (R+0.9*r*np.cos(phi2*2*np.pi))*np.sin(theta2*2*np.pi)
            z2 = 0.9*r*np.sin(phi2*2*np.pi)*np.ones_like(theta2)

            x3 = (R+r*np.cos(phi2*2*np.pi ))*np.cos(theta2*2*np.pi)
            y3 = (R+r*np.cos(phi2*2*np.pi))*np.sin(theta2*2*np.pi)
            z3 = r*np.sin(phi2*2*np.pi)*np.ones_like(theta2)


            x = np.concatenate(((x0, x1), (R+r0*np.cos(phi2*2*np.pi ))*np.cos((theta_vec)*2*np.pi), (x2,x3)))
            y = np.concatenate(((y0, y1), (R+r0*np.cos(phi2*2*np.pi))*np.sin(theta_vec*2*np.pi), (y2,y3)))
            z = np.concatenate(((z0, z1), r0*np.sin(phi2*2*np.pi)*np.ones_like(theta_vec), (z2,z3)))

            t1 = np.r_[0, np.cumsum(np.sqrt(np.diff(x)**2+np.diff(y)**2+np.diff(z)**2))]
            f = interpolate.interp1d(t1, np.c_[x, y, z], axis=0, kind='quadratic')

            t2 = np.linspace(0,t1[-1],1e2)
            line_smooth = f(t2)
            x_smooth = line_smooth[:,0]
            y_smooth = line_smooth[:,1]
            z_smooth = line_smooth[:,2]

            mlab.plot3d(x_smooth, y_smooth, z_smooth, color=flux_color, tube_radius=r*0.02)


def plot_ab1(fig, R=1, r=0.3, theta = 0, phi = 0.5):
    ## AB path
    #  -------
    ab_path_color = (0.4, 0.7, 0.8)

    theta0 = 1

    theta_vec = theta0 + np.linspace(0, 0.75, 51)

    x = R*np.cos(theta_vec*2*np.pi)
    y = R*np.sin(theta_vec*2*np.pi)
    z = 0*np.ones_like(theta_vec)

    mlab.plot3d(x, y, z, color=ab_path_color, tube_radius=0.03)

    (u, v, w) = (x[-1]-x[-2], y[-1]-y[-2], z[-1]-z[-2])
    (u, v, w) = np.array((u, v, w))/np.linalg.norm((u, v, w))
    mlab.quiver3d(x[-1]-0.3*u, y[-1]-0.3*v, z[-1]-0.2*w, u, v, w, color=ab_path_color, scale_factor=0.9, mode='arrow')


    x = np.array([0])
    y = np.array([0])
    z = np.array([0.6])
    (u, v, w) = 0, 0, -1
    (u, v, w) = np.array((u, v, w))/np.linalg.norm((u, v, w))
    mlab.quiver3d(x[0], y[0], z[0], u, v, w, color=ab_path_color, scale_factor=0.9, mode='arrow')


def plot_ab2(fig, R=1, r=0.3, theta = 0, phi = 0.5):
    ## AB path
    #  -------
    ab_path_color = (0.9, 0.64, 0.09)

    phi0 = 0.1
    r0 = 1.1*r

    phi_vec = phi0 + np.linspace(0, 0.9, 51)

    x = (R+r0*np.cos(phi_vec*2*np.pi ))*np.cos(theta*2*np.pi)
    y = (R+r0*np.cos(phi_vec*2*np.pi))*np.sin(theta*2*np.pi)
    z = r0*np.sin(phi_vec*2*np.pi)

    mlab.plot3d(x, y, z, color=ab_path_color, tube_radius=0.01)
    (u, v, w) = (x[-1]-x[-2], y[-1]-y[-2], z[-1]-z[-2])
    (u, v, w) = np.array((u, v, w))/np.linalg.norm((u, v, w))
    mlab.quiver3d(x[-1], y[-1], z[-1], u, v, w, color=ab_path_color, scale_factor=0.3, mode='arrow')


    theta0 = -0.1
    r0 = 0.9*r

    theta_vec = theta0 + np.linspace(0, 0.9, 51)

    x = (R+r0*np.cos(phi*2*np.pi ))*np.cos(theta_vec*2*np.pi)
    y = (R+r0*np.cos(phi*2*np.pi))*np.sin(theta_vec*2*np.pi)
    z = r0*np.sin(phi*2*np.pi)*np.ones_like(theta_vec)

    mlab.plot3d(x, y, z, color=ab_path_color, tube_radius=0.01)

    (u, v, w) = (x[-1]-x[-2], y[-1]-y[-2], z[-1]-z[-2])
    (u, v, w) = np.array((u, v, w))/np.linalg.norm((u, v, w))
    mlab.quiver3d(x[-1], y[-1], z[-1], u, v, w, color=ab_path_color, scale_factor=0.3, mode='arrow')


def plot_loop(fig, R=1, r=0.3, theta = 0, phi = 0.5, dtheta=0.02):
    ## AB path
    #  -------
    ab_path_color = (0.9, 0.64, 0.09)

    phi0 = 0.1
    r0 = 1.1*r

    phi_vec = phi0 + np.linspace(0.01, 0.99, 51)

    x = (R+r0*np.cos(phi_vec*2*np.pi ))*np.cos(theta*2*np.pi)
    y = (R+r0*np.cos(phi_vec*2*np.pi))*np.sin(theta*2*np.pi)
    z = r0*np.sin(phi_vec*2*np.pi)

    phi_vec = np.flipud(phi_vec)

    x = np.concatenate((x, (R+r0*np.cos(phi_vec*2*np.pi ))*np.cos((theta+dtheta)*2*np.pi)))
    y = np.concatenate((y, (R+r0*np.cos(phi_vec*2*np.pi))*np.sin((theta+dtheta)*2*np.pi)))
    z = np.concatenate((z, r0*np.sin(phi_vec*2*np.pi)))

    x = np.concatenate((x, x[0:1]))
    y = np.concatenate((y, y[0:1]))
    z = np.concatenate((z, z[0:1]))

    mlab.plot3d(x, y, z, color=ab_path_color, tube_radius=0.01)


if __name__ == '__main__':

    figures_folder = '../../../LocalData/Figures/'
    inch_to_cm = 2.54
    width0 = 10  # [cm]
    save_flag = True
    dpi = 200

    mlab.close(all=True)

    width = width0/inch_to_cm*dpi
    heigth = width

    fig = mlab.figure( bgcolor=(1, 1, 1), size=(width, heigth))
    plot_torus(fig)
    plot_lattice(fig)
    mlab.view(59.131688183623773, 46.333297791730359, 5.076713094515017, np.array([ 0.10338066,  0.14440405, -0.08600698]))
    if save_flag:
        out_file = os.path.join(figures_folder, 'torus.png')
        mlab.savefig(out_file, figure=fig)
        img = PIL.Image.open(out_file)
        img.save(out_file, dpi=[dpi, dpi])



    fig = mlab.figure( bgcolor=(1, 1, 1), size=(width, heigth))
    plot_torus(fig)
    plot_lattice(fig)
    plot_ab1(fig, )
    mlab.view(59.131688183623773, 46.333297791730359, 5.076713094515017, np.array([ 0.10338066,  0.14440405, -0.08600698]))
    if save_flag:
        out_file = os.path.join(figures_folder, 'torus_ab1.png')
        mlab.savefig(out_file, figure=fig)
        img = PIL.Image.open(out_file)
        img.save(out_file, dpi=[dpi, dpi])


    fig = mlab.figure( bgcolor=(1, 1, 1), size=(width, heigth))
    plot_torus(fig)
    plot_lattice(fig)
    plot_ab2(fig, )
    mlab.view(59.131688183623773, 46.333297791730359, 5.076713094515017, np.array([ 0.10338066,  0.14440405, -0.08600698]))
    if save_flag:
        out_file = os.path.join(figures_folder, 'torus_ab2.png')
        mlab.savefig(out_file, figure=fig)
        img = PIL.Image.open(out_file)
        img.save(out_file, dpi=[dpi, dpi])


    fig = mlab.figure( bgcolor=(1, 1, 1), size=(width, heigth))
    plot_torus(fig)
    plot_lattice(fig)
    plot_loop(fig, )
    mlab.view(59.131688183623773, 46.333297791730359, 5.076713094515017, np.array([ 0.10338066,  0.14440405, -0.08600698]))
    if save_flag:
        out_file = os.path.join(figures_folder, 'torus_linear.png')
        mlab.savefig(out_file, figure=fig)
        img = PIL.Image.open(out_file)
        img.save(out_file, dpi=[dpi, dpi])


    fig = mlab.figure( bgcolor=(1, 1, 1), size=(width, heigth))
    plot_torus(fig)
    #plot_lattice(fig)
    plot_flux1(fig)
    plot_ab1(fig, )
    mlab.view(59.131688183623773, 46.333297791730359, 5.076713094515017, np.array([ 0.10338066,  0.14440405, -0.08600698]))
    if save_flag:
        out_file = os.path.join(figures_folder, 'torus_with_flux1.png')
        mlab.savefig(out_file, figure=fig)
        img = PIL.Image.open(out_file)
        img.save(out_file, dpi=[dpi, dpi])


    fig = mlab.figure( bgcolor=(1, 1, 1), size=(width, heigth))
    plot_torus(fig)
    plot_lattice(fig)
    plot_flux1(fig)
    plot_flux2(fig)
    mlab.view(59.131688183623773, 46.333297791730359, 5.076713094515017, np.array([ 0.10338066,  0.14440405, -0.08600698]))
    if save_flag:
        out_file = os.path.join(figures_folder, 'torus_with_flux2.png')
        mlab.savefig(out_file, figure=fig)
        img = PIL.Image.open(out_file)
        img.save(out_file, dpi=[dpi, dpi])



