#!/usr/bin/python

import os
import sys
import numpy as np
import pandas as pd
import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.ticker as mtick

import auxil.colormaps as cmaps
import auxil.processing as processing

if __name__ == "__main__":

    save_flag = False
    data_folder = '../../../LocalData/Data/'
    figures_folder = '../../../LocalData/Figures/'
    data_file = os.path.join(data_folder, 'non_interacting.h5')

    plt.close('all')

    v_coeff = 0;
    # v_coeff = 0.05;
    # v_coeff = 0.5;

    data_file = os.path.join(data_folder,r'chern_numbers_v{:.2f}'.format(v_coeff).replace('.',  '_') + r'.h5')

    chern_numbers_df = pd.read_hdf(data_file,'data')

    data = chern_numbers_df.pivot(index='filling', columns='t_over_j', values='chern_number')
    data2 = chern_numbers_df.pivot(index='filling', columns='t_over_j', values='gap')
    data.values[np.isnan(data2.values)] = np.nan
    if v_coeff == 0:
        data.loc[10][(data.columns > 3.15) & (data.columns < 4.6)] = np.nan
        data.loc[12][(data.columns > 4.6)] = np.nan
    elif v_coeff == 0.5:
        data.loc[10][(data.columns > 3.95)] = np.nan
        data.loc[12][(data.columns > 4.6)] = np.nan
    elif v_coeff == 0.05:
        data.loc[12][(data.columns > 4.6)] = np.nan
    data = data.sort_index(axis=0)
    data = data.sort_index(axis=1)
    [x ,y] = np.meshgrid(data.columns.values, data.index.values)
    values = data.values

    x_min = data.columns.values.min()
    x_max = 3 #data.columns.values.max()
    y_min = 0.5 #data.index.values.min()-0.5
    y_max = data.index.values.max()+0.5


    width = 900
    heigth = 600
    dpi = 100
    fig = plt.figure(figsize=(width/dpi,heigth/dpi),dpi=dpi)
    fig.suptitle('Chern Number', fontsize=14)
    axes = fig.add_subplot(1, 1, 1)
    axes.set_xlim(x_min, x_max)
    #axes.set_axis_bgcolor((1,1,1))
    axes.fill((x_min, x_max, x_max, x_min), (y_min, y_min, y_max, y_max), 'w', hatch='xxx', zorder=0)
    axes.set_xlabel('t / J', fontsize=20)
    axes.set_ylabel('# of Fermions', fontsize=20)
    axes.xaxis.set_tick_params(labelsize=15)
    axes.yaxis.set_tick_params(labelsize=15)
    axes.xaxis.set_major_formatter(mtick.FormatStrFormatter('%0.2f'))
    #axes4.yaxis.set_major_formatter(mtick.FormatStrFormatter('%0.2f'))

    chern_img = axes.imshow(np.flipud(values), vmin=-32, vmax=32,
                             extent=[data.columns.values.min(), data.columns.values.max(), data.index.values.min()-0.5, data.index.values.max()+0.5],
                             aspect='auto',  interpolation ='nearest',
                             cmap=plt.get_cmap('BrBG',2**16)
                             # cmap=cmaps.viridis,
                             )
    plt.colorbar(chern_img, ax=axes)
    axes.set_ylim((y_min, y_max))

    for i_fill in data.index.values:
        axes.plot((x_min, x_max),(i_fill-0.47,)*2, 'k', linewidth=0.5)

    if v_coeff == 0.0:
        axes.text(1.75, 1, '1', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 2, '2', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 3, '3', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 4, '4', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 5, '5', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 6, '6', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 7, '7', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 8, '8', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 9, '9', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(0.9, 10, '-6', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 10, '10', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(4.8, 10, '10', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 11, '11', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 12, '-4', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(0.6, 13, '-3', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(0.85, 13, '29', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.5, 13, '-3', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(2.6, 13, '13', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(4.8, 13, '-3', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(0.8, 14, '-2', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.3, 14, '-18', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 14, '14', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(0.45, 15, '15', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(0.65, 15, '-1', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(0.85, 15, '15', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.2, 15, '-17', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 15, '15', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(3.7, 15, '-1', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(4.5, 15, '-17', size=10, verticalalignment='center', horizontalalignment='center')
    elif v_coeff == 0.5:
        axes.text(1.75, 1, '1', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 2, '2', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 3, '3', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 4, '4', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 5, '5', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 6, '6', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 7, '7', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 8, '8', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 9, '9', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 9, '9', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1, 10, '-6', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 10, '10', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 11, '11', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 12, '-4', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 13, '13', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 14, '-2', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.75, 15, '15', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(2.95, 15, '-17', size=10, verticalalignment='center', horizontalalignment='center')
    elif v_coeff == 0.05:
        axes.text(2.75, 11, '11', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(2.75, 12, '-4', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(0.65, 13, '13', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(0.88, 13, '29', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.3, 13, '-3', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(2.75, 13, '13', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.4, 14, '-2', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(2.75, 14, '14', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.1, 15, '15', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(1.85, 15, '-1', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(2.75, 15, '15', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(3.8, 15, '-1', size=10, verticalalignment='center', horizontalalignment='center')
        axes.text(4.7, 15, '-17', size=10, verticalalignment='center', horizontalalignment='center')

    if save_flag:
        out_file = os.path.join(figures_folder+'phase_diagram_v{:.2f}'.format(v_coeff).replace('.',  '_') + '.png')
        fig.savefig(out_file, dpi=dpi)

