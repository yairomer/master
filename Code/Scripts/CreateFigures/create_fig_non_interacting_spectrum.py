#!/usr/bin/python

import os
import sys
import time
import fractions

import yaml
import numpy as np
import pandas as pd
import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.ticker as mtick

import auxil.colormaps as cmaps
import auxil.processing as processing

inch_to_cm = 2.54

if __name__ == "__main__":

    def _j(*args):
        return os.path.realpath(os.path.join(*args))

    file_folder = _j(os.path.dirname(__file__))

    data_folder = _j(file_folder, '../../../LocalData/Data/')
    data2_folder = _j(file_folder, '../../../LocalData/Master_org/Data/runs/')
    figures_folder = _j(file_folder, '../../../LocalData/Figures/')
    chern_data_file = _j(data_folder, 'chern_numbers_v0_00.h5')
    data_file1 = _j(data2_folder, './Run4x4_K/results.h5')
    data_file2 = _j(data2_folder, './Run4x4_K_7to11/results.h5')

    #width0 = 17
    save_flag = False
    dpi = 150

    v_coeff = 0.00
    t_coeff = 1.00

    group_by_list = ('numParticles')

    plt.close('all')
    plt.ion()
    plt.rc('font', size=10)

    width = 15/inch_to_cm
    heigth = 17/inch_to_cm
    fig1 = plt.figure(figsize=(width,heigth),dpi=dpi)
    axes1 = fig1.add_subplot(1, 1, 1)

    axes1.set_title('Energy spectrum', fontsize=12)
    axes1.set_xlabel('# fermions')
    axes1.set_ylabel('Total energy / # fermions')
    axes1.set_xlim(-3, 19)
    axes1.set_ylim(-4.6, -0.5)

    axes1.grid(b=True)


    data1 = pd.read_hdf(data_file1,'results')
    data2 = pd.read_hdf(data_file2,'results')
    data = pd.concat((data1, data2))
    data = data[(data['vCoeff']==v_coeff) & (data['tCoeff']==t_coeff)]
    grouped_data = data.groupby(group_by_list)


    color_list = ['r', 'b']

    plot_args = [dict(linewidth=4),
                 dict(linewidth=2),
                 ]
    pos_list = [[30, -115],
                [-50, 15],
                ]
    line = [None for _ in range(2)]
    for n_particles in sorted(grouped_data.indices.keys()):

        data1 = grouped_data.get_group(n_particles)

        data2 = data1.copy()
        data2['groundEnergies'] = data2['groundEnergies'].map(lambda x: x[0])

        for i in np.arange(1,4):
            data_tmp = data1.copy()
            data_tmp['groundEnergies'] = data_tmp['groundEnergies'].map(lambda x: x[i])
            data2 = data2.append(data_tmp)

        data2.set_index('groundEnergies', inplace=True)
        data2.sort_index(inplace=True)

        def set_arrow_values(x, y, dx, dy, m1=0.45, m2=0.15):
            l = float(dx**2+dy**2)**0.5
            m1, m2 = 1-m1/l, 1-(m1+m2)/l
            x, y, dx, dy = x+m1*dx, y+m1*dy, -m2*dx, -m2*dy
            return x, y, dx, dy

        for i,energy in enumerate(data2.index[:2]):
            line[i] = axes1.plot(n_particles+np.array((-0.3, 0.3)), energy*np.array((1, 1))/n_particles, color=color_list[i], **plot_args[i])[0]
            text= 'k=({},{})\ns={}'.format(fractions.Fraction(data2.loc[energy]['kx']),
                                           fractions.Fraction(data2.loc[energy]['ky']),
                                           fractions.Fraction(np.round((np.sqrt(data2.loc[energy]['totalSpin']+0.25)-0.5)*2)/2.)
                                           )
            arrowprops=dict(arrowstyle="->", connectionstyle="angle,angleA=0,angleB=90,rad=10", color=color_list[i], alpha=0.2, linewidth=2)
            #xytext = (n_particles+pos_list[i][0], energy/n_particles+pos_list[i][1]+7*n_particles)
            if i==0:
                xytext = (310+0*n_particles-50*(n_particles%2), -370+16*n_particles)
            else:
                xytext = (80+0*n_particles-50*(n_particles%2), -257+16*n_particles)
            axes1.annotate(s=text,
                           xy=(n_particles, energy/n_particles), xycoords='data',
                           xytext=xytext , textcoords='axes points',
                           verticalalignment='center', horizontalalignment='center', color=color_list[i],
                           fontsize=9,
                           arrowprops=arrowprops,
                           )
#            axes1.text(n_particles+pos_list[i][0], energy/n_particles+pos_list[i][1], text,
#                       verticalalignment='center', horizontalalignment='center', color=color_list[i])
#            x, y, dx, dy = set_arrow_values(n_particles, energy/n_particles, pos_list[i][0], pos_list[i][1])
#            axes1.arrow(x, y, dx, dy,
#                        linewidth=3,
#                        alpha=0.5,
#                        color=color_list[i])

        plt.show()
        axes1.legend(line, ('Ground State Energy', 'First Exsited State Energy'), loc='lower left', fontsize=10)

    if save_flag:
        out_file = os.path.join(figures_folder,'energy_spectrum.png')
        if not os.path.isdir(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        fig1.savefig(out_file, dpi=dpi)

        out_file = os.path.join(figures_folder,'energy_spectrum.eps')
        fig1.savefig(out_file, dpi=dpi)

