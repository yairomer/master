import os

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

inch_to_cm = 2.54

if __name__ == "__main__":

    # Parameters
    # ==========
    save_flag = True
    dpi = 100

    # Files and folders
    # =================
    def _j(*args):
        if args[0][0] == '.':
            args = (os.path.dirname(__file__),) + args
        return os.path.realpath(os.path.join(*args))

    data_folder = _j('../../../LocalData/Data/')
    data2_folder = _j('../../../LocalData/Master_org/Data/runs/')
    figures_folder = _j('../../../LocalData/Figures/')
    data_file = _j(data_folder, 'non_interacting_chern.h5')

    # Prepare data
    # ============
    resultsK    = pd.DataFrame()
    resultsK    = resultsK.append(pd.read_hdf('D:/Dropbox/MasterData/Run4x4_K/results.h5','results'),ignore_index=True)
    resultsK    = resultsK.append(pd.read_hdf('D:/Dropbox/MasterData/Run4x4_K_7to11/results.h5','results'),ignore_index=True)
    resultsK    = resultsK.append(pd.read_hdf('D:/Dropbox/MasterData/Run4x4_K16/results.h5','results'),ignore_index=True)

    ###############################################
    v_coeff = 0.00
    resultsK = resultsK[(resultsK['vCoeff']==v_coeff)]

    ###############################################
    tmpFunc = np.vectorize(lambda x: x[0] if type(x)==np.ndarray else x)

    resultsK.groundEnergies = resultsK.groundEnergies.map(lambda x: x[0])

    ################################################
    tmp = resultsK.groupby(['numParticles','tCoeff'])
    groudEnergyK = tmp['groundEnergies'].min().reset_index()


    # Plot
    # ====
    plt.close('all')
    plt.ion()
    plt.rc('font', size=10)

    width = 15/inch_to_cm
    height = 10/inch_to_cm
    fig1 = plt.figure(figsize=(width, height), dpi=dpi)
    fig1.suptitle('', fontsize=12)

    axes1 = fig1.add_subplot(1, 1, 1)
    #axes1.set_title('$\\frac{\\partial^2(Ground\\ Energy)}{\\partial(\\#\\ fermions)^2}$\n', fontsize=25)
    axes1.set_xlabel('# of fermions', fontsize=10)
    axes1.set_ylabel('$\Delta_{pb}$', fontsize=13)
    axes1.set_ylim(-1.2, 0.5)
    axes1.set_ylim(-1.2, 0.5)


    t_over_j_list = np.arange(0.5, 5, 1)
    colorList = ['b','g','r','c','m','y','k','b','g']
    for i,itCoeff in enumerate(t_over_j_list):
        val = groudEnergyK[groudEnergyK['tCoeff']==itCoeff].set_index('numParticles')['groundEnergies']
        x = val.index.values
        val = val.values

        val = np.diff(val, 2)
        x = x[1:-1]

        axes1.plot(x[1::2], val[1::2], 'x'+colorList[i])
    plt.grid('on')

    axes1.plot(axes1.get_xlim(), (0, 0), 'k', linewidth=2)

    axes1.text(9, -1.1, '4 pair\nof Holes', size=10, verticalalignment='center', horizontalalignment='center')
    axes1.text(11, -1.1, '3 pair\nof Holes', size=10, verticalalignment='center', horizontalalignment='center')
    axes1.text(13, -1.1, '2 pair\nof Holes', size=10, verticalalignment='center', horizontalalignment='center')
    axes1.text(15, -1.1, '1 pair\nof Holes', size=10, verticalalignment='center', horizontalalignment='center')

    #plt.title(r'$\frac{\partiald^2(Ground Energy)}{\partial(# fermions)^2}$')
    #plt.title('$\\frac{\\partial^2(Ground\\ Energy)}{\\partial(\\#\\ fermions)^2}$\n', fontsize=25)
    plt.title('$\Delta_{pb}$', fontsize=15)
    plt.legend(['t/J={}'.format(n) for n in t_over_j_list], fontsize=10, loc='lower left')
    #plt.gca().set_position((0.125, 0.1, 0.775, 0.7))


    if save_flag:
        out_file = os.path.join(figures_folder, r'second_derivative{:.2f}'.format(v_coeff).replace('.',  '_') + r'.png')
        if not os.path.isdir(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        fig1.savefig(out_file, dpi=dpi)

        out_file = os.path.join(figures_folder, r'second_derivative{:.2f}'.format(v_coeff).replace('.',  '_') + r'.eps')
        fig1.savefig(out_file, dpi=dpi)


