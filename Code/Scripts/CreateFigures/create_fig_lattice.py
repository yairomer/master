import os
import time

import numpy as np
import matplotlib.pyplot as plt
import fractions

import chern_numbers

inch_to_cm = 2.54

if __name__ == '__main__':

    figures_folder = '../../../LocalData/Figures/'
    save_flag = True
    dpi = 100

    plt.close('all')
    plt.ion()

    system_params = {
        'x_size': 4,
        'y_size': 4,
        'n_particles': 1,
        'n_up_spins': 1,
        'use_hilbert_space_lut_flag': True,

        'perp_flux_quanta': 1,
        'null_point': [0, 0],

        't_coeff': 1,
        'j_coeff': 0.5,
        'v_coeff': 0,
        'ni_nj': True,
        'use_max_offset': True,
        'use_hamiltonian_matrix_flag': True,
    }

    calculation_params = {
        'num_of_energies': 3,
        'num_of_lanczos_vectors': 10,
        'iterations': None,
        'tolerance': 1e-5,
        'project_on_k': False,
    }

    lattice = chern_numbers.SquareLattice(**system_params)

    width = 10/inch_to_cm
    heigth = width
    fig = plt.figure(figsize=(width,heigth),dpi=dpi)
    lattice_figure = {'fig': fig}
    lattice.plot_lattice(figure_objects=lattice_figure, grid_plot_args={'linestyle': 'solid'})
    lattice_figure['axes'].set_xlabel('X', fontsize=10)
    lattice_figure['axes'].set_ylabel('Y', fontsize=10)
    lattice_figure['axes'].tick_params(axis='both', labelsize=10)

    if save_flag:
        out_file = os.path.join(figures_folder,'lattice.eps')
        if not os.path.isdir(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        fig.savefig(out_file, dpi=dpi)

        out_file = os.path.join(figures_folder,'lattice.png')
        fig.savefig(out_file, dpi=dpi)

    width = 10/inch_to_cm
    heigth = width
    fig = plt.figure(figsize=(width,heigth),dpi=dpi)
    lattice_figure = {'fig': fig}
    lattice.plot_lattice(figure_objects=lattice_figure, grid_plot_args={'linestyle': 'solid'})
    lattice_figure['axes'].set_xlabel('X', fontsize=10)
    lattice_figure['axes'].set_ylabel('Y', fontsize=10)
    lattice_figure['axes'].tick_params(axis='both', labelsize=10)

    ab_path_color = (0.9, 0.64, 0.09)
    arraow_parms = dict(width=0.03, head_width=0.2, head_length=0.1,
                        fc=ab_path_color, ec=ab_path_color, alpha=0.7)

    lattice_figure['axes'].arrow(1, 1, lattice.x_size-1, 0, **arraow_parms)
    lattice_figure['axes'].arrow(-1, 1, 1.5, 0, **arraow_parms)
    lattice_figure['axes'].arrow(2, 2, 0, lattice.y_size-2, **arraow_parms)
    lattice_figure['axes'].arrow(2, -1, 0, 2.5, **arraow_parms)

    if save_flag:
        out_file = os.path.join(figures_folder,'lattice_ab.eps')
        if not os.path.isdir(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        fig.savefig(out_file, dpi=dpi)

        out_file = os.path.join(figures_folder,'lattice_ab.png')
        fig.savefig(out_file, dpi=dpi)



    width = 10/inch_to_cm
    heigth = width
    fig = plt.figure(figsize=(width,heigth),dpi=dpi)
    lattice_figure = {'fig': fig}
    lattice.plot_lattice(figure_objects=lattice_figure, grid_plot_args={'linestyle': 'solid'})
    lattice_figure['axes'].set_xlabel('X', fontsize=10)
    lattice_figure['axes'].set_ylabel('Y', fontsize=10)
    lattice_figure['axes'].tick_params(axis='both', labelsize=10)

    ab_path_color = (0.9, 0.64, 0.09)
    lattice_figure['axes'].arrow(1, -0.5, 0, lattice.y_size-0.1, **arraow_parms)
    lattice_figure['axes'].arrow(1, lattice.y_size-0.5, 0.3-0.1, 0, **arraow_parms)
    lattice_figure['axes'].arrow(1.3, lattice.y_size-0.5, 0, -lattice.y_size+0.1, **arraow_parms)
    lattice_figure['axes'].arrow(1.3, -0.5, -0.3+0.1, 0, **arraow_parms)
    if save_flag:
        out_file = os.path.join(figures_folder,'lattice_linear.eps')
        if not os.path.isdir(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        fig.savefig(out_file, dpi=dpi)

        out_file = os.path.join(figures_folder,'lattice_linear.png')
        fig.savefig(out_file, dpi=dpi)



    magnetic_field = chern_numbers.MagneticField(**system_params)
    phases = magnetic_field.calc_phase(lattice.nn_xy_list[:, 0, :], lattice.nn_xy_list[:, 1, :])
    phases = (phases+np.pi) % (2*np.pi) - np.pi
    text = [('${}\\cdot2\pi$\n'.format(fractions.Fraction(abs(val)/2/np.pi)) if val != 0 else '') for val in phases]

    width = 15/inch_to_cm
    heigth = width
    fig = plt.figure(figsize=(width,heigth),dpi=dpi)
    lattice_figure = {'fig': fig}
    lattice.plot_lattice(figure_objects=lattice_figure, grid_plot_args={'linestyle': 'solid'})
    lattice_figure['axes'].plot(system_params['null_point'][0], system_params['null_point'][1], 'xr')
    lattice.plot_path(lattice.nn_xy_list[:, 0, :], lattice.nn_xy_list[:, 1, :],
                      values=phases/2/np.pi, text=text,
                      path_plot_args={'color': (0, 0, .4), 'alpha': 0.8, 'headwidth':8, 'width':0.03},
                      text_plot_args={'color': (0, 0, 0), 'size': 10},
                      data_name='nn_pairs', figure_objects=lattice_figure,
                      )
    lattice_figure['axes'].set_xlabel('X', fontsize=10)
    lattice_figure['axes'].set_ylabel('Y', fontsize=10)
    lattice_figure['axes'].tick_params(axis='both', labelsize=10)
    lattice_figure['axes'].plot(magnetic_field.null_point[0], magnetic_field.null_point[1], 'x', color=(0,0,0.3), markersize=15, markeredgewidth=2)
    lattice_figure['axes'].text(magnetic_field.null_point[0]+0, magnetic_field.null_point[1]-0.35,
                                '$\\left(x_{in},y_{in}\\right)$',
                                size=10, horizontalalignment='center')
    if save_flag:
        out_file = os.path.join(figures_folder,'lattice_null0.eps')
        if not os.path.isdir(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        fig.savefig(out_file, dpi=dpi)

        out_file = os.path.join(figures_folder,'lattice_null0.png')
        fig.savefig(out_file, dpi=dpi)


    system_params['null_point'] = (1.5, 1.5)
    magnetic_field = chern_numbers.MagneticField(**system_params)
    phases = magnetic_field.calc_phase(lattice.nn_xy_list[:, 0, :], lattice.nn_xy_list[:, 1, :])
    phases = (phases+np.pi) % (2*np.pi) - np.pi
    text = [('${:.3f}\\cdot2\pi$\n'.format(abs(val)/2/np.pi) if val != 0 else '') for val in phases]
    #text = [(''.format(fractions.Fraction(abs(val)/2/np.pi)) if val != 0 else '') for val in phases]

    width = 15/inch_to_cm
    heigth = width
    fig = plt.figure(figsize=(width,heigth),dpi=dpi)
    lattice_figure = {'fig': fig}
    lattice.plot_lattice(figure_objects=lattice_figure, grid_plot_args={'linestyle': 'solid'})
    lattice_figure['axes'].plot(system_params['null_point'][0], system_params['null_point'][1], 'xr')
    lattice.plot_path(lattice.nn_xy_list[:, 0, :], lattice.nn_xy_list[:, 1, :],
                      values=phases/2/np.pi, text=text,
                      path_plot_args={'color': (0, 0, .4), 'alpha': 0.8, 'headwidth':8, 'width':0.03},
                      text_plot_args={'color': (0, 0, 0), 'size': 10},
                      data_name='nn_pairs', figure_objects=lattice_figure,
                      )
    lattice_figure['axes'].set_xlabel('X', fontsize=10)
    lattice_figure['axes'].set_ylabel('Y', fontsize=10)
    lattice_figure['axes'].tick_params(axis='both', labelsize=10)
    lattice_figure['axes'].plot(magnetic_field.null_point[0], magnetic_field.null_point[1], 'x', color=(0,0,0.3), markersize=15, markeredgewidth=2)
    lattice_figure['axes'].text(magnetic_field.null_point[0]+0, magnetic_field.null_point[1]+0.35,
                                '$\\left(x_{in},y_{in}\\right)$',
                                size=10, horizontalalignment='center')
    if save_flag:
        out_file = os.path.join(figures_folder,'lattice_null1.eps')
        if not os.path.isdir(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        fig.savefig(out_file, dpi=dpi)

        out_file = os.path.join(figures_folder,'lattice_null1.png')
        fig.savefig(out_file, dpi=dpi)

