import numpy as np
import pandas as pd

def extract_berry_curv(data):
    x_correlation = data.pivot(index='nullPointY', columns='nullPointX', values='xCorrelation')
    y_correlation = data.pivot(index='nullPointY', columns='nullPointX', values='yCorrelation')

    null_point_x_vec = x_correlation.columns
    null_point_y_vec = x_correlation.index

    integrand = np.angle(x_correlation.values[:-1, :-1]
                         * y_correlation.values[:-1, 1:]
                         * x_correlation.values[1:, :-1].conj()
                         * y_correlation.values[:-1, :-1].conj()
                         )/2/np.pi

    null_point_x_vec_c = pd.Index((null_point_x_vec.values[1:]+null_point_x_vec.values[:-1])/2, name='nullPointX')
    null_point_y_vec_c = pd.Index((null_point_y_vec.values[1:]+null_point_y_vec.values[:-1])/2, name='nullPointY')

    dx = null_point_x_vec[1] - null_point_x_vec[0]
    dy = null_point_y_vec[1] - null_point_y_vec[0]
    integrand = pd.DataFrame(integrand, columns=null_point_x_vec_c, index=null_point_y_vec_c)/dx/dy

    null_point_x_size = null_point_x_vec[-1] - null_point_x_vec[0]
    null_point_y_size = null_point_y_vec[-1] - null_point_y_vec[0]
    grid_area = null_point_x_size*null_point_y_size

    x_size = data['xSize'].iat[0]
    y_size = data['ySize'].iat[0]
    n_sites = x_size*y_size

    chern_number = integrand.sum().sum()*dx*dy/grid_area*n_sites

    return integrand, chern_number
