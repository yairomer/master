#!/usr/bin/python

import os
import numpy as np
import pandas as pd
import matplotlib.pylab as plt
import matplotlib.ticker as mtick

inch_to_cm = 2.54

if __name__ == "__main__":

    # Parameters
    # ==========
    save_flag = True

    dpi = 100
    titles_size = 11
    lables_size = 11
    font_size = 10
    fig1_size = (7/inch_to_cm, 14/inch_to_cm)
    fig2_size = (7/inch_to_cm, 7/inch_to_cm)

    # Files and folders
    # =================
    def _j(*args):
        if args[0][0] == '.':
            args = (os.path.dirname(__file__),) + args
        return os.path.realpath(os.path.join(*args))

    data_folder = _j('../../../LocalData/Data/')
    figures_folder = _j('../../../LocalData/Figures/')
    data_file = _j(data_folder, 'non_interacting_chern.h5')

    # Prepare data
    # ============
    chern_numbers_df = pd.read_hdf(data_file,'data')

    data = chern_numbers_df.set_index('filling')
    data = data.sort_index(axis=0)

    data['chern_number'] = np.array((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, -15, -14, -13, -12, -11, -10, -9, -8, -7, -6, -5, -4, -3, -2))[:,np.newaxis]
    data.loc[31] = -1

    y = data.index.values
    values = data.values

    x_min = 0
    x_max = 1
    y_min = y.min()-0.5
    y_max = y.max()+0.5


    # Plot
    # ====
    plt.close('all')

    # Figure 1
    # --------
    fig = plt.figure(figsize=fig1_size,dpi=dpi)
    fig.suptitle('Chern Number\nNon-Interacting Model', fontsize=titles_size)
    axes = fig.add_subplot(1, 1, 1)
    #axes.set_axis_bgcolor((1,1,1))
    #axes.fill((x_min, x_max, x_max, x_min), (y_min, y_min, y_max, y_max), 'w', hatch='xxx', zorder=0)
    axes.set_ylabel('# of Fermions', fontsize=lables_size)
    #axes.xaxis.set_tick_params(labelsize=font_size)
    axes.yaxis.set_tick_params(labelsize=font_size)
    axes.xaxis.set_major_locator(plt.NullLocator())
    #axes.xaxis.set_major_formatter(mtick.FormatStrFormatter('%0.2f'))
    #axes4.yaxis.set_major_formatter(mtick.FormatStrFormatter('%0.2f'))

    chern_img = axes.imshow(np.flipud(values), vmin=-32, vmax=32,
                             extent=[x_min, x_max, y_min, y.max()+0.5],
                             aspect='auto',  interpolation ='nearest',
                             cmap=plt.get_cmap('BrBG',2**16)
                             )
    plt.colorbar(chern_img, ax=axes)
    axes.set_ylim((y_min, y_max))

    for i_fill in data.index.values:
        axes.plot((x_min, x_max),(i_fill-0.47,)*2, 'k', linewidth=0.5)

    axes.text(0.5, 1, '1', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 2, '2', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 3, '3', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 4, '4', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 5, '5', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 6, '6', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 7, '7', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 8, '8', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 9, '9', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 10, '10', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 11, '11', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 12, '12', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 13, '13', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 14, '14', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 15, '15', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 16, '0', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 17, '-15', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 18, '-14', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 19, '-13', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 20, '-12', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 21, '-11', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 22, '-10', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 23, '-9', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 24, '-8', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 25, '-7', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 26, '-6', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 27, '-5', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 28, '-4', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 29, '-3', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 30, '-2', size=font_size, verticalalignment='center', horizontalalignment='center')
    axes.text(0.5, 31, '-1', size=font_size, verticalalignment='center', horizontalalignment='center')

    axes.set_position((0.2, 0.1, 0.545, 0.8))

    if save_flag:
        out_file = _j(figures_folder, r'phase_diagram_non_interacting.png')
        if not os.path.isdir(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        fig.savefig(out_file, dpi=dpi)

    # Figure 2
    # --------
    fig = plt.figure(figsize=fig2_size,dpi=dpi)
    axes = fig.add_subplot(1, 1, 1)
    axes.set_xlabel('# of Fermions', fontsize=lables_size)
    axes.set_ylabel('Chern number', fontsize=lables_size)
    axes.xaxis.set_tick_params(labelsize=font_size)
    axes.yaxis.set_tick_params(labelsize=font_size)

    axes.plot(y, data, 'd-')
    axes.set_position((0.125, 0.2, 0.775, 0.7))
    plt.grid('on')

    if save_flag:
        out_file = _j(figures_folder, r'chern_non_interacting.png')
        if not os.path.isdir(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        fig.savefig(out_file, dpi=dpi)


