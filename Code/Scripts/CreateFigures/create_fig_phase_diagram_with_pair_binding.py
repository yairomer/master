#!/usr/bin/python

import os
import numpy as np
import pandas as pd
import matplotlib.pylab as plt
import matplotlib.ticker as mtick

inch_to_cm = 2.54

if __name__ == "__main__":

    # Parameters
    # ==========
    save_flag = True

    v_coeff = 0;
    # v_coeff = 0.05;
    # v_coeff = 0.5;

    dpi = 100
    titles_size = 12
    lables_size = 12
    font_size = 10
    fig1_size = (14/inch_to_cm, 9/inch_to_cm)
    fig2_size = (14/inch_to_cm, 9/inch_to_cm)
    fig3_size = (14/inch_to_cm, 9/inch_to_cm)
    x_lim = [0.5, 3]

    t_J_ratio = 1

    # Files and folders
    # =================
    def _j(*args):
        if args[0][0] == '.':
            args = (os.path.dirname(__file__),) + args
        return os.path.realpath(os.path.join(*args))

    data_folder = _j('../../../LocalData/Data/')
    figures_folder = _j('../../../LocalData/Figures/')
    data_file =_j(data_folder, 'non_interacting.h5')
    data_file_energy_1 = 'D:/Dropbox/MasterData/Run4x4_K/results.h5'
    data_file_energy_2 = 'D:/Dropbox/MasterData/Run4x4_K_7to11/results.h5'

    # Prepare data
    # ============
    data_energy_1 = pd.read_hdf(data_file_energy_1,'results')
    data_energy_2 = pd.read_hdf(data_file_energy_2,'results')
    data_energy = pd.concat((data_energy_1, data_energy_2))
    data_energy = data_energy[data_energy['vCoeff']==v_coeff]
    tmpFunc = np.vectorize(lambda x: x[0] if type(x)==np.ndarray else x)
    data_energy['groundEnergies'] = data_energy['groundEnergies'].map(lambda x: x[0])
    tmp = data_energy.groupby(['numParticles','tCoeff'])
    data_energy = tmp['groundEnergies'].min().reset_index()
    data_energy = data_energy.pivot(index='numParticles', columns='tCoeff', values='groundEnergies')

    pair_binding = np.diff(data_energy, 2, axis=0)[1::2]


    data_file = os.path.join(data_folder,r'chern_numbers_v{:.2f}'.format(v_coeff).replace('.',  '_') + r'.h5')
    chern_numbers_df = pd.read_hdf(data_file,'data')

    data = chern_numbers_df.pivot(index='filling', columns='t_over_j', values='chern_number')
    data2 = chern_numbers_df.pivot(index='filling', columns='t_over_j', values='gap')
    data.values[np.isnan(data2.values)] = np.nan
    if v_coeff == 0:
        data.loc[10][(data.columns > 3.15) & (data.columns < 4.6)] = np.nan
        data.loc[12][(data.columns > 4.6)] = np.nan
    elif v_coeff == 0.5:
        data.loc[10][(data.columns > 3.95)] = np.nan
        data.loc[12][(data.columns > 4.6)] = np.nan
    elif v_coeff == 0.05:
        data.loc[12][(data.columns > 4.6)] = np.nan
    data = data.sort_index(axis=0)
    data = data.sort_index(axis=1)
    data = data.iloc[1::2]
    [x ,y] = np.meshgrid(data.columns.values, data.index.values)
    values = data.values

    x_min = data.columns.values.min()
    x_max = data.columns.values.max()
    y_min = data.index.values.min()-1
    y_max = data.index.values.max()+1

    # Plot
    # ====
    plt.close('all')

    # Figure 1
    # --------
    fig1 = plt.figure(figsize=fig1_size,dpi=dpi)
    fig1.suptitle('Chern Number', fontsize=titles_size)
    fig1.subplots_adjust(bottom=0.15)
    axes1 = fig1.add_subplot(1, 1, 1)
    #axes1.set_axis_bgcolor((1,1,1))
    axes1.fill((x_min, x_max, x_max, x_min), (y_min, y_min, y_max, y_max), 'w', hatch='xxx', zorder=0)
    axes1.set_xlabel('t / J', fontsize=lables_size)
    axes1.set_ylabel('# of Fermions', fontsize=lables_size)
    axes1.xaxis.set_tick_params(labelsize=font_size)
    axes1.yaxis.set_tick_params(labelsize=font_size)
    axes1.xaxis.set_major_formatter(mtick.FormatStrFormatter('%0.2f'))
    #axes1.yaxis.set_major_formatter(mtick.FormatStrFormatter('%0.2f'))
    axes1.set_xlim(x_lim)

    chern_img = axes1.imshow(np.flipud(values), vmin=-32, vmax=32,
                             extent=[data.columns.values.min(), data.columns.values.max(), data.index.values.min()-1, data.index.values.max()+1],
                             aspect='auto',  interpolation ='nearest',
                             cmap=plt.get_cmap('BrBG',2**16)
                             # cmap=cmaps.viridis,
                             )
    plt.colorbar(chern_img, ax=axes1)
    axes1.set_ylim((y_min, y_max))

    for i_fill in data.index.values:
        axes1.plot((x_min, x_max),(i_fill-0.96,)*2, 'k', linewidth=0.5)

    if save_flag:
        out_file = _j(figures_folder,r'chern_diagram_even_v{:.2f}'.format(v_coeff).replace('.',  '_') + r'.png')
        if not os.path.isdir(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        fig1.savefig(out_file, dpi=dpi)

#        out_file = _j(figures_folder,r'chern_diagram_even_v{:.2f}'.format(v_coeff).replace('.',  '_') + r'.eps')
#        fig1.savefig(out_file, dpi=dpi)

    # Figure 2
    # --------
    fig2 = plt.figure(figsize=fig2_size,dpi=dpi)
    fig2.suptitle('$\Delta_{pb}$', fontsize=titles_size+2)
    fig2.subplots_adjust(bottom=0.15)
    axes2 = fig2.add_subplot(1, 1, 1)
    #axes2.set_axis_bgcolor((1,1,1))
    axes2.set_xlabel('t / J', fontsize=lables_size)
    axes2.set_ylabel('# of Fermions', fontsize=lables_size)
    axes2.xaxis.set_tick_params(labelsize=font_size)
    axes2.yaxis.set_tick_params(labelsize=font_size)
    axes2.xaxis.set_major_formatter(mtick.FormatStrFormatter('%0.2f'))
    #axes2.yaxis.set_major_formatter(mtick.FormatStrFormatter('%0.2f'))
    axes2.set_xlim(x_lim)

    pair_binding_img = axes2.imshow(np.flipud(pair_binding), vmin=-1, vmax=-0.3, alpha=0.6,
                             extent=[data.columns.values.min(), data.columns.values.max(), data.index.values.min()+1, data.index.values.max()+1],
                             aspect='auto',  interpolation ='nearest',
                             cmap=plt.get_cmap('Reds_r',2**16)
                             # cmap=cmaps.viridis,
                             )

    plt.colorbar(pair_binding_img, ax=axes2)

    if save_flag:
        out_file = _j(figures_folder,r'pair_binding_diagram_v{:.2f}'.format(v_coeff).replace('.',  '_') + r'.png')
        if not os.path.isdir(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        fig2.savefig(out_file, dpi=dpi)

#        out_file = _j(figures_folder,r'pair_binding_diagram_v{:.2f}'.format(v_coeff).replace('.',  '_') + r'.eps')
#        fig2.savefig(out_file, dpi=dpi)



    # Figure 3
    # --------
    fig3 = plt.figure(figsize=fig3_size,dpi=dpi)
    fig1.suptitle('Chern Number at t/J={}'.format(t_J_ratio), fontsize=titles_size)
    fig1.subplots_adjust(bottom=0.15)
    axes3 = fig3.add_subplot(1, 1, 1)
    #axes3.set_axis_bgcolor((1,1,1))
    axes3.set_xlabel('# of pairs of holes', fontsize=lables_size)
    axes3.set_ylabel('Chern Number', fontsize=lables_size)
    axes3.xaxis.set_tick_params(labelsize=font_size)
    axes3.yaxis.set_tick_params(labelsize=font_size)
    axes3.set_ylim([-7, 9])
    axes3.grid('on')

    fill_indx = data.index[data.index.values % 2 == 0].values.astype(int)
    t_J_indx = np.nonzero(data.columns == t_J_ratio)
    axes3.plot((16-fill_indx)/2, data[t_J_ratio].loc[fill_indx].values, 'd-')
    axes3.set_position((0.125, 0.2, 0.775, 0.7))

    if save_flag:
        out_file = _j(figures_folder,r'holes_chern_v{:.2f}'.format(v_coeff).replace('.',  '_') + r'.png')
        if not os.path.isdir(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        fig3.savefig(out_file, dpi=dpi)
