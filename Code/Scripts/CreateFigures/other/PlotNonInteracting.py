import os
import sys
import time

import numpy as np
import pandas as pd
import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.ticker as mtick

from scipy import stats

from chern_numbers.auxil import mirror_data_frame as mirror
from chern_numbers.auxil.colormaps import viridis

if __name__ == "__main__":

    data_folder = '../../../LocalData/Data/'
    figures_folder = '../../../LocalData/Figures/'
    data_file = os.path.join(data_folder, 'non_interacting.h5')

    # Create figure
    # -------------
    plt.close('all')

    width = 800
    height = 400
    dpi = 100
    fig1 = plt.figure(figsize=(width/dpi, height/dpi), dpi=dpi)
    fig1.suptitle('', fontsize=14)

    axes1 = fig1.add_subplot(1, 2, 1, projection='3d')
    axes1.set_title('Berry Curvature', fontsize=10)
    axes1.set_xlabel('\nNull point y', fontsize=8)
    axes1.set_ylabel('\nNull point x', fontsize=8)
    axes1.set_xlim([-0.5, 0.5])
    axes1.set_ylim([-0.5, 0.5])
    axes1.xaxis.set_ticks(np.linspace(-0.5, 0.5, 4))
    axes1.yaxis.set_ticks(np.linspace(-0.5, 0.5, 4)[1:])
    axes1.xaxis.set_major_formatter(mtick.FormatStrFormatter('%0.2f'))
    axes1.yaxis.set_major_formatter(mtick.FormatStrFormatter('%0.2f'))
    axes1.zaxis.set_major_formatter(mtick.FormatStrFormatter('      %0.2f'))
    axes1.xaxis.set_tick_params(labelsize=8)
    axes1.yaxis.set_tick_params(labelsize=8)
    axes1.zaxis.set_tick_params(labelsize=8)
    barry_surf = None

    axes2 = fig1.add_subplot(1, 2, 2, projection='3d')
    axes2.view_init(elev=12, azim=-35)
    axes2.set_title('Ground Energy\nand First Excited State Energy', fontsize=10)
    axes2.set_xlabel('\nNull point y', fontsize=8)
    axes2.set_ylabel('\nNull point x', fontsize=8)
    axes2.set_zlabel('\nEnergy', fontsize=8)
    axes2.set_xlim([-0.5, 0.5])
    axes2.set_ylim([-0.5, 0.5])
    axes2.xaxis.set_ticks(np.linspace(-0.5, 0.5, 4))
    axes2.yaxis.set_ticks(np.linspace(-0.5, 0.5, 4)[1:])
    axes2.xaxis.set_major_formatter(mtick.FormatStrFormatter('%0.2f'))
    axes2.yaxis.set_major_formatter(mtick.FormatStrFormatter('%0.2f'))
    axes2.zaxis.set_major_formatter(mtick.FormatStrFormatter('      %0.4f'))
    axes2.xaxis.set_tick_params(labelsize=8)
    axes2.yaxis.set_tick_params(labelsize=8)
    axes2.zaxis.set_tick_params(labelsize=8)
    ground_energy_surf = None
    first_energy_surf = None

    data = pd.read_hdf(data_file, 'data')

    grouped_data = data.groupby('n_particles')

    n_particles_list = sorted(grouped_data.indices.keys())

    for n_particles in n_particles_list[:0]:

        print('n_particles = {}'.format(n_particles))

        data1 = grouped_data.get_group(n_particles)

        # plot Barrey curvature
        loops = mirror(data1.pivot(index='x_c', columns='y_c', values='wilson_loop'))
        x_grid, y_grid = np.meshgrid(loops.index.values, loops.columns.values)
        loops = loops.values
        loops_area = mirror(data1.pivot(index='x_c', columns='y_c', values='loop_area')).values

        chern_number = loops.sum() * 4*4/loops_area.sum()

        if barry_surf is not None:
            barry_surf.remove()

        barry_surf = axes1.plot_surface(x_grid, y_grid, loops/loops_area,
                                        rstride=1, cstride=1, cmap=viridis, linewidth=0)
        fig1.suptitle('filling = {:d} - Chern Number = {:d}'.format(int(n_particles), int(round(chern_number))), fontsize=14)
        start, end = axes1.get_zlim()
        axes1.zaxis.set_ticks(np.linspace(start, end, 4))

        # plot ground energy
        ground_energy = mirror(data1.pivot(index='x', columns='y', values='ground_energies').applymap(lambda x: x[0]))
        first_energy = mirror(data1.pivot(index='x', columns='y', values='ground_energies').applymap(lambda x: x[1]))
        x_grid, y_grid = np.meshgrid(ground_energy.index.values, ground_energy.columns.values)
        ground_energy = ground_energy.values
        first_energy = first_energy.values

        if ground_energy_surf is not None:
            ground_energy_surf.remove()
            first_energy_surf.remove()

        ground_energy_surf = axes2.plot_surface(x_grid, y_grid, ground_energy,
                                                rstride=1, cstride=1, cmap=viridis, linewidth=0)
        first_energy_surf = axes2.plot_surface(x_grid, y_grid, first_energy,
                                               rstride=1, cstride=1, cmap=viridis, linewidth=0)
        start, end = axes2.get_zlim()
        axes2.zaxis.set_ticks(np.linspace(start, end, 4))

        plt.show()

        out_file = os.path.join(figures_folder, r'NonInteracting/{:02d}p'.format(int(n_particles)) + '.eps')
        if not os.path.isdir(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        fig1.savefig(out_file, dpi=dpi)
        out_file = os.path.join(figures_folder, r'NonInteracting/{:02d}p'.format(int(n_particles)) + '.png')
        fig1.savefig(out_file, dpi=dpi)

        width = 800


    heigth = 600
    dpi = 100
    stats
    fig = plt.figure(figsize=(width/dpi,heigth/dpi),dpi=dpi)
    axes = fig.add_subplot(1, 1, 1)
    axes.set_xlabel('Energy', fontsize=20)
    #axes.set_ylabel('', fontsize=20)
    axes.xaxis.set_tick_params(labelsize=10)
    axes.yaxis.set_tick_params(labelsize=10)

    energies = data.loc[(data['y']==0) & (data['x']==0)]['ground_energies'].map(lambda x: x[0])
    energies = np.r_[energies[0], np.diff(energies)]
    energies = energies[::2]
    x = np.linspace(energies[0],energies[-2],100)
    tmp = stats.gaussian_kde(energies[::2])(x)
    axes.plot(x, tmp)
    plt.show()
    plt.grid('on')
