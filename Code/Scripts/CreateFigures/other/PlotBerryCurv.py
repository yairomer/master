#!/usr/bin/python

import os
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use("Agg")
import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.ticker as mtick
import matplotlib.animation as manimation

import chern_numbers
from chern_numbers.auxil import mirror_data_frame as mirror
from chern_numbers.auxil.colormaps import viridis

import auxil.processing as processing

if __name__ == "__main__":

    data_folder = '../../../LocalData/Data/'
    figures_folder = '../../../LocalData/Figures/'
    chern_data_file = os.path.join(data_folder, 'chern_numbers_v0_00.h5')
    data_file = r'D:\Master\Data\runs\Run4x4_15p_Chern\results.h5'
    v_coeff = 0.00;

    # Create figure
    # -------------
    plt.close('all')

    width = 800
    heigth = 400
    dpi = 100
    fig1 = plt.figure(figsize=(width/dpi,heigth/dpi),dpi=dpi)
    fig1.suptitle('', fontsize=14)

    axes1 = fig1.add_subplot(1, 2, 1, projection='3d')
    axes1.set_title('Berry Curvature', fontsize=10)
    axes1.set_xlabel('\nNull point y', fontsize=8)
    axes1.set_ylabel('\nNull point x', fontsize=8)
    axes1.set_xlim([-0.5,0.5])
    axes1.set_ylim([-0.5,0.5])
    axes1.xaxis.set_ticks(np.linspace(-0.5, 0.5, 4))
    axes1.yaxis.set_ticks(np.linspace(-0.5, 0.5, 4)[1:])
    axes1.xaxis.set_major_formatter(mtick.FormatStrFormatter('%0.2f'))
    axes1.yaxis.set_major_formatter(mtick.FormatStrFormatter('%0.2f'))
    axes1.zaxis.set_major_formatter(mtick.FormatStrFormatter('      %0.2f'))
    axes1.xaxis.set_tick_params(labelsize=8)
    axes1.yaxis.set_tick_params(labelsize=8)
    axes1.zaxis.set_tick_params(labelsize=8)
    berry_surf = None

    axes2 = fig1.add_subplot(1, 2, 2, projection='3d')
    axes2.view_init(elev=12, azim=-35)
    axes2.set_title('Ground Energy\nand First Excited State Energy', fontsize=10)
    axes2.set_xlabel('\nNull point y', fontsize=8)
    axes2.set_ylabel('\nNull point x', fontsize=8)
    axes2.set_zlabel('\nEnergy', fontsize=8)
    axes2.set_xlim([-0.5,0.5])
    axes2.set_ylim([-0.5,0.5])
    axes2.xaxis.set_ticks(np.linspace(-0.5, 0.5, 4))
    axes2.yaxis.set_ticks(np.linspace(-0.5, 0.5, 4)[1:])
    axes2.xaxis.set_major_formatter(mtick.FormatStrFormatter('%0.2f'))
    axes2.yaxis.set_major_formatter(mtick.FormatStrFormatter('%0.2f'))
    axes2.zaxis.set_major_formatter(mtick.FormatStrFormatter('      %0.4f'))
    axes2.xaxis.set_tick_params(labelsize=8)
    axes2.yaxis.set_tick_params(labelsize=8)
    axes2.zaxis.set_tick_params(labelsize=8)
    ground_energy_surf = None
    first_energy_surf = None

    chern_numbers_df = pd.read_hdf(chern_data_file,'data')
    data = pd.read_hdf(data_file,'results')

    data = data[(data['vCoeff']==v_coeff)]
    grouped_data = data.groupby('tCoeff')

    t_coeffs_list = sorted(grouped_data.indices.keys())

    for t_coeff in (2.7,):

        print('t_coeff = {}'.format(t_coeff))

        data1 = grouped_data.get_group(t_coeff)

        # plot Barry curvature
        integrand, chern_number = processing.extract_berry_curv(data1)

        t_over_j = data1.iloc[0]['tCoeff']/data1.iloc[0]['jCoeff']
        filling = data1.iloc[0]['numParticles']
        fig1.suptitle('filling = {:d},      t / J = {:.02f},      Chern Number = {:d}'.format(filling, t_over_j, int(round(chern_number))), fontsize=14)

        index1 = - np.flipud(integrand.index.values)
        index2 = integrand.index.values
        columns1 = - np.flipud(integrand.columns.values)
        columns2 = integrand.columns.values

        values1 = np.flipud(np.fliplr(integrand.values))
        values2 = np.flipud(integrand.values)
        values3 = np.fliplr(integrand.values)
        values4 = integrand.values

        if index1[-1] == index2[0]:
            index2 = index2[1:]
            values2 = values2[:, 1:]
            values4 = values4[:, 1:]
        if columns1[-1] == columns2[0]:
            columns2 = columns2[1:]
            values3 = values2[1:, :]
            values4 = values4[1:, :]

        index = np.r_[index1, index2]
        columns = np.r_[columns1, columns2]
        values = np.r_[np.c_[values1, values2], np.c_[values3, values4]]

        index = pd.Index(index, name=integrand.index.name)
        columns = pd.Index(columns, name=integrand.columns.name)
        integrand = pd.DataFrame(values, index=index, columns=columns)

        X, Y = np.meshgrid(integrand.columns, integrand.index, indexing='xy')

        if berry_surf is not None:
            berry_surf.remove()

        berry_surf = axes1.plot_surface(X, Y, integrand, rstride=1, cstride=1, cmap=viridis, linewidth=0)
        start, end = axes1.get_zlim()
        axes1.zaxis.set_ticks(np.linspace(start, end, 4))


        # plot ground energy
        data2 = data1.pivot(index='nullPointY', columns='nullPointX', values='groundEnergies')

        index1 = - np.flipud(data2.index.values)
        index2 = data2.index.values
        columns1 = - np.flipud(data2.columns.values)
        columns2 = data2.columns.values

        values1 = np.flipud(np.fliplr(data2.values))
        values2 = np.flipud(data2.values)
        values3 = np.fliplr(data2.values)
        values4 = data2.values

        if index1[-1] == index2[0]:
            index2 = index2[1:]
            values2 = values2[:, 1:]
            values4 = values4[:, 1:]
        if columns1[-1] == columns2[0]:
            columns2 = columns2[1:]
            values3 = values3[1:, :]
            values4 = values4[1:, :]

        index = np.r_[index1, index2]
        columns = np.r_[columns1, columns2]
        values = np.r_[np.c_[values1, values2], np.c_[values3, values4]]

        index = pd.Index(index, name=data2.index.name)
        columns = pd.Index(columns, name=data2.columns.name)
        data2 = pd.DataFrame(values, index=index, columns=columns)

        X, Y = np.meshgrid(data2.columns, data2.index, indexing='xy')

        ground_energy = data2.applymap(lambda x: x[0] if (type(x)==np.ndarray) else x)
        first_energy = data2.applymap(lambda x: x[1] if (type(x)==np.ndarray) else x)

        gap = np.min((first_energy.values-ground_energy.values)) / (np.max(first_energy.values) - np.min(ground_energy.values))

        if ground_energy_surf is not None:
            ground_energy_surf.remove()
            first_energy_surf.remove()

        ground_energy_surf = axes2.plot_surface(X, Y, ground_energy, rstride=1, cstride=1, cmap=viridis, linewidth=0)
        first_energy_surf = axes2.plot_surface(X, Y,first_energy, rstride=1, cstride=1, cmap=viridis, linewidth=0)
        start, end = axes2.get_zlim()
        axes2.zaxis.set_ticks(np.linspace(start, end, 4))

        # Plot chern numbers
        n_sites = data1.iloc[0]['xSize']*data1.iloc[0]['xSize']

        plt.show()

        out_file = os.path.join(figures_folder, r'BerryCurv.png')
        if not os.path.isdir(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        fig1.savefig(out_file, dpi=dpi)

