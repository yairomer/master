import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import yaml

import chern_numbers

if __name__ == '__main__':
    plt.close('all')

    system_params = {
        'x_size': 4,
        'y_size': 4,
        'n_particles': 1,
        'n_up_spins': 1,
        'use_hilbert_space_lut_flag': True,

        'perp_flux_quanta': 1,

        't_coeff': 1,
        'j_coeff': 0,
        'v_coeff': 0,
        'ni_nj': True,
        'use_max_offset': False,
        'use_hamiltonian_matrix_flag': True,
    }

    calculation_params = {
        'project_on_k': False,
        'loop_path': None
    }

    dx = 0.05
    dy = 0.05

    x_vec = np.arange(0, 0.5, dx)
    y_vec = np.arange(0, 0.5, dy)

    loop_path = [[dx, 0], [dx, dy], [0, dy]]

    x_grid, y_grid = np.meshgrid(x_vec, y_vec)
    results = pd.DataFrame({'x': x_grid.flat, 'y': y_grid.flat})

    calculation_params['loop_path'] = loop_path
    for indx in results.index:
        system_params['null_point'] = (results.iloc[indx]['x'], results.iloc[indx]['y'])

        ground_state_values = chern_numbers.calc_ground_state_values_non_interacting(system_params, calculation_params,
                                                                                     ground_state_values_list=None)

        for key in ground_state_values.keys():
            if key not in results.columns:
                results[key] = np.nan

        results.update(pd.DataFrame([ground_state_values], index=[indx]))

    print('\nResults:')
    print('--------')

    results_tmp1 = results
    results = results.iloc[:0].drop('wilson_loops', axis=1)
    for n_particles in range(system_params['x_size']*system_params['y_size']):
        results_tmp2 = results_tmp1
        results_tmp2['n_particles'] = n_particles
        results_tmp2['wilson_loop'] = results_tmp1['wilson_loops'].map(lambda x: x[:(n_particles+1)].sum())
        results_tmp2 = results_tmp2.drop('wilson_loops', axis=1)
        results = results.append(results_tmp2, ignore_index=True)

    # print('\n{:s}'.format(dict(results.iloc[0])))
    # timing = dict(results.iloc[0]['timing'])
    # for key in timing.keys():
    #     timing[key] = timing[key][1] - timing[key][0]
    #
    # print('\n{:s}'.format(timing))

    for n_particles in range(system_params['x_size']*system_params['y_size']):
        results2 = results.loc[results['n_particles'] == n_particles]

        x_grid = results2.pivot(index='x_c', columns='y_c', values='x_c').values
        y_grid = results2.pivot(index='x_c', columns='y_c', values='y_c').values
        loops = results2.pivot(index='x_c', columns='y_c', values='wilson_loop').values
        loops_area = results2.pivot(index='x_c', columns='y_c', values='loop_area').values

        chern_number = loops.sum() * system_params['x_size']*system_params['y_size']/loops_area.sum()

        print('Chern number: {}'.format(chern_number))

        fig = plt.figure()
        ax = fig.gca(projection='3d')
        surf = ax.plot_surface(x_grid, y_grid, loops/loops_area,
                               rstride=1, cstride=1, cmap=chern_numbers.auxil.colormaps.viridis, linewidth=0)

        # fig = plt.figure()
        # ax = fig.gca(projection='3d')
        # surf = ax.plot_surface(X2, Y2, ground_energy, rstride=1, cstride=1, cmap=plt.cm.coolwarm,
        #                        linewidth=0, antialiased=False)


    plt.show()
