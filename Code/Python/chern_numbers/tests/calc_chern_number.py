import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import yaml

import chern_numbers

if __name__ == '__main__':
    system_params = {
        'x_size': 4,
        'y_size': 4,
        'n_particles': 3,
        'n_up_spins': 1,
        'use_hilbert_space_lut_flag': True,

        'perp_flux_quanta': 1,

        't_coeff': 1,
        'j_coeff': 1,
        'v_coeff': 0,
        'ni_nj': True,
        'use_max_offset': True,
        'use_hamiltonian_matrix_flag': True,
    }

    calculation_params = {
        'num_of_energies': 3,
        'num_of_lanczos_vectors': 10,
        'iterations': None,
        'tolerance': 0,
        'project_on_k': False,
        'loop_path': None
    }

    dx = 0.1
    dy = 0.1

    x_vec = np.arange(0, 0.5, dx)
    y_vec = np.arange(0, 0.5, dy)

    loop_path = [[dx, 0], [dx, dy], [0, dy]]

    x_grid, y_grid = np.meshgrid(x_vec, y_vec)
    results = pd.DataFrame({'x': x_grid.flat, 'y': y_grid.flat})

    calculation_params['loop_path'] = loop_path
    for indx in results.index:
        system_params['null_point'] = (results.iloc[indx]['x'], results.iloc[indx]['y'])

        ground_state_values = chern_numbers.calc_ground_state_values(system_params, calculation_params,
                                                                     ground_state_values_list=None)

        for key in ground_state_values.keys():
            if key not in results.columns:
                results[key] = np.nan

        results.update(pd.DataFrame([ground_state_values], index=[indx]))

        print(results.loc[indx]['wilson_loop'])

    print('\nResults:')
    print('--------')

    print('\n{:s}'.format(dict(results.iloc[0])))

    x_grid = results.pivot(index='x_c', columns='y_c', values='x_c')
    y_grid = results.pivot(index='x_c', columns='y_c', values='y_c')
    loops = results.pivot(index='x_c', columns='y_c', values='wilson_loops')
    loops_area = results.pivot(index='x_c', columns='y_c', values='loop_area')

    chern_number = loops.values.sum() * system_params['x_size']*system_params['y_size']*4

    print('Chern number: {}'.format(chern_number))

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(x_grid, y_grid, loops/loops_area,
                           rstride=1, cstride=1, cmap=chern_numbers.auxil.colormaps.viridis, linewidth=0)

    # fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # surf = ax.plot_surface(X2, Y2, ground_energy, rstride=1, cstride=1, cmap=plt.cm.coolwarm,
    #                        linewidth=0, antialiased=False)

    timing = dict(results.iloc[0]['timing'])
    for key in timing.keys():
        timing[key] = timing[key][1] - timing[key][0]

    print('\n{:s}'.format(timing))

    plt.show()
