import numpy as np
import copy


class MagneticField:
    r"""
    Defining a magnetic field  which goes into the torus through a singular point (defined by null_point) and exits
    uniformly out through the torus. The total flux going in and out is perp_flux_quanta.
    The magnetic vector potential of this field is given by:

    .. math::

        \begin{eqnarray*}
            A\left(x,y\right) & = & \left[-\frac{2\pi}{y_{size}}\cdot\delta\left(x-\mbox{null point}_{x}\right)\left(\left(y-\mbox{null point}_{y}\right)\mod\,y_{szie}\right)+\mbox{Aharonov Bohm}_{x}\right]\hat{x}\\
            &  & \left[\frac{2\pi}{y_{size}x_{size}}\left(\left(x-\mbox{null point}_{x}\right)\mod\,x_{szie}\right)+\mbox{Aharonov Bohm}_{y}\right]\hat{y}
        \end{eqnarray*}

    Parameters
    ----------
    x_size, y_size: scalar
        The number of site in the x and y direction.
    perp_flux_quanta: scalar
        The amount of magnetic flax quanta going in and out of the torus
        (perpendicular to the lattice).
    null_point: np.array([2])
        The point upon the lattice in which the magnetic flux comes in to the torus
        (and then comes out equally distributed over the lattice)
    kwarg: ignored
        This last input exist only so that calls of form of class_name(\*\*dict) could also will support irrelevant
        keys in the dictionary.
    """

    def __init__(self, x_size, y_size, perp_flux_quanta, null_point, **kwargs):
        self.x_size = float(x_size)
        self.y_size = float(y_size)
        self.perp_flux_quanta = float(perp_flux_quanta)
        self.null_point = null_point.astype(float) if isinstance(null_point, np.ndarray) \
            else np.array(null_point, dtype=float)

    def calc_phase(self, start_point_xy, end_point_xy=None):
        r"""
        Calculate the phase gained when moving from start_point_xy to end_point_xy on a straight line.
        If xy_end is not given then xy_start is treated as a path going form xy_start[0,:]->xy_start[1,:],
        xy_start[1,:]->xy_start[2,:] and so on.

        Parameters
        ----------
        start_point_xy: np.ndarray([2]) or np.ndarray([n_pairs,2])
            A list of (x,y) starting points
        end_point_xy: optional, np.ndarray([2]) or np.ndarray([n_pairs,2])
            A direction of the movement (starting from the start_point). If step is of size [2] or [2,1] (a pair (x,y))
            and start_point is of size [2,num_of_points] then the same step is used for each start points, else then
            each starting point has it's own step matching step
            Default: see description above

        Returns
        -------
        phi: scalar or np.array([num_of_points])
            A list of the phase accumulated on each of the steps.
        """

        if isinstance(start_point_xy, (list, tuple)):
            start_point_xy = np.array(start_point_xy, dtype=float)
        else:
            start_point_xy = copy.deepcopy(start_point_xy).astype(np.float)

        if start_point_xy.ndim == 1:
            start_point_xy = start_point_xy[np.newaxis, :]

        if end_point_xy is None:
            end_point_xy = start_point_xy[1:, :].copy()
            start_point_xy = start_point_xy[:-1, :]
        elif isinstance(end_point_xy, (list, tuple)):
            end_point_xy = np.array(end_point_xy)
        else:
            end_point_xy = copy.deepcopy(end_point_xy).astype(np.float)

        if end_point_xy.ndim == 1:
            end_point_xy = end_point_xy[np.newaxis, :]

        # Renaming
        # --------
        x_start = start_point_xy[:, 0]
        y_start = start_point_xy[:, 1]
        x_end = end_point_xy[:, 0]
        y_end = end_point_xy[:, 1]

        x_size = self.x_size
        y_size = self.y_size

        null_point_x = self.null_point[0]
        null_point_y = self.null_point[1]

        # Calculation for null point at (0,0)
        # -----------------------------------

        # dividing the path into sections separated by the `x = n*x_size` lines
        # (since the potential is periodic in x_size)
        start_section = np.floor_divide(x_start, x_size)
        end_section = np.floor_divide(x_end, x_size)

        x_direction = (x_end >= x_start)*2 - 1

        n_paths = start_point_xy.shape[0]

        # Calculating phases
        # ------------------
        # The 2*pi/x_size/y_size*perp_flux_quanta multiplicative term is added in the end
        phi = np.empty_like(x_start)
        for i in range(n_paths):
            # Calculating the contribution of each section
            sections = np.arange(start_section[i], end_section[i] + x_direction[i], x_direction[i])

            if len(sections) > 1:
                x_vec = (sections+(x_direction[i] == -1)) * x_size
                x_vec[0] = x_start[i]
                x_vec = np.append(x_vec, x_end[i])
                y_vec = y_start[i] + (x_vec-x_start[i])/(x_end[i]-x_start[i])*(y_end[i]-y_start[i])
            else:
                x_vec = np.array((x_start[i], x_end[i]))
                y_vec = np.array((y_start[i], y_end[i]))

            phi[i] = ((y_vec[1:]-y_vec[:-1])*((x_vec[1:]+x_vec[:-1])/2. % x_size - x_size/2)).sum() \
                      - x_direction[i] * x_size * (y_vec[1:-1] % y_size - y_size/2).sum()

        # Adding null-point contribution
        # ------------------------------
        phi += null_point_y * (x_end-x_start)
        phi += -null_point_x * (y_end-y_start)

        # Multiplying by the correct coefficient
        phi *= 2*np.pi/self.y_size/self.x_size*self.perp_flux_quanta

        return phi

