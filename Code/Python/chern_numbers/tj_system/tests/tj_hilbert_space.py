import yaml

import chern_numbers

if __name__ == '__main__':

    system_params = {
        'x_size': 3,
        'y_size': 2,
        'n_particles': 4,
        'n_up_spins': 2,
        'use_hilbert_space_lut_flag': True,
    }

    lattice = chern_numbers.SquareLattice(**system_params)

    state_string = ''.join([
        'd u -',
        'u - d',
        ][::-1]).replace(' ', '')

    print('\nCreating Hilbert space from the following params:\n\n%s' % yaml.dump(system_params,
                                                                                  default_flow_style=False))
    h_space = chern_numbers.TJHilbertSpace(n_sites=lattice.n_sites,
                                           state_string_format=lattice.string_format(2),
                                           **system_params)
    binary_format_string = h_space.binary_format_string

    print('\nHilbert space description:')
    print('--------------------------')

    print('\nHilbert space size is: {}'.format(h_space.size))

    print('\nCompact representation is:\n{}'.format(h_space.__repr__()))

    print('\nThe full Hilbert space is:\n{}'.format(str(h_space)))

    print('\nSingle (Scalar) conventions:')
    print('----------------------------')

    print('\nStateString to ReduceSerial and FullSerial')
    full_serial = h_space.state_string_to_full_serial(state_string)
    reduce_serial = h_space.state_string_to_reduced_serial(state_string)
    print('string: {1} -> reduced: {0}  &  full: {2} (binary: {3})'.format(
          reduce_serial, state_string, full_serial, binary_format_string.format(full_serial)))

    print('\nReduceSerial to StateString and FullSerial:')
    full_serial = h_space.reduced_serial_to_full_serial(reduce_serial)
    state_string = h_space.reduced_serial_to_state_string(reduce_serial)
    print('reduced: {0} -> string: {1}  &  full: {2} (binary: {3})'.format(
          reduce_serial, state_string, full_serial, binary_format_string.format(full_serial)))

    print('\nFullSerial to StateString and ReduceSerial:')
    reduce_serial = h_space.full_serial_to_reduced_serial(full_serial)
    state_string = h_space.full_serial_to_state_string(full_serial)
    print('full: {2} -> string: {1}  &  reduced: {0}'.format(
          reduce_serial, state_string, full_serial, binary_format_string.format(full_serial)))

    # reduced_serials_list = range(h_space.size)
    #
    # reduced_serials_list2 = h_space.full_serial_to_reduced_serial(h_space.reduced_serial_to_full_serial(reduced_serials_list))
    # print(chern_numbers.tj_system.tj_hilbertSpace.create_table((reduced_serials_list, reduced_serials_list2)))
