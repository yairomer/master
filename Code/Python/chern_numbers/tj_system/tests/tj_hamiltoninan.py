import numpy as np
import matplotlib.pyplot as plt
import yaml

import chern_numbers

if __name__ == '__main__':
    plt.ion()

    system_params = {
        'x_size': 4,
        'y_size': 4,
        'n_particles': 3,
        'n_up_spins': 1,
        'use_hilbert_space_lut_flag': True,

        'perp_flux_quanta': 1,
        'null_point': [0.6, 0.7],

        't_coeff': 1,
        'j_coeff': 1,
        'v_coeff': 0,
        'ni_nj': True,
        'use_max_offset': True,
        'use_hamiltonian_matrix_flag': True,
    }

    state_string = ''.join([
        '- - d u',
        '- - - d',
        '- - - -',
        '- - - -',
        ][::-1]).replace(' ', '')

    lattice = chern_numbers.SquareLattice(**system_params)
    h_space = chern_numbers.TJHilbertSpace(n_sites=lattice.n_sites,
                                           state_string_format=lattice.string_format(2),
                                           **system_params)
    magnetic_field = chern_numbers.MagneticField(**system_params)

    print('\nCreating Hamiltonian from the following params:\n\n{:s}'.format(yaml.dump(system_params,
                                                                                       default_flow_style=False)))
    hamiltonian = chern_numbers.TJHamiltonian(lattice=lattice,
                                              h_space=h_space,
                                              magnetic_field=magnetic_field,
                                              **system_params)

    print('\nHamiltonian attributes:')
    print('-------------------')

    print('\nApply Hamiltonian:')
    print('------------------')

    reduced_serial = h_space.state_string_to_reduced_serial(state_string)
    quantum_state_in = np.zeros(h_space.size, dtype=np.complex128)
    quantum_state_in[reduced_serial] = 3

    # print('\nThe full Hilbert space is:\n{}'.format(str(h_space)))
    phases = magnetic_field.calc_phase(lattice.nn_xy_list[:, 0, :], lattice.nn_xy_list[:, 1, :])
    text = ['${:.4f}\cdot2\pi$'.format(abs(val)/2/np.pi) for val in phases]

    lattice_figure = {}
    lattice.plot_lattice(figure_objects=lattice_figure)
    lattice.plot_path(lattice.nn_xy_list[:, 0, :], lattice.nn_xy_list[:, 1, :],
                      values=phases/2/np.pi, text=text,
                      path_plot_args={'color': (0, 0, 1.), 'alpha': 0.5},
                      text_plot_args={'color': (0, 0, 1.)},
                      data_name='nn_pairs', figure_objects=lattice_figure,
                      )

    print('\nQuantum state in:\n{:s}'.format(h_space.quantum_state_to_string(quantum_state_in)))

    quantum_state_out = hamiltonian.apply_hamiltonian(quantum_state_in)
    quantum_state_out -= quantum_state_in*hamiltonian.offset

    print('\nQuantum state out:\n{:s}'.format(h_space.quantum_state_to_string(quantum_state_out)))

    density = hamiltonian.calc_density(quantum_state_out)

    lattice_figure = {}
    lattice.plot_lattice(figure_objects=lattice_figure)
    lattice.plot_image_on_lattice(density,
                                  data_plot_args={'vmin': 0, 'cmap': plt.get_cmap('Greens'), 'interpolation': 'none'},
                                  data_name='density', figure_objects=lattice_figure,
                                  )


