import numpy as np
import matplotlib.pyplot as plt
import yaml

import chern_numbers


if __name__ == '__main__':
    plt.ion()

    system_params = {
        'x_size': 4,
        'y_size': 4,
        'perp_flux_quanta': 1,
        'null_point': [1.5, 0.5],
    }

    lattice = chern_numbers.SquareLattice(**system_params)

    print('\nCreating Hilbert space from the following params:\n\n%s' % yaml.dump(system_params,
                                                                                  default_flow_style=False))
    magnetic_field = chern_numbers.MagneticField(**system_params)

    print('\nPlotting')
    print('--------')

    print('\nPlotting phases')
    phases = magnetic_field.calc_phase(lattice.nn_xy_list[:, 0, :], lattice.nn_xy_list[:, 1, :])
    phases = (phases+np.pi) % (2*np.pi) - np.pi
    text = ['${:.4f}\cdot2\pi$'.format(abs(val)/2/np.pi) for val in phases]

    lattice_figure = {}
    lattice.plot_lattice(figure_objects=lattice_figure)
    lattice_figure['axes'].plot(system_params['null_point'][0], system_params['null_point'][1], 'xr')
    lattice.plot_path(lattice.nn_xy_list[:, 0, :], lattice.nn_xy_list[:, 1, :],
                      values=phases/2/np.pi, text=text,
                      path_plot_args={'color': (0, 0, 1.), 'alpha': 0.5},
                      text_plot_args={'color': (0, 0, 1.)},
                      data_name='nn_pairs', figure_objects=lattice_figure,
                      )

    print('\nPlotting flaks')
    x_vec = np.arange(lattice.x_size)
    start_point = np.array((x_vec, -0.5*np.ones_like(x_vec))).T
    end_point = start_point + np.resize(np.array((0, lattice.y_size)), start_point.shape)
    x_flaks = magnetic_field.calc_phase(start_point, end_point)/2./np.pi
    x_flaks = (x_flaks+0.5) % 1 - 0.5
    text = ['{:.4f}'.format(abs(val)) for val in x_flaks]

    lattice_figure = {}
    lattice.plot_lattice(figure_objects=lattice_figure)
    lattice_figure['axes'].plot(system_params['null_point'][0], system_params['null_point'][1], 'xr')
    lattice.plot_path(start_point, end_point,
                      text=text, values=2*x_flaks*lattice.y_size,
                      path_plot_args={'color': (0, 0, 1.), 'alpha': 0.5},
                      text_plot_args={'color': (0, 0, 1.), 'alpha': 0.5},
                      data_name='x_flaks', figure_objects=lattice_figure,
                      )

    y_vec = np.arange(lattice.y_size)
    start_point = np.array((-0.5*np.ones_like(y_vec), y_vec)).T
    end_point = start_point + np.resize(np.array((lattice.x_size, 0)), start_point.shape)
    y_flaks = magnetic_field.calc_phase(start_point, end_point)/2./np.pi
    y_flaks = (y_flaks+0.5) % 1 - 0.5
    text = ['{:.4f}'.format(abs(val)) for val in y_flaks]

    lattice.plot_path(start_point, end_point,
                      text=text, values=2*y_flaks*lattice.x_size,
                      path_plot_args={'color': (0, 0, 1.), 'alpha': 0.5},
                      text_plot_args={'color': (0, 0, 1.), 'alpha': 0.5},
                      data_name='y_flaks', figure_objects=lattice_figure,
                      )

    path0 = np.vstack((lattice.nn_eta_vec[0],
                       lattice.nn_eta_vec[1],
                       -lattice.nn_eta_vec[0],
                       -lattice.nn_eta_vec[1],
                       ))

    flux = np.zeros([lattice.n_sites], dtype=float)
    for i_site in range(lattice.n_sites):
        path = np.cumsum(np.vstack((lattice.sites_xy_list[i_site], path0)), axis=0)
        flux[i_site] = magnetic_field.calc_phase(path).sum()/2/np.pi
    flux = flux % 1

    text_xy = lattice.sites_xy_list + np.tile((lattice.nn_eta_vec[0]+lattice.nn_eta_vec[1])/2., (lattice.n_sites, 1))
    text = ['{:.4f}'.format(val) for val in flux]
    lattice.plot_path(text_xy, text_xy,
                      text=text,
                      path_plot_args={'color': (0, 0, 0.5), 'alpha': 0.3},
                      text_plot_args={'color': (0, 0, 0.5), 'alpha': 0.3},
                      data_name='cell_flux', figure_objects=lattice_figure,
                      )

    plt.show()
