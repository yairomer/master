import numpy as np
import matplotlib.pyplot as plt
import yaml

import chern_numbers

if __name__ == '__main__':
    plt.ion()

    system_params = {
        'x_size': 4,
        'y_size': 3,
    }

    site_ind = 4

    print('\nCreating lattice from the following params:\n\n%s' % yaml.dump(system_params, default_flow_style=False))
    lattice = chern_numbers.SquareLattice(**system_params)

    print('\nLattice attributes:')
    print('-------------------')

    print('\nThe number of sites is: %d' % lattice.n_sites)
    print('\nThe lattice:\n%s' % lattice.string_format(3).format(lattice.sites_ind_list))

    print('\nThe number of sites is: %d' % lattice.n_sites)

    print('\nSite''s list indices:\n%s' % str(lattice.sites_ind_list))

    print('\nSite''s list coordinates:\n%s' % str(lattice.sites_xy_list.T))

    print('\nNearest neighbor pairs indices:\n%s' % str(lattice.nn_ind_list.T))

    print('\nNearest neighbor pairs coordinates:\n%s' % str(lattice.nn_xy_list.transpose((1, 2, 0))))

    print('\nDiagonal next nearest neighbor pairs indices:\n%s' % str(lattice.diag_nnn_ind_list.T))

    print('\nDiagonal next nearest neighbor pairs coordinates:\n%s' % str(lattice.diag_nnn_xy_list.transpose((1, 2, 0))))

    print('\nXY <-> index conversions:')
    print('--------------------------')

    print('\nIndex to XY')
    site_xy = lattice.site_ind_to_xy(site_ind)
    print('Index: %d -> XY: (x: %d, y: %d)' % (site_ind, site_xy[0], site_xy[1]))

    print('\nXY to index')
    site_ind = lattice.site_xy_to_ind(site_xy)
    print('XY: (x: {1[0]}, y: {1[1]}) -> Index: {0}'.format(site_ind, site_xy))

    print('\nPlots')
    print('--------------------------')

    print('\nPlotting lattice')
    lattice_figure = {}
    lattice.plot_lattice(figure_objects=lattice_figure)

    print('\nPlotting connections')
    lattice.plot_path(lattice.nn_xy_list[:, 0, :], lattice.nn_xy_list[:, 1, :],
                      path_plot_args={'color': (.8, 0, 0), 'alpha': 0.5},
                      data_name='nn_pairs', figure_objects=lattice_figure,
                      )
    lattice.plot_path(lattice.diag_nnn_xy_list[:, 0, :], lattice.diag_nnn_xy_list[:, 1, :],
                      path_plot_args={'color': (.4, 0, 0), 'alpha': 0.5},
                      data_name='diag_nnn_pairs', figure_objects=lattice_figure,
                      )

    print('\nPlotting index')
    lattice.plot_image_on_lattice(lattice.sites_ind_list,
                                  data_plot_args={'vmin': 0, 'cmap': plt.get_cmap('Greens'), 'interpolation': 'none'},
                                  data_name='index', figure_objects=lattice_figure,
                                  )

