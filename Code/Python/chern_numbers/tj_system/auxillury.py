from . import TJHilbertSpace

def calc_hilbert_space_size(n_sites, n_particles, n_up_spins):
    size = TJHilbertSpace(n_sites, n_particles, n_up_spins, use_hilbert_space_lut_flag=False).size

    return size
