import copy

import numpy as np


class NChooseKLUTClass:
    """
    An auxiliary class for speeding up the `N choose K` calculation by storing the results in a LUT.
    All possible combination of n-choose-k are stored up to n = maxValue (defaulted to 64).

    Parameters
    ----------
    max_val: optional
        The maximal value of n (and therefore k) for which to calculate the lut.
        Default: 64

    """
    def __init__(self, max_val=64):
        self.LUT = np.zeros((max_val+1, max_val+1), int)
        self.LUT[0, 0] = 1
        for n in range(1, max_val+1):
            self.LUT[n, 0] = 1
            for k in range(1, n):
                self.LUT[n, k] = self.LUT[n-1, k]+self.LUT[n-1, k-1]
            self.LUT[n, n] = 1

    def __call__(self, n, k):
        """
        Calculates n-choose-k
        """
        return self.LUT[n, k]


class TJHilbertSpace:
    r"""
    For a fixed number of sites, particles and total spin in the z direction, the class define a Hilbert Space which
    spans all the possible combinations which contains at max 1 particle per-site. The class defines a mapping from
    a legit folk state to a serial index in the range [0,(`hilbert_space_size`-1)].
    The size of the Hilbert space is given by ( `n_sites` choose `n_particles` ) x ( `n_particles` choose `n_up_spins` ).

    Parameters
    ----------
    n_sites: int
        The number of sites.
    n_particles: int
        The number particles on the lattice.
    n_up_spins:  int
        The total spin in the z direction.
    state_string_format: optional, string
        A format to use when printing out state strings. Mainly exist for defining the arrangement of the particles
        on the lattice.
        Default: '{0}{1}{2}..{n_sites-1}'
    use_hilbert_space_lut_flag: optional, bool
        If true a lut for the fullSerial<->reducedSerial is calculated on initialization and all conversion are done
        using the lut
        Default: True
    kwarg: ignored
        This last input exist only so that calls of form of class_name(\*\*dict) could also will support irrelevant
        keys in the dictionary.

    Notes
    -----
    The class uses 3 types of representations for a system state:

        FullSerial:
            A representation spanning all possible folk states using two sets of binary representation of `n_sites`
            bits, one for storing the occupation and one storing the spin at each site. The two binary numbers are
            combined into a single `2 x n_sites` bit number which defines the state. The range of this representation
            is then [0,2^(2 x n_sites)-1].
            In the occupancy part occupied site are marked as '1' and empty as '0', and in the spin part up-spin
            is marked by '1' and down by '0'. For an empty site the spin bit should by 0.

            This representation is over complete in the meaning that a range of 4^n_sites is used when only a range
            of 3^n_sites is needed. This is due to the fact that a spin bit is stored even for empty sites. The
            reason is that this representation it is faster for applying spin and hopping operators to a given state.
            The result of that is that there are illegal numbers in the `FullSerial` representation in which an empty
            site done not have '0' as a spin bit. For efficiency there is no validation on the functions input and the
            output when using these numbers is undetermined.

            The `FullSerial` representation is used mainly for applying operators to states.

            Important note:
                'FullSerial' is stored as a 64bit integer and since each site is represented by 2 bits, the class can
                only support a lattice with up to 32 sites. In addition since a signed integer is used, when working
                with a 32 sites lattice the range of the representation is [-2^(2 x n_sites -1),2^(2 x n_sites-1)-1]
                (and not [0,2^(2 x n_sites)-1]).

        ReducedSerial:
            A representation which represents only the valid states under the constraint of a fix number of particles
            and a fix number of total spin in the z direction. The range of this representation is therefor
            [0,`hilbert_space_size`] where each number represents one of the valid states ordered according to their
            order in the 'FullSerial' representation.

        StateString:
            A string of 'n_sites' characters each representing a site where:

            :`-`: represent an empty site.
            :`u`: represent a site occupied by an up spin particle.
            :`d`: represent a site occupied by an up spin particle.

    In addition, two type of quantum states are defined which are super-position (linear combination) of system states.
    FullQuantumState, which is just an array of complex numbers of length `hilbert_space_size` representing the linear
    combination of states according to their order in the reduced serial ordering.
    SparseQuantumState, which is composed out of two lists representing only the non zero states in the linear
    combination. The two arrays are an array of integer indexes of the non-zeros states and an array of the same length
    of complex coefficients of the non-zeros states.
    The quantum representations are used in this class only for printing them as strings

    Examples
    --------
    A system of (n_sites=5, n_particles=3, n_up_spins=1) is represented by:

    hilbert_space_size = 420

    +------------------+---------------+----------------+----------------------+
    |  Reduced Serial  |  Full Serial  |  State String  |  Full Serial Binary  |
    +==================+===============+================+======================+
    |        0         |      225      |  | ``udd--``   |      0011100001      |
    +------------------+---------------+----------------+----------------------+
    |        1         |      226      |  | ``dud--``   |      0011100010      |
    +------------------+---------------+----------------+----------------------+
    |        2         |      228      |  | ``ddu--``   |      0011100100      |
    +------------------+---------------+----------------+----------------------+
    |        :         |       :       |       :        |          :           |
    +------------------+---------------+----------------+----------------------+
    |        27        |      900      |  | ``--udd``   |      1110000100      |
    +------------------+---------------+----------------+----------------------+
    |        28        |      904      |  | ``--dud``   |      1110001000      |
    +------------------+---------------+----------------+----------------------+
    |        29        |      912      |  | ``--ddu``   |      1110010000      |
    +------------------+---------------+----------------+----------------------+

        states legend: u - up spin, d - down spin, - - empty site

    """
    
    def __init__(self, n_sites, n_particles, n_up_spins, state_string_format=None, use_hilbert_space_lut_flag=True, **kwargs):
        self.n_sites = int(n_sites)
        self.n_particles = int(n_particles)
        self.n_up_spins = int(n_up_spins)
        self.state_string_format = state_string_format
        self.use_lut_flag = bool(use_hilbert_space_lut_flag)

        self.n_choose_k_lut = NChooseKLUTClass(max_val=self.n_sites)

        self._calc_size()
        
        if self.use_lut_flag:
            self._calc_lut()

        self.binary_format_string = '{{0:0{:d}b}}'.format(2*self.n_sites)
        
    def _calc_size(self):
        """
        'Internal function'.
        Calculates the Hilbert space size and stores it into the class's variable size.
        """

        self.n_particles_comb = self.n_choose_k_lut(self.n_sites, self.n_particles)
        
        self.n_spins_comb = self.n_choose_k_lut(self.n_particles, self.n_up_spins)
            
        self.size = self.n_particles_comb*self.n_spins_comb

    def _calc_lut(self):
        """
        'Internal function'.
        Calculates a 'ReducedSerial' to 'FullSerial' lut.
        """
        self.use_lut_flag = False
        self.lut = self.reduced_serial_to_full_serial(np.arange(self.size))
        self.use_lut_flag = True

    def reduced_serial_to_full_serial(self, reduced_serial):
        """
        Converts from 'ReducedSerial' to 'FullSerial' representation (see class description).

        Parameters
        ----------
        reduced_serial: int or np.ndarray ([num_of_input_states] of int)

        Returns
        -------
        full_serial: int or np.ndarray ([num_of_input_states] of np.int64)
            Output type is according to input
        """

        if isinstance(reduced_serial, (list, tuple)):
            reduced_serial = np.array(reduced_serial, dtype=np.int64)
            is_scalar = False
            n_inputs = len(reduced_serial)
        elif isinstance(reduced_serial, np.ndarray):
            is_scalar = False
            n_inputs = len(reduced_serial)
        else:
            reduced_serial = np.array((reduced_serial,), dtype=np.int64)
            is_scalar = True
            n_inputs = 1

        if self.use_lut_flag:
            full_serial = self.lut[reduced_serial]
        else:
            full_serial = np.zeros(n_inputs, np.int64)

            particles_left = self.n_particles*np.ones(n_inputs, dtype=int)
            up_spins_left = self.n_up_spins*np.ones(n_inputs, dtype=int)
            particles_reduced_serial = reduced_serial//self.n_spins_comb
            spin_reduced_serial = reduced_serial % self.n_spins_comb

            for site in range(self.n_sites)[::-1]:
                particle_comb_temp = self.n_choose_k_lut(site, particles_left)
                ind = (particles_reduced_serial >= particle_comb_temp)

                full_serial[ind] |= 2**np.int64(site+self.n_sites)
                particles_reduced_serial[ind] -= particle_comb_temp[ind]
                particles_left[ind] -= 1

                spin_comb_temp = self.n_choose_k_lut(particles_left[ind], up_spins_left[ind])
                ind2 = (spin_reduced_serial[ind] >= spin_comb_temp)
                ind[ind] = ind2

                full_serial[ind] |= 2**np.int64(site)
                spin_reduced_serial[ind] -= spin_comb_temp[ind2]
                up_spins_left[ind] -= 1

        if is_scalar:
            full_serial = full_serial[0]

        return full_serial

    def full_serial_to_reduced_serial(self, full_serial):
        """
        Converts from 'FullSerial' to 'ReducedSerial' representation (see class description).

        Parameters
        ----------
        full_serial: int or np.ndarray ([num_of_input_states] of int)

        Returns
        -------
        reduced_serial: int or np.ndarray ([num_of_input_states] of np.int64)
            Output type is according to input
        """

        if isinstance(full_serial, (list, tuple)):
            full_serial = np.array(full_serial, dtype=np.int64)
            is_scalar = False
            n_inputs = len(full_serial)
        elif isinstance(full_serial, np.ndarray):
            is_scalar = False
            n_inputs = len(full_serial)
        else:
            full_serial = np.array((full_serial,), dtype=np.int64)
            is_scalar = True
            n_inputs = 1

        if self.use_lut_flag:
            reduced_serial = np.searchsorted(self.lut, full_serial)
        else:
            particles_reduced_serial = np.zeros(n_inputs, np.int64)
            spin_reduced_serial = np.zeros(n_inputs, np.int64)

            next_particle = np.ones(n_inputs, dtype=int)
            next_up_spin = np.ones(n_inputs, dtype=int)

            for site in range(self.n_sites):
                ind = np.bitwise_and(full_serial, 2**np.int64(site+self.n_sites)) != 0

                particles_reduced_serial[ind] += self.n_choose_k_lut(site, next_particle[ind])
                next_particle[ind] += 1

                ind[ind] = np.bitwise_and(full_serial[ind], 2**np.int64(site)) != 0

                spin_reduced_serial[ind] += self.n_choose_k_lut(next_particle[ind]-2, next_up_spin[ind])
                next_up_spin[ind] += 1

            reduced_serial = particles_reduced_serial*self.n_spins_comb + spin_reduced_serial

        if is_scalar:
            reduced_serial = reduced_serial[0]

        return reduced_serial

    def full_serial_to_state_string(self, full_serial):
        """
        Converts from 'FullSerial' to 'StateString' representation (see class description).

        Parameters
        ----------
        full_serial: int or np.ndarray ([num_of_input_states] of int)

        Returns
        -------
        state_string: string or np.ndarray ([num_of_input_states] of (num_of_sites chars))
            Output type is according to input
        """

        if isinstance(full_serial, (list, tuple)):
            full_serial = np.array(full_serial, dtype=np.int64)
            is_scalar = False
            n_inputs = len(full_serial)
        elif isinstance(full_serial, np.ndarray):
            is_scalar = False
            n_inputs = len(full_serial)
        else:
            full_serial = np.array((full_serial,), dtype=np.int64)
            is_scalar = True
            n_inputs = 1

        state_string = np.empty((n_inputs, self.n_sites), dtype='S1')
        state_string.fill('-')
        for site in range(self.n_sites):
            ind = np.bitwise_and(full_serial, 2**np.int64(site+self.n_sites)) != 0
            ind2 = np.bitwise_and(full_serial[ind], 2**np.int64(site)) != 0

            ind_up = np.zeros_like(ind)
            ind_down = np.zeros_like(ind)

            ind_up[ind] = ind2
            ind_down[ind] = np.logical_not(ind2)

            state_string[ind_up, site] = 'u'
            state_string[ind_down, site] = 'd'

        state_string = state_string.view('S%d' % self.n_sites).ravel()
        
        if is_scalar:
            state_string = state_string[0].tostring()

        return state_string

    def reduced_serial_to_state_string(self, reduced_serial):
        """
        Converts from 'ReducedSerial' to 'StateString' representation (see class description).

        Parameters
        ----------
        reduced_serial: int or np.ndarray ([num_of_input_states] of int)

        Returns
        -------
        state_string: string or np.ndarray ([num_of_input_states] of (num_of_sites chars))
            Output type is according to input
        """
        full_serial = self.reduced_serial_to_full_serial(reduced_serial)
        state_string = self.full_serial_to_state_string(full_serial)
        return state_string

    def state_string_to_full_serial(self, state_string):
        """
        Converts from 'StateString' to 'FullSerial' representation (see class description).

        Parameters
        ----------
        state_string: string or np.ndarray ([num_of_input_states] of (num_of_sites chars))

        Returns
        -------
        full_serial: int or np.ndarray ([num_of_input_states] of np.int64)
            Output type is according to input
        """

        if isinstance(state_string, (list, tuple)):
            state_string = np.array(state_string, dtype='S%d' % self.n_sites)
            is_scalar = False
            n_inputs = len(state_string)
        elif isinstance(state_string, np.ndarray):
            is_scalar = False
            n_inputs = len(state_string)
        elif isinstance(state_string, (str,)):
            state_string = np.array((state_string,), dtype='S%d' % self.n_sites)
            is_scalar = True
            n_inputs = 1

        state_string = state_string.view('S1').reshape((n_inputs, self.n_sites))

        full_serial = np.zeros(n_inputs, np.int64)
        for site in range(self.n_sites):
            ind = state_string[:, site] == 'u'
            full_serial[ind] += 2**np.int64(site+self.n_sites) + 2**site

            ind = state_string[:, site] == 'd'
            full_serial[ind] += 2**np.int64(site+self.n_sites)

        if is_scalar:
            full_serial = full_serial[0]

        return full_serial

    def state_string_to_reduced_serial(self, state_string):
        """
        Converts from 'StateString' to 'ReducedSerial' representation (see class description).

        Parameters
        ----------
        state_string: string or np.ndarray ([num_of_input_states] of (num_of_sites chars))

        Returns
        -------
        reduced_serial: int or np.ndarray ([num_of_input_states] of np.int64)
            Output type is according to input
        """

        full_serial = self.state_string_to_full_serial(state_string)
        reduced_serial = self.full_serial_to_reduced_serial(full_serial)
        return reduced_serial

    def quantum_state_to_string(self, quantum_state, force_full=False):
        inds = np.arange(self.size)
        coeffs = quantum_state

        if not force_full:
            inds2 = np.abs(coeffs) > np.spacing(1)
            inds = inds[inds2]
            coeffs = coeffs[inds2]

        return self.sparse_quantum_state_to_string(inds, coeffs)

    def sparse_quantum_state_to_string(self, inds, coeffs, for_resturcturedtext=False):

        titles_list = ('Reduced Serial', 'Amplitude', 'Phase', 'State String')

        state_strings_list = self.reduced_serial_to_state_string(inds)

        if self.state_string_format is not None:
            state_strings_list = [self.state_string_format.format(list(state_string))
                                  for state_string in state_strings_list]
        if for_resturcturedtext:
            state_strings_list = ['\n'.join(['| ``'+state_string_line+'``' for state_string_line in state_string.split('\n')])
                                  for state_string in state_strings_list]

        values_lists_list = [inds, np.abs(coeffs), np.angle(coeffs)/2/np.pi, state_strings_list]

        formats_list = ['{:d}', '{:.4f}', '{:.4f} 2pi', '{:s}']

        str_out = create_table(values_lists_list, titles_list, formats_list)

        return str_out

    def create_table(self, preview_only=False, for_resturcturedtext=False):

        if self.size < 7:
            preview_only = False

        titles_list = ('Reduced Serial', 'Full Serial', 'State String', 'Full Serial Binary')

        if preview_only:
            reduced_serials_list = range(0, 4)+range(self.size-3, self.size)
        else:
            reduced_serials_list = range(self.size)
        full_serials_list = self.reduced_serial_to_full_serial(reduced_serials_list)
        state_strings_list = self.full_serial_to_state_string(full_serials_list)
        if self.state_string_format is not None:
            state_strings_list = [self.state_string_format.format(list(state_string))
                                  for state_string in state_strings_list]
        if for_resturcturedtext:
            state_strings_list = ['\n'.join(['| ``'+state_string_line+'``' for state_string_line in state_string.split('\n')])
                                  for state_string in state_strings_list]

        values_lists_list = [reduced_serials_list, full_serials_list, state_strings_list, full_serials_list]

        formats_list = ['{}', '{}', '{}', self.binary_format_string]

        if preview_only:
            values_lists_list[1] = values_lists_list[1].tolist()
            values_lists_list[3] = [self.binary_format_string.format(val) for val in values_lists_list[3]]
            formats_list[3] = '{}'

            for i in range(len(values_lists_list)):
                values_lists_list[i][3] = ':'

        str_out = create_table(values_lists_list, titles_list, formats_list)

        return str_out

    def __str__(self):
        str_out = self.create_table()
        return str_out

    def __repr__(self):
        str_out = self.create_table(preview_only=True)
        return str_out

def create_table(values_lists_list, titles_list=None, formats_list=None):

    n_cols = len(values_lists_list)
    n_rows = len(values_lists_list[0])

    values_list_strings = [''] * n_cols
    column_widths_list = [0] * n_cols
    row_heights_list = [0] * n_rows

    if formats_list is None:
        formats_list = '{}'
    if not isinstance(formats_list, (list, tuple)):
        formats_list = (formats_list,)*n_cols

    for i_col, (format_str, values_list) in enumerate(zip(formats_list, values_lists_list)):
        values_list_strings[i_col] = [format_str.format(val).split('\n') for val in values_list]
        if n_rows > 0:
            column_widths_list[i_col] = max([max(len(val_line) for val_line in val) for val in values_list_strings[i_col]])
        else:
            column_widths_list[i_col] = 0
        if titles_list is not None:
            column_widths_list[i_col] = max(column_widths_list[i_col], len(titles_list[i_col]))
        column_widths_list[i_col] += 4

    values_list_strings = [list(values_list) for values_list in zip(*values_list_strings)]

    for i_row in range(n_rows):
        row_heights_list[i_row] = max([len(val) for val in values_list_strings[i_row]])

        for i_col in range(n_cols):
            values_list_strings[i_row][i_col] += [''] * (row_heights_list[i_row]-len(values_list_strings[i_row][i_col]))

            extend_str_format = '{:^%d}' % column_widths_list[i_col]
            for i_line in range(row_heights_list[i_row]):
                 values_list_strings[i_row][i_col][i_line] = extend_str_format.format(values_list_strings[i_row][i_col][i_line])

        values_list_strings[i_row] = zip(*values_list_strings[i_row])

    if titles_list is not None:
        titles_list = list(copy.deepcopy(titles_list))
        for i_col in range(n_cols):
            extend_str_format = '{:^%d}' % column_widths_list[i_col]
            titles_list[i_col] = extend_str_format.format(titles_list[i_col])

    sep_line = '+' + '+'.join(['-'*x for x in column_widths_list]) + '+\n'
    title_line = sep_line.replace('-', '=')

    str_out = sep_line
    if titles_list is not None:
        str_out += '|' + '|'.join(titles_list) + '|\n'
        str_out += title_line
    for i_row in range(n_rows):
        for i_line in range(len(values_list_strings[i_row])):
            str_out += '|' + '|'.join(values_list_strings[i_row][i_line]) + '|\n'
        str_out += sep_line

    return str_out
