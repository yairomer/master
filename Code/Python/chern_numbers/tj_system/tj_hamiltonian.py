import numpy as np
from scipy.sparse import csc_matrix

import matplotlib.pyplot as plt

from .tj_hilbert_space import TJHilbertSpace


class TJHamiltonian:

    """
    Define the an Hamiltonian for a t-J system embedded on a torus with magnetic
    flux going through it. The class defines how the Hamiltonian operates on a
    physical quantum state (super-position of ising states).

    Parameters
    ----------
    lattice: Lattice
        A lattice object defining the lattice
    h_space: HilbertSpace
        A object defining the hilbert space
    t_coeff: scalar
        The t (nn hopping) coefficient of the t-J model.
    j_coeff: scalar
        The j (nn spin interaction) coefficient of the t-J model.
    v_coeff: optional, scalar
        The v (diagonal nnn hopping) coefficient of the t-J model.
        Default: 0
    ni_nj: optional, bool
        A flag of if to add the n_i*n_j/4 term to the hamiltonian
        Default: True
    magnetic_field: optional, MagneticVectorPotential
        An object used of calculating the phase gained when moving particles between sites
    use_max_offset: optional, bool
        If true then a max_energy value is calculated and the Hamiltonian calculation results are H-max energy. This
        is required for the ground state calculation using the Lanczos iterative algorithm since it  guaranties that
        all the system's energy will be negative values and the ground state will be the largest in magnitude.
        Default: False
    use_matrix_flag: optional, bool
        If true a sparse matrix of the Hamiltonian is calculated on
        initialization and all operations are done using the lut
        Default: True
    kwarg: ignored
        This last input exist only so that calls of form of class_name(\*\*dict) could also will support irrelevant
        keys in the dictionary.
    """

    def __init__(self, lattice, h_space,
                 t_coeff, j_coeff, v_coeff=0, ni_nj=True,
                 magnetic_field=None,
                 use_max_offset=False,
                 use_hamiltonian_matrix_flag=True,
                 matrix_coeffs_type=np.complex128,
                 **kwargs):

        self.lattice = lattice
        self.h_space = h_space
        self.t_coeff = float(t_coeff)
        self.j_coeff = float(j_coeff)
        self.v_coeff = float(v_coeff)
        self.ni_nj = bool(ni_nj)
        self.magnetic_field = magnetic_field
        self.use_max_offset = bool(use_max_offset)
        self.use_matrix_flag = bool(use_hamiltonian_matrix_flag)
        self.matrix_coeffs_type = matrix_coeffs_type

        self.counter = 0

        # gather connections
        # ------------------

        # Nearest neighbor connections
        from_list = self.lattice.nn_ind_list[:, 0]
        to_list = self.lattice.nn_ind_list[:, 1]
        if self.magnetic_field is not None:
            phase_list = self.magnetic_field.calc_phase(lattice.nn_xy_list[:, 0, :], lattice.nn_xy_list[:, 1, :])
        else:
            phase_list = np.zeros_like(from_list, dtype=float)
        self.nn_pairs = zip(from_list, to_list, phase_list)

        if self.v_coeff != 0:
            from_list = self.lattice.diag_nnn_ind_list[:, 0]
            to_list = self.lattice.diag_nnn_ind_list[:, 1]
            if self.magnetic_field is not None:
                phase_list = self.magnetic_field.calc_phase(lattice.nn_xy_list[:, 0, :], lattice.nn_xy_list[:, 1, :])
            else:
                phase_list = np.zeros_like(from_list, dtype=float)
            self.diag_nnn_pairs = zip(from_list, to_list, phase_list)

        if self.use_max_offset:
            self.offset = -self._calc_max_energy()
        else:
            self.offset = 0

        if self.use_matrix_flag:
            self.half_mat = self._calc_matrix()
        else:
            self.half_mat = None

    def apply_hamiltonian(self, q_state_in, q_state_out=None, intermediate_state=None):
        """
        Apply the hamiltonian on a give quantum state.

        Parameters
        ----------
        q_state_in: numpy.ndarray ([hilbertSpaceSize] of complex)
            A vector representing the system's quantum state
            (the super-position of ising states (defined in the tJHilbertSpace
            class)) on which the Hamiltonian operates
        q_state_out: numpy.ndarray ([hilbertSpaceSize] of complex), optional
            A place holder for the q_state_out output.
        intermediate_state: numpy.ndarray ([hilbertSpaceSize] of complex), optional
            A place holder for the intermediate calculations. Only relevant if use_matrix_flag of the class is used

        Returns
        -------
        q_state_out: numpy.ndarray ([hilbertSpaceSize] of complex)
            A vector representing the resulting system's quantum state.
        """

        if q_state_out is None:
            suppress_output = False
        else:
            suppress_output = True

        if self.use_matrix_flag:
            if q_state_out is None:
                q_state_out = self.half_mat * q_state_in
            else:
                q_state_out[:] = self.half_mat * q_state_in

            if intermediate_state is None:
                q_state_out += (q_state_in.conj() * self.half_mat).conj()
            else:
                intermediate_state[:] = q_state_in.conj() * self.half_mat
                q_state_out += intermediate_state.conj()

        else:
            if self.use_max_offset and self.offset != 0:
                if q_state_out is None:
                    q_state_out = self.offset*q_state_in
                else:
                    q_state_out[:] = self.offset*q_state_in
            else:
                if q_state_out is None:
                    q_state_out = np.zeros_like(q_state_in)
                else:
                    q_state_out[:] = 0

            if self.t_coeff != 0:
                for (site_ind1, site_ind2, phase) in self.nn_pairs:
                    # Calculating T
                    pos_ind1, pos_ind2, neg_ind1, neg_ind2 = self._calc_hop_coeffs(site_ind1, site_ind2)
                    if phase != 0:
                        coeff = self.t_coeff*np.exp(1j*phase)
                    else:
                        coeff = self.t_coeff
                    q_state_out[pos_ind2] += coeff * q_state_in[pos_ind1]
                    q_state_out[neg_ind2] -= coeff * q_state_in[neg_ind1]
                    q_state_out[pos_ind1] += coeff.conjugate() * q_state_in[pos_ind2]
                    q_state_out[neg_ind1] -= coeff.conjugate() * q_state_in[neg_ind2]

            if self.j_coeff != 0:
                for (site_ind1, site_ind2, _) in self.nn_pairs:
                    # Calculating Jzz
                    # Sz_i1 * Sz_i2
                    # Diagonal coefficients
                    coeff = self.j_coeff * self._calc_s_z_coeffs(site_ind1) * self._calc_s_z_coeffs(site_ind2)
                    q_state_out += coeff * q_state_in

                    # Calculating J2
                    # S-_i1 * S+_i2/2
                    ind1, ind2 = self._calc_spin_xy_interaction_coeffs(site_ind1, site_ind2)
                    q_state_out[ind2] += self.j_coeff / 2. * q_state_in[ind1]
                    q_state_out[ind1] += self.j_coeff / 2. * q_state_in[ind2]

            if self.ni_nj:
                for (site_ind1, site_ind2, _) in self.nn_pairs:
                    # Calculating -n_i1*n_i2/4
                    # Diagonal coefficients
                    coeff = -self.j_coeff * self._calc_ni_coeffs(site_ind1) * self._calc_ni_coeffs(site_ind2) / 4.
                    q_state_out += coeff * q_state_in

            if self.v_coeff != 0:
                for (site_ind1, site_ind2, phase) in self.diag_nnn_pairs:
                    # Calculating V
                    pos_ind1, pos_ind2, neg_ind1, neg_ind2 = self._calc_hop_coeffs(site_ind1, site_ind2)
                    if phase != 0:
                        coeff = self.v_coeff*np.exp(1j*phase)
                    else:
                        coeff = self.v_coeff
                    q_state_out[pos_ind2] += coeff * q_state_in[pos_ind1]
                    q_state_out[neg_ind2] -= coeff * q_state_in[neg_ind1]
                    q_state_out[pos_ind1] += coeff * q_state_in[pos_ind2]
                    q_state_out[neg_ind1] -= coeff * q_state_in[neg_ind2]

        self.counter += 1

        if not suppress_output:
            return q_state_out

    def calc_current(self, q_state):
        """
        Calculate the current of particles on a given quantum state vector at each nearest neighbor connection.

        Parameters
        ----------
        q_state: numpy.ndarray ([hilbertSpaceSize] of complex)
            A vector representing the system's quantum state (the super-position of ising states (defined in the
            tJHilbertSpace class))

        Returns
        -------
        current: numpy.ndarray ([n_sites] of float)
            The current on each nearest neighbor connection corresponding to the sites list store in the lattice class
            variable *nn_ind_inst*.
        """

        current = np.empty((len(self.nn_pairs),), dtype=float)
        for i, (site_ind1, site_ind2, phase) in enumerate(self.nn_pairs):
            pos_ind1, pos_ind2, neg_ind1, neg_ind2 = self._calc_hop_coeffs(site_ind1, site_ind2)
            if phase != 0:
                coeff = self.t_coeff*np.exp(1j*phase)
            else:
                coeff = self.t_coeff
            current[i] = 2*(coeff * q_state[pos_ind1] * q_state[pos_ind2].conjugate()).imag.sum()
            current[i] += 2*(coeff * q_state[neg_ind1] * q_state[neg_ind2].conjugate()).imag.sum()

        return current

    def calc_density(self, q_state):
        """
        Calculate the density  operator applied on a given quantum state vector at each site.

        Parameters
        ----------
        q_state: numpy.ndarray ([hilbertSpaceSize] of complex)
            A vector representing the system's quantum state (the super-position of ising states (defined in the
            tJHilbertSpace class))

        Returns
        -------
        density: numpy.ndarray ([n_sites] of float)
            The density at each site corresponding to the sites list store in the lattice class variable *sites_list*.
        """

        q_state_square = np.abs(q_state)**2
        q_state_square /= q_state_square.sum()
        density = np.zeros_like(self.lattice.sites_ind_list, dtype=float)
        for i, site_ind in enumerate(self.lattice.sites_ind_list):
            density[i] = (q_state_square*self._calc_ni_coeffs(site_ind)).sum()

        return density

    def to_dense_matrix(self):
        """
        Calculates the Hamiltonian representation as a dense matrix

        Returns
        -------
        hamiltonian_matrix: numpy.ndarray ([hilbertSpaceSize, hilbertSpaceSize])
        """

        hamiltonian_matrix = self.half_mat.todense() + self.half_mat.todense().T.conj()
        return hamiltonian_matrix

    def _calc_max_energy(self):
        """
        Calculate abound on the highest possible energy (eigen value) using Gershgorin circle theorem.

        Returns
        -------
        max_energy: scalar (float)
            A bound on the highest system energy.
        """

        centers_vec = np.zeros(self.h_space.size)
        radius_vec = np.zeros(self.h_space.size)
        if self.t_coeff != 0:
            for (site_ind1, site_ind2, _) in self.nn_pairs:
                pos_ind1, pos_ind2, neg_ind1, neg_ind2 = self._calc_hop_coeffs(site_ind1, site_ind2)
                coeff = abs(self.t_coeff)
                radius_vec[pos_ind2] += coeff
                radius_vec[neg_ind2] += coeff
                radius_vec[pos_ind1] += coeff
                radius_vec[neg_ind1] += coeff

        if self.j_coeff != 0:
            for (site_ind1, site_ind2, _) in self.nn_pairs:
                coeff = self.j_coeff * self._calc_s_z_coeffs(site_ind1) * self._calc_s_z_coeffs(site_ind2)
                centers_vec += coeff

                ind1, ind2 = self._calc_spin_xy_interaction_coeffs(site_ind1, site_ind2)
                coeff = abs(self.j_coeff / 2.)
                radius_vec[ind2] += coeff
                radius_vec[ind1] += coeff

        if self.ni_nj:
            for (site_ind1, site_ind2, _) in self.nn_pairs:
                coeff = -self.j_coeff * self._calc_ni_coeffs(site_ind1) * self._calc_ni_coeffs(site_ind2) / 4.
                centers_vec += coeff

        if (self.v_coeff is not None) & (self.v_coeff != 0):
            for (site_ind1, site_ind2, phase) in self.diag_nnn_pairs:
                pos_ind1, pos_ind2, neg_ind1, neg_ind2 = self._calc_hop_coeffs(site_ind1, site_ind2)
                coeff = abs(self.v_coeff)
                radius_vec[pos_ind2] += coeff
                radius_vec[neg_ind2] += coeff
                radius_vec[pos_ind1] += coeff
                radius_vec[neg_ind1] += coeff

        max_energy = np.max(centers_vec + radius_vec)

        return max_energy

    def _calc_matrix(self):
        """
        Inner function
        calculate the Hamiltonian operator as a spasre matrix to improve
        calculation time.
        """

        n = self.h_space.size
        if self.use_max_offset and self.offset != 0:
            diag_coeffs = np.ones(n, dtype=self.matrix_coeffs_type) * self.offset
        else:
            diag_coeffs = np.zeros(n, dtype=self.matrix_coeffs_type)

        off_diag_ind1_list = []
        off_diag_ind2_list = []
        off_diag_coeffs_list = []

        if self.t_coeff != 0:
            pos_ind1, _, neg_ind1, _ = self._calc_hop_coeffs(self.nn_pairs[0][0], self.nn_pairs[0][1])
            n_inds = len(pos_ind1)+len(neg_ind1)

            off_diag_ind1 = np.empty((len(self.nn_pairs), n_inds), dtype=np.int64)
            off_diag_ind2 = np.empty((len(self.nn_pairs), n_inds), dtype=np.int64)
            off_diag_coeffs = np.empty((len(self.nn_pairs), n_inds), dtype=self.matrix_coeffs_type)

            for i, (site_ind1, site_ind2, phase) in enumerate(self.nn_pairs):
                # Calculating T
                pos_ind1, pos_ind2, neg_ind1, neg_ind2 = self._calc_hop_coeffs(site_ind1, site_ind2)
                if phase != 0:
                    coeff = self.t_coeff*np.exp(1j*phase)
                else:
                    coeff = self.t_coeff

                n_pos = len(pos_ind1)

                off_diag_ind1[i][:n_pos] = pos_ind1
                off_diag_ind2[i][:n_pos] = pos_ind2
                off_diag_coeffs[i][:n_pos] = coeff * np.ones_like(pos_ind1, dtype=self.matrix_coeffs_type)
                off_diag_ind1[i][n_pos:] = neg_ind1
                off_diag_ind2[i][n_pos:] = neg_ind2
                off_diag_coeffs[i][n_pos:] = -coeff * np.ones_like(neg_ind1, dtype=self.matrix_coeffs_type)

            off_diag_ind1_list.append(off_diag_ind1)
            off_diag_ind2_list.append(off_diag_ind2)
            off_diag_coeffs_list.append(off_diag_coeffs)

        if self.j_coeff != 0:
            ind1, _ = self._calc_spin_xy_interaction_coeffs(self.nn_pairs[0][0], self.nn_pairs[0][1])
            n_inds = len(ind1)

            off_diag_ind1 = np.empty((len(self.nn_pairs), n_inds), dtype=np.int64)
            off_diag_ind2 = np.empty((len(self.nn_pairs), n_inds), dtype=np.int64)
            off_diag_coeffs = np.empty((len(self.nn_pairs), n_inds), dtype=self.matrix_coeffs_type)

            for i, (site_ind1, site_ind2, _) in enumerate(self.nn_pairs):
                # Calculating Jzz
                # Sz_i1 * Sz_i2
                # Diagonal coefficients
                coeff = self.j_coeff * self._calc_s_z_coeffs(site_ind1) * self._calc_s_z_coeffs(site_ind2)
                diag_coeffs += coeff

                # Calculating J2
                # S-_i1 * S+_i2/2
                ind1, ind2 = self._calc_spin_xy_interaction_coeffs(site_ind1, site_ind2)
                off_diag_ind1[i] = ind1
                off_diag_ind2[i] = ind2
                off_diag_coeffs[i] = self.j_coeff / 2. * np.ones_like(ind1, dtype=self.matrix_coeffs_type)

            off_diag_ind1_list.append(off_diag_ind1)
            off_diag_ind2_list.append(off_diag_ind2)
            off_diag_coeffs_list.append(off_diag_coeffs)

        if self.ni_nj:
            for (site_ind1, site_ind2, _) in self.nn_pairs:
                # Calculating -n_i1*n_i2/4
                # Diagonal coefficients
                coeff = -self.j_coeff * self._calc_ni_coeffs(site_ind1) * self._calc_ni_coeffs(site_ind2) / 4.
                diag_coeffs += coeff

        if (self.v_coeff is not None) & (self.v_coeff != 0):
            pos_ind1, _, neg_ind1, _ = self._calc_hop_coeffs(self.diag_nnn_pairs[0][0], self.diag_nnn_pairs[0][1])
            n_inds = len(pos_ind1)+len(neg_ind1)

            off_diag_ind1 = np.empty((len(self.nn_pairs), n_inds), dtype=np.int64)
            off_diag_ind2 = np.empty((len(self.nn_pairs), n_inds), dtype=np.int64)
            off_diag_coeffs = np.empty((len(self.nn_pairs), n_inds), dtype=self.matrix_coeffs_type)

            for i, (site_ind1, site_ind2, phase) in enumerate(self.diag_nnn_pairs):
                # Calculating V
                pos_ind1, pos_ind2, neg_ind1, neg_ind2 = self._calc_hop_coeffs(site_ind1, site_ind2)
                if phase != 0:
                    coeff = self.v_coeff*np.exp(1j*phase)
                else:
                    coeff = self.v_coeff
                off_diag_ind1[i][:n_pos] = pos_ind1
                off_diag_ind2[i][:n_pos] = pos_ind2
                off_diag_coeffs[i][:n_pos] = coeff * np.ones_like(pos_ind1, dtype=self.matrix_coeffs_type)
                off_diag_ind1[i][n_pos:] = neg_ind1
                off_diag_ind2[i][n_pos:] = neg_ind2
                off_diag_coeffs[i][n_pos:] = -coeff * np.ones_like(neg_ind1, dtype=self.matrix_coeffs_type)

            off_diag_ind1_list.append(off_diag_ind1)
            off_diag_ind2_list.append(off_diag_ind2)
            off_diag_coeffs_list.append(off_diag_coeffs)

        for array in off_diag_ind1_list:
            array.shape = (array.size,)
        for array in off_diag_ind2_list:
            array.shape = (array.size,)
        for array in off_diag_coeffs_list:
            array.shape = (array.size,)

        off_diag_ind1_list.append(np.arange(n))
        off_diag_ind2_list.append(np.arange(n))
        off_diag_coeffs_list.append(diag_coeffs/2)

        off_diag_ind1_list = np.concatenate(off_diag_ind1_list, axis=0)
        off_diag_ind2_list = np.concatenate(off_diag_ind2_list, axis=0)
        off_diag_coeffs_list = np.concatenate(off_diag_coeffs_list, axis=0)

        half_mat = csc_matrix((off_diag_coeffs_list, (off_diag_ind2_list, off_diag_ind1_list)), shape=(n, n))

        return half_mat

    def _calc_hop_coeffs(self, site_ind1, site_ind2):
        """
        For the full hilbert space calculates which are the sates that are affected by a hopping operator between two
        given sites. The function returns a list of the affected states and to which state they transform under the
        hopping operator. The results are split into two pairs of indices, one for states that under the operation
        gain a minus sign (due to the fermions statistics) and those which are unchanged.

        Parameters
        ----------
        site_ind1: scalar
            The site index of the origin site (for the hopping).
        site_ind2: scalar
            The site index of the final site (for the hopping).

        Returns
        -------
        pos_ind1: numpy.ndarray ([num_of_positive_affected_site] of uint64)
            An array of the states that are affected by the operator and
            produce a positive hoping sign (for fermions).
        pos_ind2: numpy.ndarray ([num_of_positive_affected_site] of uint64)
            An array of the states into which the states in 'pos_ind1' transform
            to.
        neg_ind1: numpy.ndarray ([num_of_positive_affected_site] of uint64)
            An array of the states that are affected by the operator and
            produce a negative hoping sign (for fermions).
        neg_ind2: numpy.ndarray ([num_of_positive_affected_site] of uint64)
            An array of the states into which the states in 'neg_ind1' transform
            to.
        """

        full_h_space = self.h_space.lut
        n_sites = self.lattice.n_sites

        sites_mask = 2 ** np.int64(site_ind1 + n_sites) + 2 ** np.int64(site_ind2 + n_sites)
        valid_hopping_mask = 2 ** np.int64(site_ind1 + n_sites)

        ind1 = np.flatnonzero((full_h_space & sites_mask) == valid_hopping_mask).astype(np.int64)

        if ind1.size == 0:

            pos_ind1 = np.array([]).astype(np.int64)
            pos_ind2 = np.array([]).astype(np.int64)
            neg_ind1 = np.array([]).astype(np.int64)
            neg_ind2 = np.array([]).astype(np.int64)

            return pos_ind1, pos_ind2, neg_ind1, neg_ind2

        full_h_space = full_h_space[ind1].copy()

        spin_up_mask = 2 ** np.int64(site_ind1)
        ind_up = (full_h_space & spin_up_mask) != 0

        # mask to swap particles only
        hope_spin_down_mask = 2 ** np.int64(site_ind1 + n_sites) + 2 ** np.int64(site_ind2 + n_sites)
        # mask to swap particles and spin
        hope_spin_up_mask = 2 ** np.int64(site_ind1 + n_sites) + 2 ** np.int64(site_ind2 + n_sites) + 2 ** np.int64(site_ind1) + 2 ** np.int64(site_ind2)

        full_h_space[ind_up] ^= hope_spin_up_mask
        full_h_space[np.logical_not(ind_up)] ^= hope_spin_down_mask

        ind2 = self.h_space.full_serial_to_reduced_serial(full_h_space)

        delta_sites = int(abs(site_ind1 - site_ind2)) - 1

        if delta_sites < 1:
            pos_ind1 = ind1
            pos_ind2 = ind2
            neg_ind1 = np.array([]).astype(np.int64)
            neg_ind2 = np.array([]).astype(np.int64)
        else:
            # check fermions statistics
            i_min = int(min(site_ind1, site_ind2))
            full_h_space >>= (i_min + 1 + n_sites)

            hop_sign = np.ones_like(ind1, dtype=bool)
            for i in range(delta_sites):
                hop_sign ^= (full_h_space & 1).astype(bool)
                full_h_space >>= 1

            pos_ind1 = ind1[hop_sign]
            pos_ind2 = ind2[hop_sign]
            neg_ind1 = ind1[np.logical_not(hop_sign)]
            neg_ind2 = ind2[np.logical_not(hop_sign)]

        return pos_ind1, pos_ind2, neg_ind1, neg_ind2

    def _calc_spin_xy_interaction_coeffs(self, site_ind1, site_ind2):
        """
        For the full Hilbert space calculates which are the sates that are affected by a Sx Sy operator
        (actually it is the Si-\*Sj+ operator) between two given sites. The function returns a list of the affected
        states and to which state they transform under the operation.

        Parameters
        ----------
        site_ind1: scalar
            The site index of the origin site (for the hopping).
        site_ind2: scalar
            The site index of the final site (for the hopping).

        Returns
        -------
        ind1: numpy.ndarray ([num_of_affected_site] of np.int64)
            An array of the states that are affected by the operator.
        ind2: numpy.ndarray ([num_of_affected_site] of np.int64)
            An array of the states into which the states in 'ind1' transform to.
        """

        full_h_space = self.h_space.lut
        n_sites = self.lattice.n_sites

        sites_and_spins_mask = 2 ** np.int64(site_ind1 + n_sites) + 2 ** np.int64(site_ind2 + n_sites) + 2 ** np.int64(site_ind1) + 2 ** np.int64(site_ind2)
        valid_spin_mask = 2 ** np.int64(site_ind1 + n_sites) + 2 ** np.int64(site_ind2 + n_sites) + 2 ** np.int64(site_ind1)

        ind1 = np.flatnonzero((full_h_space & sites_and_spins_mask) == valid_spin_mask).astype(np.int64)

        if ind1.size == 0:

            ind2 = np.array([]).astype(np.int64)

            return ind1, ind2

        full_h_space = full_h_space[ind1].copy()

        spins_mask = 2 ** np.int64(site_ind1) + 2 ** np.int64(site_ind2)

        full_h_space ^= spins_mask

        ind2 = self.h_space.full_serial_to_reduced_serial(full_h_space)

        return ind1, ind2

    def _calc_s_z_coeffs(self, site_ind):
        """
        Parameters
        ----------
        site_ind: scalar
            The site index of the site (on which Sz operates).

        Returns
        -------
        coeff: numpy.ndarray ([hilbertSpaceSize] of float)
            The coefficient of the Si_z operator on each state.
        """

        # For the full Hilbert space calculates the output coefficient of applying a Si_z operator on a given site.
        # The state is of course unchanged under the operation and the function returns the output coefficient
        # (+0.5 or -0.5) which is resulted by applying the operator on each of the states in the Hilbert space.

        full_h_space = self.h_space.lut
        n_sites = self.lattice.n_sites

        site_mask = 2 ** np.int64(site_ind + n_sites)
        spin_mask = 2 ** np.int64(site_ind)

        coeff = ((full_h_space & site_mask) != 0) * (((full_h_space & spin_mask) != 0) - 0.5)

        return coeff

    def _calc_ni_coeffs(self, site_ind):
        """
        For the full Hilbert space calculates the output coefficient of applying a ni operator on a given site.
        The state is of course unchanged under the operation and the function returns the output coefficient
        (0 or 1) which is resulted by applying the operator on each of the states in the Hilbert space.

        Parameters
        ----------
        site_ind: scalar
            The site index of the site (on which ni operates).

        Returns
        -------
        coeff: numpy.ndarray ([hilbertSpaceSize] of float)
            The coefficient of the ni operator on each state.
        """

        full_h_space = self.h_space.lut
        n_sites = self.lattice.n_sites

        site_mask = 2 ** np.int64(site_ind + n_sites)

        coeff = ((full_h_space & site_mask) != 0).astype(float)

        return coeff

    def plot_quantum_state(self, q_state, axes=None):
        if axes is None:
            axes = self.plot_lattice(plot_magnetic_potential_flag=False)

        density = self.calc_density(q_state)

        density = density.reshape([self.x_size, self.y_size])
        density = density[np.ix_(np.r_[-1, :density.shape[0], 0], np.r_[-1, :density.shape[1], 0])]

        img_handel = axes.imshow(density, extent=(-1.5, self.x_size+0.5, -1.5, self.y_size+0.5),
                                 vmin=0, origin='lower', cmap=plt.get_cmap('Greens'))

        axes.figure.colorbar(img_handel)

        plt.draw()



