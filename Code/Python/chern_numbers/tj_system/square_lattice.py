import numpy as np
import matplotlib.pyplot as plt


class SquareLattice:
    r"""
    A class defining the arrangement of the sites in space and the connection between them (nearest neighbors and next
    nearest neighbors)

    Parameters
    ----------
    x_size: int
        The sites along the x axes.
    y_size: int
        The sites along the x axes.
    kwarg: ignored
        This last input exist only so that calls of form of class_name(\*\*dict) could also will support irrelevant
        keys in the dictionary.

    Notes
    -----
    Each site on the lattice can be represented either by a serial index number or by it's coordinates (x,y)
    The numbering is by running on x first with periodic boundary condition starting from 0.

    Example
    -------
    A 3x3 lattice is numbered as following::

                :   : : :   :
            ... 2 | 0 1 2 | 0 ...
                --+------+--
          ^ ... 8 | 6 7 8 | 6 ...
        y | ... 5 | 3 4 5 | 3 ...
          | ... 2 | 0 1 2 | 0 ...
                --+-------+--
            ... 8 | 6 7 8 | 6 ...
                :   : : :   :
                     --->
                      x

    so:

    * (0,0) -> 0
    * (1,0) -> 1
    * (0,1) -> x_size
    * (x,y) -> (x mod x_size) + (y mod y_size)*x_size

    Attributes
    ----------
    n_sites: int
        The number of sites on the lattice
    sites_ind_list: np.ndarray([n_sites] of int)
        A list of all sites indices (simply np.arange(self.n_sites))
    site_xy_list:  np.ndarray([2, n_sites])
        A list of all sites coordinates
    site_xy_list: np.ndarray([2, n_sites])
        A list of all sites coordinates
    nn_ind_list: np.ndarray([2, n_sites])
        A list of all the nearest neighbor pairs by indices
    nn_xy_list: np.ndarray([2, 2, n_sites])
        A list of all the nearest neighbor pairs coordinates ((x1,y1),(x2,y2))
    """

    nn_eta_vec = np.array(((1, 0), (0, 1)))  # nearest neighbors
    """Direction vectors of the nearest neighbor connections"""

    diag_nnn_eta_vec = np.array(((1, 1), (1, -1)))  # diagonal next nearest neighbors
    """Direction vectors of the diagonal next nearest neighbor connections"""

    def __init__(self, x_size, y_size, **kwargs):
        self.x_size = int(x_size)
        self.y_size = int(y_size)

        self.n_sites = self.x_size * self.y_size

        self.sites_ind_list = np.arange(self.n_sites, dtype=int)
        self.sites_xy_list = self.site_ind_to_xy(self.sites_ind_list)

        from_xy_list = np.tile(self.sites_xy_list, (self.nn_eta_vec.shape[0], 1))
        direction_list = np.repeat(self.nn_eta_vec, self.sites_xy_list.shape[0], axis=0)
        to_xy_list = self.move_on_lattice(from_xy_list, direction_list)
        from_inds_list = self.site_xy_to_ind(from_xy_list)
        to_inds_list = self.site_xy_to_ind(to_xy_list)

        self.nn_xy_list = np.concatenate((from_xy_list[:, np.newaxis, :], to_xy_list[:, np.newaxis, :]), axis=1)
        self.nn_ind_list = np.concatenate((from_inds_list[:, np.newaxis], to_inds_list[:, np.newaxis]), axis=1)

        from_xy_list = np.tile(self.sites_xy_list, (self.nn_eta_vec.shape[0], 1))
        direction_list = np.repeat(self.diag_nnn_eta_vec, self.sites_xy_list.shape[0], axis=0)
        to_xy_list = self.move_on_lattice(from_xy_list, direction_list)
        from_inds_list = self.site_xy_to_ind(from_xy_list)
        to_inds_list = self.site_xy_to_ind(to_xy_list)

        self.diag_nnn_xy_list = np.concatenate((from_xy_list[:, np.newaxis, :], to_xy_list[:, np.newaxis, :]), axis=1)
        self.diag_nnn_ind_list = np.concatenate((from_inds_list[:, np.newaxis], to_inds_list[:, np.newaxis]), axis=1)

    def string_format(self, n_chars=1):
        """
        Returns a string which can be used to text-draw the lattice with control on the characters at each site by
        using the string as follow::

            lattice_string_format.format(a_list_of_char_per_site)

        Parameters
        ----------
        n_chars: optional, int
            The number of characters to reserve for each site
            Default: 1

        Returns
        -------
        string_format: string
        """

        str_out = '\n'.join([''.join(['{0[%d]:^%d}' % (x+y*self.x_size, n_chars) for x in range(self.x_size)])
                             for y in range(self.y_size)[::-1]])

        return str_out

    def site_xy_to_ind(self, site_xy):
        """
        Converts (x,y) coordinate to a site index.

        Parameters
        ----------
        site_xy: list or nd.array([n_sites, 2] of numeric)
            A list of site's coordinate, (x,y)

        Returns
        -------
        site_ind: nd.array([n_sites] of int)
            Site's index.
        """

        if isinstance(site_xy, (list, tuple)):
            site_xy = np.array(site_xy)

        if site_xy.ndim == 1:
            is_scalar = True
            site_xy = site_xy[np.newaxis, :]
        else:
            is_scalar = False

        site_ind = (site_xy[:, 0] % self.x_size) + (site_xy[:, 1] % self.y_size)*self.x_size

        if is_scalar:
            site_ind = site_ind[0]

        return site_ind

    def site_ind_to_xy(self, site_ind):
        """
        Converts a site index to (x,y) coordinate . The inverse function of xy_to_site_ind. See xy_to_site_ind
        for more details

        Parameters
        ----------
        site_ind: scalar or nd.array([n_sites] of numeric)
            Site's index.

        Returns
        -------
        site_xy: nd.array([2,n_sites] of int)
            Site's coordinate. The type of the out put is determent by the input.
        """

        if isinstance(site_ind, (list, tuple)):
            site_ind = np.array(site_ind)
            is_scalar = False
        elif isinstance(site_ind, (np.ndarray, )):
            is_scalar = False
        else:
            site_ind = np.array((site_ind,))
            is_scalar = True

        x = site_ind % self.x_size
        y = np.floor_divide(site_ind, self.x_size)

        site_xy = np.concatenate((x[:, np.newaxis], y[:, np.newaxis]), axis=1)

        if is_scalar:
            site_xy = site_xy[0]

        return site_xy

    @staticmethod
    def move_on_lattice(start_point_xy, direction):
        """
        Generates a list of pairs of the sites form sites_list and the sites that they are connected to by eta_vec

        Parameters
        ----------
        start_point_xy: nd.array([2]) or nd.array([num_of_points, 2])
            A list of coordinates *(x,y)* of start points.
        direction: nd.array([2]) or nd.array([num_of_points, 2])
            A *(x,y)* vectors defining the movement.

        If only one of the two inputs is of size [2] or [2,1] while the other has num_of_points>1, than the smaller
        input is replicated into a [num_of_points, 2] array.

        Returns
        -------
        end_point: nd.array([2]) or nd.array([num_of_points, 2])
            A list of coordinates *(x,y)* of end points.
        """

        if isinstance(start_point_xy, (list, tuple)):
            start_point_xy = np.array(start_point_xy)

        if isinstance(direction, (list, tuple)):
            start_point_xy = np.array(start_point_xy)

        if start_point_xy.ndim == 1 and direction.ndim == 1:
            is_scalar = True
            start_point_xy = start_point_xy[np.newaxis, :]
            direction = direction[np.newaxis, :]
        elif start_point_xy.shape[0] == 1 and direction.shape[0] > 1:
            is_scalar = False
            start_point_xy = np.tile(start_point_xy.resize, (1, direction.shape[0]))
        elif start_point_xy.shape[0] > 1 and direction.shape[0] == 1:
            is_scalar = False
            direction = np.tile(direction.resize, (1, start_point_xy.shape[0]))
        else:
            is_scalar = False

        end_point = start_point_xy + direction

        if is_scalar:
            end_point = end_point[0]

        return end_point

    def plot_lattice(self, lattice_plot_args=None, grid_plot_args=None,
                     edge_plot_args=None, figure_objects=None,
                     minimal_plot_flag=False):
        """
        Plots the lattice points on a figure.

        Parameters
        ----------
        lattice_plot_args: optional, dict
            Additional arguments to path to the lattice (scatter) plot command
        grid_plot_args: optional, dict
            Additional arguments to path to the grid (plot) plot command
        edge_plot_args: optional, dict
            Additional arguments to path to the edge (plot) plot command
        figure_objects: optional, dict
            A dictionary containing the handles to all the graphic objects in the plot. If given then existing handles
            are only updated and missing handles are created. This could be used for example to plot the lattice in
            an existing figure. If given as an input, if for an empty dictionary, the dictionary is updated with the
            new handles
            The relevant fields of the dictionary are:

                :fig: The figure object
                :axes: The axes object
                :sites: The lattice sites
        minimal_plot_flag: optional, bool
            If `minimal_plot_flag` is true then additional 'cosmetic' stuff such as updating titles and setting axes
            limits and etc. are skipped. Is created to allow drawing data over an existing plot.
        """

        if lattice_plot_args is None:
            lattice_plot_args = {}

        if grid_plot_args is None:
            grid_plot_args = {}

        if edge_plot_args is None:
            edge_plot_args = {}

        if figure_objects is None:
            figure_objects = {}

        if 'fig' not in figure_objects:
            if 'axes' in figure_objects:
                figure_objects['fig'] = figure_objects['axes'].figure
            else:
                figure_objects['fig'] = plt.figure()
        fig = figure_objects['fig']

        if 'axes' not in figure_objects:
            figure_objects['axes'] = fig.add_subplot(1, 1, 1)
        axes = figure_objects['axes']

        if 'grid' not in figure_objects:
            plot_args = {'color': (0.1, 0.1, 0.1),
                         'linewidth': 1,
                         'linestyle': 'dashed',
                         'zorder': 0.11,
                         }
            plot_args.update(grid_plot_args)

            grid_y = [None]*self.x_size
            for i_x in range(self.x_size):
                grid_y[i_x] = axes.plot((i_x, i_x), (-1, self.y_size), **plot_args)
            figure_objects['grid_y'] = grid_y

            grid_x = [None]*self.y_size
            for i_y in range(self.y_size):
                grid_x[i_y] = axes.plot((-1, self.x_size), (i_y, i_y), **plot_args)
            figure_objects['grid_x'] = grid_x

        if 'sites' not in figure_objects:
            plot_args = {'s': 100,
                         'c': 'r',
                         'alpha': 0.7,
                         'zorder': 0.12,
                         }
            plot_args.update(lattice_plot_args)

            sites = axes.scatter(self.sites_xy_list[:, 0], self.sites_xy_list[:, 1], **plot_args)
            figure_objects['sites'] = sites

        if 'edge' not in figure_objects:
            plot_args = {'color': (0.8, 0.8, 0.8),
                         'linewidth': 4,
                         'zorder': 0.1,
                         }
            plot_args.update(edge_plot_args)

            edge = [axes.plot((-0.5, -0.5), (-1, self.y_size), '--', **plot_args),
                    axes.plot((self.x_size-0.5, self.x_size-0.5), (-1, self.y_size), '--', **plot_args),
                    axes.plot((-1, self.x_size), (-0.5, -0.5), '--', **plot_args),
                    axes.plot((-1, self.x_size), (self.y_size-0.5, self.y_size-0.5), '--', **plot_args),
                    ]
            figure_objects['edge'] = edge

        if not minimal_plot_flag:
            figure_objects['axes'].axis([-0.75, self.x_size-0.25, -0.75, self.y_size-0.25])

        plt.draw()

    @staticmethod
    def plot_path(xy_start, xy_end=None, values=None, text=None,
                  path_plot_args=None, text_plot_args=None,
                  data_name='path', figure_objects=None):
        """
        Plots the paths from xy_start to xy_end. If xy_end is not given then xy_start is treated as a path going
        form xy_start[0,:]->xy_start[1,:] ,xy_start[1,:]->xy_start[2,:] and so on.

        Parameters
        ----------
        xy_start: np.ndarray([2]) or np.ndarray([n_points,2])
        xy_end: optional, np.ndarray([2]) or np.ndarray([n_points,2])
            Default: see description above
        values: optional, scalar or np.ndarray([n_points])
            A value to put on each path
        values: optional, list ([n_points] of string)
            A text to put on each path
        path_plot_args: optional, dict
            Additional arguments to path to the path (quiver) plot command
        text_plot_args: optional, dict
            Additional arguments to path to the text (text) plot command
        data_name: optional, string
            The key name to use in the `figure_objects` dictionary.
            Default: 'path'
        figure_objects: optional, dict
            A dictionary containing the handles to all the graphic objects in the plot. If given then existing handles
            are only updated and missing handles are created. This could be used for example to plot the lattice in
            an existing figure. If given as an input, if for an empty dictionary, the dictionary is updated with the
            new handles
            The relevant fields of the dictionary are:

                :fig: The figure object
                :axes: The axes object
                :`data_name`: The plotted pairs
        """

        if isinstance(xy_start, (list, tuple)):
            xy_start = np.array(xy_start)

        if xy_start.ndim == 1:
            xy_start = xy_start[np.newaxis, :]

        if xy_end is None:
            xy_end = xy_start[1:, :].copy()
            xy_start = xy_start[:-1, :]
        elif isinstance(xy_end, (list, tuple)):
            xy_end = np.array(xy_end)

        if xy_end.ndim == 1:
            xy_end = xy_end[np.newaxis, :]

        if values is None:
            pass
        elif isinstance(values, np.ndarray):
            pass
        elif isinstance(values, (list, tuple)):
            values = np.array(values)
        else:
            values = np.array((values,))

        if path_plot_args is None:
            path_plot_args = {}

        if text_plot_args is None:
            text_plot_args = {}

        if figure_objects is None:
            figure_objects = {}

        if 'fig' not in figure_objects:
            if 'axes' in figure_objects:
                figure_objects['fig'] = figure_objects['axes'].figure
            else:
                figure_objects['fig'] = plt.figure()
        fig = figure_objects['fig']
        if 'axes' not in figure_objects:
            figure_objects['axes'] = fig.add_subplot(1, 1, 1)
        axes = figure_objects['axes']

        xy_start = xy_start.astype(float)
        xy_end = xy_end.astype(float)
        center = (xy_start+xy_end)/2.
        step = (xy_end - xy_start)
        if values is not None:
            step = step*np.resize((values / np.linalg.norm(step, axis=1)), step.T.shape).T
        step_angle = np.angle(step[:, 0] + 1j*step[:, 1])

        if data_name not in figure_objects:
            plot_args = {
                'units': 'xy',
                'scale': 1,
                'scale_units': 'xy',
                'pivot': 'middle',
                'width': 0.02,
                'alpha': 0.5,
                'angles': 'xy',
                }
            plot_args.update(path_plot_args)

            path = axes.quiver(center[:, 0], center[:, 1], step[:, 0], step[:, 1], **plot_args)
            figure_objects[data_name] = path

            if text is not None:

                text_obj = [None] * len(text)
                for i, (x, y, text_val, angle) in enumerate(zip(center[:, 0], center[:, 1], text, step_angle)):
                    angle = ((angle + np.pi/2) % np.pi) - np.pi/2
                    x += 0.1*np.cos(angle+np.pi/2)
                    y += 0.1*np.sin(angle+np.pi/2)

                    plot_args = {'horizontalalignment': 'center',
                                 'verticalalignment': 'center',
                                 'rotation': angle/np.pi*180,
                                 }
                    plot_args.update(text_plot_args)

                    text_obj[i] = axes.text(x, y, text_val, **plot_args)

                figure_objects[data_name+'_text'] = text_obj

        plt.draw()

    def plot_image_on_lattice(self, data_at_sites, data_plot_args=None,
                              data_name='data_at_sites', figure_objects=None):
        """
        Plot a a 2D function over the lattice given at the littice sites

        Parameters
        ----------
        data_at_sites: np.ndarray([n_sites])
        data_name: optional, string
            The key name to use in the `figure_objects` dictionary.
            Default: 'path'
        path_plot_args: optional, dict
            Additional arguments to path to the path (quiver) plot command
        figure_objects: optional, dict
            A dictionary containing the handles to all the graphic objects in the plot. If given then existing handles
            are only updated and missing handles are created. This could be used for example to plot the lattice in
            an existing figure. If given as an input, if for an empty dictionary, the dictionary is updated with the
            new handles
            The relevant fields of the dictionary are:

                :fig: The figure object
                :axes: The axes object
                :`data_name`: The plotted data_at_sites
        """

        if isinstance(data_at_sites, (list, tuple)):
            data_at_sites = np.array(data_at_sites)

        if data_plot_args is None:
            data_plot_args = {}

        if figure_objects is None:
            figure_objects = {}

        if 'fig' not in figure_objects:
            if 'axes' in figure_objects:
                figure_objects['fig'] = figure_objects['axes'].figure
            else:
                figure_objects['fig'] = plt.figure()
        fig = figure_objects['fig']
        if 'axes' not in figure_objects:
            figure_objects['axes'] = fig.add_subplot(1, 1, 1)
        axes = figure_objects['axes']

        data_at_sites = data_at_sites.reshape((self.y_size, self.x_size))
        # XY = self.sites_xy_list.resize((2, self.y_size, self.x_size))

        data_at_sites = data_at_sites[np.ix_(np.r_[-1, :data_at_sites.shape[0], 0], np.r_[-1, :data_at_sites.shape[1], 0])]
        # XY = XY[np.ix_((0, 1), np.r_[-1, :XY.shape[1], 0], np.r_[-1, :XY.shape[2], 0])]

        if data_name not in figure_objects:
            plot_args = {'origin': 'lower',
                         }
            plot_args.update(data_plot_args)

            data_obj = axes.imshow(data_at_sites, extent=(-1.5, self.x_size+0.5, -1.5, self.y_size+0.5), **plot_args)

            figure_objects[data_name] = data_obj

        plt.draw()
