import os
import time

import yaml

def read_yaml(yaml_file):
    if type(yaml_file) == str and os.path.exists(yaml_file):
        while os.path.exists(yaml_file+'.notReady'):
            time.sleep(0.5)

        yaml_file_stream = open(yaml_file)
        data_struct = yaml.load(yaml_file_stream)
        yaml_file_stream.close()
        return data_struct
    else:
        print 'Invalid file: ', yaml_file
        raise Exception('Invalid file: %s' % yaml_file)
