def to_list(x):
    if type(x) not in (list, tuple):
        return [x]
    else:
        return x
