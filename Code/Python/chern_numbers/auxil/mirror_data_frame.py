import numpy as np
import pandas as pd

def mirror_data_frame(data):
    index1 = - np.flipud(data.index.values)
    index2 = data.index.values
    columns1 = - np.flipud(data.columns.values)
    columns2 = data.columns.values

    values1 = np.flipud(np.fliplr(data.values))
    values2 = np.flipud(data.values)
    values3 = np.fliplr(data.values)
    values4 = data.values

    if index1[-1] == index2[0]:
        index2 = index2[1:]
        values2 = values2[:, 1:]
        values4 = values4[:, 1:]
    if columns1[-1] == columns2[0]:
        columns2 = columns2[1:]
        values3 = values3[1:, :]
        values4 = values4[1:, :]

    index = np.r_[index1, index2]
    columns = np.r_[columns1, columns2]
    values = np.r_[np.c_[values1, values2], np.c_[values3, values4]]

    index = pd.Index(index, name=data.index.name)
    columns = pd.Index(columns, name=data.columns.name)
    data_out = pd.DataFrame(values, index=index, columns=columns)

    return data_out
