import numpy as np
from scipy.sparse.linalg import eigsh
from scipy.sparse.linalg import LinearOperator


def calc_ground_state(hamiltonian,
                      num_of_energies=3,
                      num_of_lanczos_vectors=10,
                      max_iterations=None,
                      tolerance=0,
                      project_on_k=False,
                      k_projection=None,
                      **kwargs):

    # Setting parameters
    # ------------------
    nev = int(num_of_energies)
    ncvp = int(num_of_lanczos_vectors)
    maxitp = int(max_iterations) if max_iterations is not None else hamiltonian.h_space.size*10
    tolp = float(tolerance)

    # Calculate ground state
    # ----------------------
    if project_on_k:
        H_func = lambda v: k_projection(hamiltonian.apply_hamiltonian(v))
    else:
        H_func = lambda v: hamiltonian.apply_hamiltonian(v)

    n = hamiltonian.h_space.size
    H_lin_op = LinearOperator((n, n), H_func, dtype=complex)

    (ground_energies, V) = eigsh(H_lin_op,
                                 k=nev,
                                 which='LM',
                                 ncv=ncvp,
                                 maxiter=maxitp,
                                 tol=tolp)

    ground_energies = ground_energies-hamiltonian.offset

    # Sort ground states
    # ------------------
    IX = np.argsort(ground_energies)

    ground_energies = ground_energies[IX]
    ground_state = V[:, IX[0]]

    # Choose phase
    # ------------
    ground_state /= ground_state[0]/abs(ground_state[0])

    return ground_energies, ground_state


