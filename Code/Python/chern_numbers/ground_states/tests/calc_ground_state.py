import time

import numpy as np
import matplotlib.pyplot as plt
import yaml

import chern_numbers

if __name__ == '__main__':
    plt.ion()

    system_params = {
        'x_size': 4,
        'y_size': 4,
        'n_particles': 1,
        'n_up_spins': 1,
        'use_hilbert_space_lut_flag': True,

        'perp_flux_quanta': 1,
        'null_point': [0.7, 0.5],

        't_coeff': 1,
        'j_coeff': 0.5,
        'v_coeff': 0,
        'ni_nj': True,
        'use_max_offset': True,
        'use_hamiltonian_matrix_flag': True,
    }

    calculation_params = {
        'num_of_energies': 3,
        'num_of_lanczos_vectors': 10,
        'iterations': None,
        'tolerance': 1e-5,
        'project_on_k': False,
    }

    lattice = chern_numbers.SquareLattice(**system_params)
    t0 = time.time()
    h_space = chern_numbers.TJHilbertSpace(n_sites=lattice.n_sites,
                                           state_string_format=lattice.string_format(2),
                                           **system_params)
    hilbert_space_time = time.time()-t0

    magnetic_field = chern_numbers.MagneticField(**system_params)

    t0 = time.time()
    hamiltonian = chern_numbers.TJHamiltonian(lattice=lattice,
                                              h_space=h_space,
                                              magnetic_field=magnetic_field,
                                              **system_params)
    hamiltonian_time = time.time()-t0

    print('\nCreating Hamiltonian from the following params:\n\n{:s}'.format(yaml.dump(system_params,
                                                                                       default_flow_style=False)))

    print('\nCalculating ground states with the following params:\n\n{:s}'.format(yaml.dump(calculation_params,
                                                                                            default_flow_style=False)))

    hamiltonian.counter = 0
    t0 = time.time()
    ground_energies, ground_state = chern_numbers.calc_ground_state(hamiltonian, **calculation_params)
    calc_time = time.time()-t0
    n_iterations = hamiltonian.counter
    print('N-iteretions: {:d}, Took: {:g}s, {:g}s, {:g}s'.format(n_iterations,
                                                                 hilbert_space_time,
                                                                 hamiltonian_time,
                                                                 calc_time,
                                                                 ))

    print('\nGround energies:\n{:s}'.format(str(ground_energies)))
    print('\nGround state out:\n{:s}'.format(h_space.quantum_state_to_string(ground_state)))

    print('\nValidating ground state by apply Hamiltonian:')
    print('---------------------------------------------')
    ground_state2 = hamiltonian.apply_hamiltonian(ground_state)

    ind = np.abs(ground_state) > 10*calculation_params['tolerance']
    min_ratio = (ground_state2/ground_state)[ind].real.min() - hamiltonian.offset
    max_ratio = (ground_state2/ground_state)[ind].real.max() - hamiltonian.offset

    print('The ratio between the states is: min:{}, max: {}.'.format(min_ratio, max_ratio))

    density = hamiltonian.calc_density(ground_state)

    lattice_figure = {}
    lattice.plot_lattice(figure_objects=lattice_figure)
    lattice.plot_image_on_lattice(density,
                                  data_plot_args={'vmin': 0, 'cmap': plt.get_cmap('Greens'), 'interpolation': 'none'},
                                  data_name='density', figure_objects=lattice_figure,
                                  )
    plt.colorbar(lattice_figure['density'])

    current = hamiltonian.calc_current(ground_state)
    lattice.plot_path(lattice.nn_xy_list[:, 0, :], lattice.nn_xy_list[:, 1, :],
                      values=current/(np.max(np.max(np.abs(current)), 100*np.spacing(1)))*0.7,
                      path_plot_args={'color': (0, 0, 0), 'linewidth': 5},
                      data_name='current', figure_objects=lattice_figure,
                      )

    if 0:
        print('\nTiming:')
        print('-------')
        for n_particles in []:  #range(1, lattice.n_sites+1):
            system_params['n_particles'] = n_particles
            system_params['n_up_spins'] = np.floor(n_particles/2)

            t0 = time.time()
            h_space = chern_numbers.TJHilbertSpace(n_sites=lattice.n_sites,
                                                   state_string_format=lattice.string_format(2),
                                                   **system_params)
            hilbert_space_time = time.time()-t0

            t0 = time.time()
            hamiltonian = chern_numbers.TJHamiltonian(lattice=lattice,
                                                      h_space=h_space,
                                                      magnetic_field=magnetic_field,
                                                      **system_params)
            hamiltonian_time = time.time()-t0

            hamiltonian.counter = 0
            t0 = time.time()
            ground_energies, ground_state = chern_numbers.calc_ground_state(hamiltonian, **calculation_params)
            calc_time = time.time()-t0
            n_iterations = hamiltonian.counter

            print('N-particles: {:d}, Hilbert space size: {:d}, N-iteretions: {:d}, time: {:g}s, {:g}s, {:g}s' \
                            .format(n_particles,
                                    h_space.size,
                                    n_iterations,
                                    hilbert_space_time,
                                    hamiltonian_time,
                                    calc_time,
                                    ))

        plt.show()
