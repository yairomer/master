import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import yaml

import chern_numbers

if __name__ == '__main__':
    plt.ion()

    system_params = {
        'x_size': 4,
        'y_size': 4,
        'n_particles': 1,
        'n_up_spins': 1,
        'use_hilbert_space_lut_flag': True,

        'perp_flux_quanta': 1,
        'null_point': [0.7, 0.5],

        't_coeff': 0.5,
        'j_coeff': 1,
        'v_coeff': 0,
        'ni_nj': True,
        'use_max_offset': True,
        'use_hamiltonian_matrix_flag': True,
    }

    calculation_params = {
        'num_of_energies': 3,
        'num_of_lanczos_vectors': 10,
        'iterations': None,
        'tolerance': 0,
        'project_on_k': False,
        'loop_path': [[0.05, 0], [0.05, 0.05], [0, 0.05]]
    }

    ground_state_values_list = None

    print('\nCreating ground state values from the following params:\n\n{:s}\n\n{:s}'.format(
                yaml.dump(system_params, default_flow_style=False),
                yaml.dump(calculation_params, default_flow_style=False),
                ))

    ground_state_values = chern_numbers.calc_ground_state_values(system_params, calculation_params, ground_state_values_list)
    print(ground_state_values['wilson_loop'])

    if 1:
        nx = 10
        ny = 10
        x_vec = np.linspace(0, 0.5, nx+1) + np.spacing(1)*100
        y_vec = np.linspace(0, 0.5, ny+1) + np.spacing(1)*100

        dx = x_vec[1]-x_vec[0]
        dy = x_vec[1]-x_vec[0]

        x_vec2 = x_vec[:-1]+dx/2
        y_vec2 = y_vec[:-1]+dy/2

        system_params['loop_path'] = [[dx, 0], [dx, dy], [0, dy]]

        X, Y = np.meshgrid(x_vec, y_vec)
        X2, Y2 = np.meshgrid(x_vec2, y_vec2)
        loops = np.empty_like(X2)
        ground_energy = np.empty_like(X2)

        for i_x, x in enumerate(x_vec[:-1]):
            for i_y, y in enumerate(y_vec[:-1]):
                system_params['null_point'] = (x, y)
                ground_state_values = chern_numbers.calc_ground_state_values(system_params, calculation_params, ground_state_values_list)

                loops[i_x, i_y] = ground_state_values['wilson_loop']
                ground_energy[i_x, i_y] = ground_state_values['ground_energies'][0]
                print(ground_state_values['wilson_loop'])

        print('\nResults:')
        print('--------')

        # loop_path = np.array(calculation_params['loop_path']).copy()
        # loop_path = np.concatenate((((0, 0),), loop_path, ((0, 0),)))
        # dx = loop_path[1:, 0] - loop_path[:-1, 0]
        # y_mean = (loop_path[1:, 1] + loop_path[:-1, 1])/2
        # wilson_loop_area = (dx*y_mean).sum()

        wilson_loop_area = dx*dy

        loops2 = loops / wilson_loop_area*system_params['x_size']*system_params['y_size']

        fig = plt.figure()
        ax = fig.gca(projection='3d')
        surf = ax.plot_surface(X2, Y2, loops2, rstride=1, cstride=1, cmap=plt.cm.coolwarm,
                               linewidth=0, antialiased=False)

        fig = plt.figure()
        ax = fig.gca(projection='3d')
        surf = ax.plot_surface(X2, Y2, ground_energy, rstride=1, cstride=1, cmap=plt.cm.coolwarm,
                               linewidth=0, antialiased=False)

        plt.show()

        # print('\n{:s}'.format(yaml.dump(ground_state_values, default_flow_style=False)))
