import time
import copy

import numpy as np

from .. import SquareLattice
from .. import TJHilbertSpace
from .. import MagneticField
from .. import TJHamiltonian
from . import calc_ground_state


class Timer:
    def __init__(self, time_begin=0):
        self.time_begin = time_begin
        pass

    def __enter__(self):
        self.time = np.empty((2,))
        self.time[0] = time.time() - self.time_begin
        return self.time

    def __exit__(self, ty, val, tb):
        self.time[1] = time.time() - self.time_begin


def calc_ground_state_values(system_params, calculation_params, ground_state_values_list):

    ground_state_values = {}
    timing = {}

    time_begin = time.time()

    with Timer(time_begin) as timing['global']:
        with Timer(time_begin) as timing['lattice']:
            lattice = SquareLattice(**system_params)

        with Timer(time_begin) as timing['hilbert_space']:
            h_space = TJHilbertSpace(n_sites=lattice.n_sites,
                                     state_string_format=lattice.string_format(2),
                                     **system_params)

        with Timer(time_begin) as timing['magnetic_field']:
            magnetic_field = MagneticField(**system_params)

        with Timer(time_begin) as timing['hamiltonian']:
            hamiltonian = TJHamiltonian(lattice=lattice,
                                        h_space=h_space,
                                        magnetic_field=magnetic_field,
                                        **system_params)

        hamiltonian.counter = 0
        with Timer(time_begin) as timing['ground_state']:
            ground_energies, ground_state = calc_ground_state(hamiltonian, **calculation_params)
        ground_state_values['ground_energies'] = ground_energies
        ground_state_values['n_iterations'] = hamiltonian.counter

        if (ground_state_values_list is None) or ('wilson_loop' in ground_state_values_list):
            loop_path = np.array(calculation_params['loop_path'])

            system_params2 = copy.copy(system_params)

            loop_values = [{}] * loop_path.shape[0]

            ground_state_prev = ground_state

            wilson_loop = 1

            loop_path_tmp = np.vstack(([0, 0], loop_path, [0, 0]))
            x_c, y_c = np.mean(loop_path_tmp[:-1, :], axis=0) + np.array(system_params['null_point'])
            dx_vec = np.diff(loop_path_tmp[:, 0])
            v_avg_vec = (loop_path_tmp[:-1, 1] + loop_path_tmp[1:, 1])/2
            loop_area = -np.sum(dx_vec*v_avg_vec)

            for i in range(len(loop_path)):
                system_params2['null_point'] = np.array(system_params['null_point']) + np.array(loop_path[i])

                loop_values[i]['timing'] = {}

                with Timer(time_begin) as loop_values[i]['timing']['magnetic_field_loop']:
                    magnetic_field = MagneticField(**system_params2)

                with Timer(time_begin) as loop_values[i]['timing']['hamiltonian']:
                    hamiltonian = TJHamiltonian(lattice=lattice,
                                                h_space=h_space,
                                                magnetic_field=magnetic_field,
                                                **system_params2)

                hamiltonian.counter = 0
                with Timer(time_begin) as loop_values[i]['timing']['ground_state']:
                    ground_energies, ground_state_current = calc_ground_state(hamiltonian, **calculation_params)
                loop_values[i]['ground_energies'] = ground_energies
                loop_values[i]['n_iterations'] = hamiltonian.counter

                wilson_loop *= np.vdot(ground_state_prev, ground_state_current)

                ground_state_prev = ground_state_current

            wilson_loop *= np.vdot(ground_state_prev, ground_state)

            wilson_loop = np.angle(wilson_loop)/2/np.pi

            ground_state_values['wilson_loop'] = wilson_loop
            ground_state_values['x_c'] = x_c
            ground_state_values['y_c'] = y_c
            ground_state_values['loop_area'] = loop_area
            ground_state_values['loop'] = loop_values

    ground_state_values['time_begin'] = time_begin
    ground_state_values['timing'] = timing

    return ground_state_values
