chernNumbers dependencies
=========================
core
----
- numpy
- sciyp
- matplotlib
- pyyaml

For documentation
-----------------
- sphinx
- numpydoc
- sphinx_rtd_theme (now part of sphinx)
