----------------
chernNumbers API
----------------

.. autosummary::

    chern_numbers.SquareLattice.move_on_lattice

tJSystem
--------

SquareLattice
^^^^^^^^^^^^^
.. autoclass:: chern_numbers.SquareLattice
    :members:
    :private-members:
    :undoc-members:

tJHilbertSpace
^^^^^^^^^^^^^^
.. autoclass:: chern_numbers.TJHilbertSpace
    :members:
    :private-members:
    :undoc-members:

MagneticField
^^^^^^^^^^^^^^
.. autoclass:: chern_numbers.MagneticField
    :members:
    :private-members:
    :undoc-members:

tJHamiltonian
^^^^^^^^^^^^^
.. autoclass:: chern_numbers.TJHamiltonian
    :members:
    :private-members:
    :undoc-members:

..
    calc_magnetic_potential
    ^^^^^^^^^^^^^^^^^^^^^^^
    .. autofunction:: chern_numbers.tj_system.magnetic_potential.calc_magnetic_phase

----------------------
chernNumbers functions
----------------------

-----
Tests
-----

